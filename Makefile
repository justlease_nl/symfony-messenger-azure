DOCKER=docker run --rm -it -v "$(shell pwd):/usr/share/nginx" justlease/php:php7.4-builder
PHP_CS=vendor/bin/php-cs-fixer fix --verbose --diff
fix:
	$(DOCKER) $(PHP_CS)
analyse:
	$(DOCKER) vendor/bin/phpstan analyse
lint:
	$(DOCKER) $(PHP_CS) --dry-run
test:
	$(DOCKER) vendor/bin/phpunit tests
