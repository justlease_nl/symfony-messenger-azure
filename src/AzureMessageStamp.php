<?php

declare(strict_types=1);

namespace Justlease\AzureMessengerAdapter;

use Symfony\Component\Messenger\Stamp\StampInterface;
use WindowsAzure\ServiceBus\Models\BrokeredMessage;

final class AzureMessageStamp implements StampInterface
{
    public BrokeredMessage $message;

    public function __construct(BrokeredMessage $message)
    {
        $this->message = $message;
    }
}
