<?php

declare(strict_types=1);

/**
 * LICENSE: Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * PHP version 5
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @see      https://github.com/WindowsAzure/azure-sdk-for-php
 */

namespace WindowsAzure\ServiceBus;

use WindowsAzure\Common\Internal\Http\HttpCallContext;
use WindowsAzure\Common\Internal\Http\HttpClient;
use WindowsAzure\Common\Internal\Resources;
use WindowsAzure\Common\Internal\ServiceRestProxy;
use WindowsAzure\ServiceBus\Internal\IServiceBus;
use WindowsAzure\ServiceBus\Models\BrokeredMessage;
use WindowsAzure\ServiceBus\Models\BrokerProperties;
use WindowsAzure\ServiceBus\Models\ReceiveMessageOptions;

/**
 * This class constructs HTTP requests and receive HTTP responses
 * for Service Bus.
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @version   Release: 0.5.0_2016-11
 *
 * @see      https://github.com/WindowsAzure/azure-sdk-for-php
 */
class ServiceBusRestProxy extends ServiceRestProxy implements IServiceBus
{
    /**
     * Sends a brokered message.
     *
     * Queues:
     *
     * @see http://msdn.microsoft.com/en-us/library/windowsazure/hh780775
     *
     * Topic Subscriptions:
     * @see http://msdn.microsoft.com/en-us/library/windowsazure/hh780786
     *
     * @param string          $path            The path to send message
     * @param BrokeredMessage $brokeredMessage The brokered message
     */
    public function sendMessage(string $path, BrokeredMessage $brokeredMessage): void
    {
        $httpCallContext = new HttpCallContext();
        $httpCallContext->setMethod(Resources::HTTP_POST);
        $httpCallContext->addStatusCode(Resources::STATUS_CREATED);
        $httpCallContext->setPath($path);
        $contentType = $brokeredMessage->getContentType();

        if (null !== $contentType) {
            $httpCallContext->addHeader(
                Resources::CONTENT_TYPE,
                $contentType
            );
        }

        $brokerProperties = $brokeredMessage->getBrokerProperties();
        if (null !== $brokerProperties) {
            $httpCallContext->addHeader(
                Resources::BROKER_PROPERTIES,
                $brokerProperties->toString()
            );
        }
        $customProperties = $brokeredMessage->getProperties();

        if (!empty($customProperties)) {
            foreach ($customProperties as $key => $value) {
                $value = json_encode($value, \JSON_THROW_ON_ERROR);
                $httpCallContext->addHeader($key, $value);
            }
        }

        $httpCallContext->setBody($brokeredMessage->getBody());
        $this->sendHttpContext($httpCallContext);
    }

    /**
     * Sends a queue message.
     *
     * @see http://msdn.microsoft.com/en-us/library/windowsazure/hh780775
     *
     * @param string          $queueName       The name of the queue
     * @param BrokeredMessage $brokeredMessage The brokered message
     */
    public function sendQueueMessage(string $queueName, BrokeredMessage $brokeredMessage): void
    {
        $path = sprintf(Resources::SEND_MESSAGE_PATH, $queueName);
        $this->sendMessage($path, $brokeredMessage);
    }

    /**
     * Receives a queue message.
     *
     * @see http://msdn.microsoft.com/en-us/library/windowsazure/hh780735
     * @see http://msdn.microsoft.com/en-us/library/windowsazure/hh780756
     *
     * @param string                     $queueName             The name of the
     *                                                          queue
     * @param ReceiveMessageOptions|null $receiveMessageOptions The options to
     *                                                          receive the message
     */
    public function receiveQueueMessage(
        string $queueName,
        ReceiveMessageOptions $receiveMessageOptions = null
    ): ?BrokeredMessage {
        $queueMessagePath = sprintf(Resources::RECEIVE_MESSAGE_PATH, $queueName);

        return $this->receiveMessage(
            $queueMessagePath,
            $receiveMessageOptions
        );
    }

    // @codingStandardsIgnoreStart

    /**
     * Receives a message.
     *
     * Queues:
     *
     * @see http://msdn.microsoft.com/en-us/library/windowsazure/hh780735
     * @see http://msdn.microsoft.com/en-us/library/windowsazure/hh780756
     *
     * Topic Subscriptions:
     * @see http://msdn.microsoft.com/en-us/library/windowsazure/hh780722
     * @see http://msdn.microsoft.com/en-us/library/windowsazure/hh780770
     *
     * @param string                $path                  The path of the
     *                                                     message
     * @param ReceiveMessageOptions $receiveMessageOptions The options to
     *                                                     receive the message
     */
    public function receiveMessage(string $path, ReceiveMessageOptions $receiveMessageOptions = null): ?BrokeredMessage
    {
        if (null === $receiveMessageOptions) {
            $receiveMessageOptions = new ReceiveMessageOptions();
        }

        $httpCallContext = new HttpCallContext();
        $httpCallContext->setPath($path);
        $httpCallContext->addStatusCode(Resources::STATUS_CREATED);
        $httpCallContext->addStatusCode(Resources::STATUS_NO_CONTENT);
        $httpCallContext->addStatusCode(Resources::STATUS_OK);
        $timeout = $receiveMessageOptions->getTimeout();
        if (null !== $timeout) {
            $httpCallContext->addQueryParameter('timeout', (string) $timeout);
        }

        if ($receiveMessageOptions->getIsReceiveAndDelete()) {
            $httpCallContext->setMethod(Resources::HTTP_DELETE);
        } elseif ($receiveMessageOptions->getIsPeekLock()) {
            $httpCallContext->setMethod(Resources::HTTP_POST);
        } else {
            throw new \InvalidArgumentException(Resources::INVALID_RECEIVE_MODE_MSG);
        }

        $response = $this->sendHttpContext($httpCallContext);
        if (Resources::STATUS_NO_CONTENT === $response->getStatusCode()) {
            $brokeredMessage = null;
        } else {
            $responseHeaders = HttpClient::getResponseHeaders($response);

            $brokerProperties = new BrokerProperties();

            if (\array_key_exists('brokerproperties', $responseHeaders)) {
                $brokerProperties = BrokerProperties::create($responseHeaders['brokerproperties']);
            }

            if (\array_key_exists('location', $responseHeaders)) {
                $brokerProperties->setLockLocation($responseHeaders['location']);
            }

            $brokeredMessage = new BrokeredMessage();
            $brokeredMessage->setBrokerProperties($brokerProperties);

            if (\array_key_exists(Resources::CONTENT_TYPE, $responseHeaders)) {
                $brokeredMessage->setContentType($responseHeaders[Resources::CONTENT_TYPE]);
            }

            if (\array_key_exists('date', $responseHeaders)) {
                $brokeredMessage->setDate($responseHeaders['date']);
            }

            $brokeredMessage->setBody((string) $response->getBody());

            foreach ($responseHeaders as $headerKey => $value) {
                $decodedValue = json_decode($value);
                if (is_scalar($decodedValue)) {
                    $brokeredMessage->setProperty(
                        $headerKey,
                        $decodedValue
                    );
                }
            }
        }

        return $brokeredMessage;
    }

    // @codingStandardsIgnoreEnd

    /**
     * Sends a brokered message to a specified topic.
     *
     * @see http://msdn.microsoft.com/en-us/library/windowsazure/hh780786
     *
     * @param string          $topicName       The name of the topic
     * @param BrokeredMessage $brokeredMessage The brokered message
     */
    public function sendTopicMessage(string $topicName, BrokeredMessage $brokeredMessage): void
    {
        $topicMessagePath = sprintf(Resources::SEND_MESSAGE_PATH, $topicName);
        $this->sendMessage($topicMessagePath, $brokeredMessage);
    }

    /**
     * Receives a subscription message.
     *
     * @see http://msdn.microsoft.com/en-us/library/windowsazure/hh780722
     * @see http://msdn.microsoft.com/en-us/library/windowsazure/hh780770
     *
     * @param string                     $topicName             The name of the
     *                                                          topic
     * @param string                     $subscriptionName      The name of the
     *                                                          subscription
     * @param ReceiveMessageOptions|null $receiveMessageOptions The options to
     *                                                          receive the subscription message
     */
    public function receiveSubscriptionMessage(
        string $topicName,
        string $subscriptionName,
        ReceiveMessageOptions $receiveMessageOptions = null
    ): ?BrokeredMessage {
        $messagePath = sprintf(
            Resources::RECEIVE_SUBSCRIPTION_MESSAGE_PATH,
            $topicName,
            $subscriptionName
        );

        $brokeredMessage = $this->receiveMessage(
            $messagePath,
            $receiveMessageOptions
        );

        return $brokeredMessage;
    }

    /**
     * Unlocks a brokered message.
     *
     * Queues:
     *
     * @see http://msdn.microsoft.com/en-us/library/windowsazure/hh780723
     *
     * Topic Subscriptions:
     * @see http://msdn.microsoft.com/en-us/library/windowsazure/hh780737
     *
     * @param BrokeredMessage $brokeredMessage The brokered message
     */
    public function unlockMessage(BrokeredMessage $brokeredMessage): void
    {
        $httpCallContext = new HttpCallContext();
        $httpCallContext->setMethod(Resources::HTTP_PUT);
        $lockLocationPath = $this->getLockLocationPath($brokeredMessage);
        $httpCallContext->setPath($lockLocationPath);
        $httpCallContext->addStatusCode(Resources::STATUS_OK);
        $this->sendHttpContext($httpCallContext);
    }

    /**
     * Deletes a brokered message.
     *
     * Queues:
     *
     * @see http://msdn.microsoft.com/en-us/library/windowsazure/hh780767
     *
     * Topic Subscriptions:
     * @see http://msdn.microsoft.com/en-us/library/windowsazure/hh780768
     *
     * @param BrokeredMessage $brokeredMessage The brokered message
     */
    public function deleteMessage(BrokeredMessage $brokeredMessage): void
    {
        $httpCallContext = new HttpCallContext();
        $httpCallContext->setMethod(Resources::HTTP_DELETE);
        $lockLocationPath = $this->getLockLocationPath($brokeredMessage);
        $httpCallContext->setPath($lockLocationPath);
        $httpCallContext->addStatusCode(Resources::STATUS_OK);
        $this->sendHttpContext($httpCallContext);
    }

    private function getLockLocationPath(BrokeredMessage $brokeredMessage): string
    {
        $lockLocation = $brokeredMessage->getLockLocation() ?? '';
        $lockLocationArray = parse_url($lockLocation);

        if (\is_array($lockLocationArray) && \array_key_exists(Resources::PHP_URL_PATH, $lockLocationArray)) {
            $lockLocationPath = $lockLocationArray[Resources::PHP_URL_PATH];
            $lockLocationPath = preg_replace(
                '@^\/@',
                Resources::EMPTY_STRING,
                $lockLocationPath
            );
        } else {
            $lockLocationPath = Resources::EMPTY_STRING;
        }

        if (empty($lockLocationPath)) {
            throw new \InvalidArgumentException(Resources::MISSING_LOCK_LOCATION_MSG);
        }

        return $lockLocationPath;
    }
}
