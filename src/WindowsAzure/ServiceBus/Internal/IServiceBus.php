<?php

declare(strict_types=1);

/**
 * LICENSE: Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * PHP version 5
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @see      https://github.com/WindowsAzure/azure-sdk-for-php
 */

namespace WindowsAzure\ServiceBus\Internal;

use Exception;
use WindowsAzure\Common\Internal\FilterableService;
use WindowsAzure\ServiceBus\Models\BrokeredMessage;
use WindowsAzure\ServiceBus\Models\ReceiveMessageOptions;

/**
 * This class constructs HTTP requests and receive HTTP responses for Service Bus.
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @version   Release: 0.5.0_2016-11
 *
 * @see      https://github.com/WindowsAzure/azure-sdk-for-php
 */
interface IServiceBus extends FilterableService
{
    /**
     * Sends a brokered message.
     *
     * @param string          $path            The path to send message
     * @param BrokeredMessage $brokeredMessage The brokered message
     *
     * @throws Exception
     */
    public function sendMessage(string $path, BrokeredMessage $brokeredMessage): void;

    /**
     * Sends a queue message.
     *
     * @param string          $queueName       The name of the queue to send
     *                                         message
     * @param BrokeredMessage $brokeredMessage The brokered message
     *
     * @throws Exception
     */
    public function sendQueueMessage(string $queueName, BrokeredMessage $brokeredMessage): void;

    /**
     * Receives a queue message.
     *
     * @param string                     $queueName              The name of the
     *                                                           queue
     * @param ReceiveMessageOptions|null $receivedMessageOptions The options to
     *                                                           receive the message
     *
     * @throws Exception
     */
    public function receiveQueueMessage(string $queueName, ReceiveMessageOptions $receivedMessageOptions = null): ?BrokeredMessage;

    /**
     * Receives a message.
     *
     * @param string                $path                  The path of the
     *                                                     message
     * @param ReceiveMessageOptions $receiveMessageOptions The options to
     *                                                     receive the message
     *
     * @throws Exception
     */
    public function receiveMessage(string $path, ReceiveMessageOptions $receiveMessageOptions): ?BrokeredMessage;

    /**
     * Sends a brokered message to a specified topic.
     *
     * @param string          $topicName       The name of the topic
     * @param BrokeredMessage $brokeredMessage The brokered message
     *
     * @throws Exception
     */
    public function sendTopicMessage(string $topicName, BrokeredMessage $brokeredMessage): void;

    /**
     * Receives a subscription message.
     *
     * @param string                     $topicName             The name of the
     *                                                          topic
     * @param string                     $subscriptionName      The name of the
     *                                                          subscription
     * @param ReceiveMessageOptions|null $receiveMessageOptions The options to
     *                                                          receive the subscription message
     *
     * @throws Exception
     */
    public function receiveSubscriptionMessage(
        string $topicName,
        string $subscriptionName,
        ReceiveMessageOptions $receiveMessageOptions = null
    ): ?BrokeredMessage;

    /**
     * Unlocks a brokered message.
     *
     * @param BrokeredMessage $brokeredMessage The brokered message
     *
     * @throws Exception
     */
    public function unlockMessage(BrokeredMessage $brokeredMessage): void;

    /**
     * Deletes a brokered message.
     *
     * @param BrokeredMessage $brokeredMessage The brokered message
     *
     * @throws Exception
     */
    public function deleteMessage(BrokeredMessage $brokeredMessage): void;
}
