<?php

declare(strict_types=1);

/**
 * LICENSE: Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * PHP version 5
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @see      https://github.com/WindowsAzure/azure-sdk-for-php
 */

namespace WindowsAzure\ServiceBus\Internal;

use Exception;
use WindowsAzure\Common\Internal\FilterableService;
use WindowsAzure\ServiceBus\Models\ListQueuesOptions;
use WindowsAzure\ServiceBus\Models\ListQueuesResult;
use WindowsAzure\ServiceBus\Models\ListRulesOptions;
use WindowsAzure\ServiceBus\Models\ListRulesResult;
use WindowsAzure\ServiceBus\Models\ListSubscriptionsOptions;
use WindowsAzure\ServiceBus\Models\ListSubscriptionsResult;
use WindowsAzure\ServiceBus\Models\ListTopicsOptions;
use WindowsAzure\ServiceBus\Models\ListTopicsResult;
use WindowsAzure\ServiceBus\Models\QueueInfo;
use WindowsAzure\ServiceBus\Models\RuleInfo;
use WindowsAzure\ServiceBus\Models\SubscriptionInfo;
use WindowsAzure\ServiceBus\Models\TopicInfo;

/**
 * This class constructs HTTP requests and receive HTTP responses for Service Bus.
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @version   Release: 0.5.0_2016-11
 *
 * @see      https://github.com/WindowsAzure/azure-sdk-for-php
 */
interface IServiceBusManagement extends FilterableService
{
    /**
     * Creates a queue with specified queue info.
     *
     * @param QueueInfo $queueInfo The information of the queue
     *
     * @throws Exception
     */
    public function createQueue(QueueInfo $queueInfo): QueueInfo;

    /**
     * Deletes a queue.
     *
     * @param string $queuePath The path of the queue
     *
     * @throws Exception
     */
    public function deleteQueue(string $queuePath): void;

    /**
     * Gets a queue with specified path.
     *
     * @param string $queuePath The path of the queue
     *
     * @throws Exception
     */
    public function getQueue(string $queuePath): QueueInfo;

    /**
     * Lists a queue.
     *
     * @param ListQueuesOptions|null $listQueueOptions The options to list the
     *                                                 queues
     *
     * @throws Exception
     */
    public function listQueues(ListQueuesOptions $listQueueOptions = null): ListQueuesResult;

    /**
     * Creates a topic with specified topic info.
     *
     * @param TopicInfo $topicInfo The information of the topic
     *
     * @throws Exception
     */
    public function createTopic(TopicInfo $topicInfo): TopicInfo;

    /**
     * Deletes a topic with specified topic path.
     *
     * @param string $topicPath The path of the topic
     *
     * @throws Exception
     */
    public function deleteTopic(string $topicPath): void;

    /**
     * Gets a topic.
     *
     * @param string $topicPath The path of the topic
     *
     * @throws Exception
     */
    public function getTopic(string $topicPath): TopicInfo;

    /**
     * Lists topics.
     *
     * @param ListTopicsOptions|null $listTopicsOptions The options to list
     *                                                  the topics
     *
     * @throws Exception
     */
    public function listTopics(ListTopicsOptions $listTopicsOptions = null): ListTopicsResult;

    /**
     * Creates a subscription with specified topic path and
     * subscription info.
     *
     * @param string           $topicPath        The path of the topic
     * @param SubscriptionInfo $subscriptionInfo The information of the
     *                                           subscription
     *
     * @throws Exception
     */
    public function createSubscription(string $topicPath, SubscriptionInfo $subscriptionInfo): SubscriptionInfo;

    /**
     * Deletes a subscription.
     *
     * @param string $topicPath        The path of the topic
     * @param string $subscriptionName The name of the subscription
     *
     * @throws Exception
     */
    public function deleteSubscription(string $topicPath, string $subscriptionName): void;

    /**
     * Gets a subscription.
     *
     * @param string $topicPath        The path of the topic
     * @param string $subscriptionName The name of the subscription
     *
     * @throws Exception
     */
    public function getSubscription(string $topicPath, string $subscriptionName): SubscriptionInfo;

    /**
     * Lists subscriptions.
     *
     * @param string                        $topicPath                The path of
     *                                                                the topic
     * @param ListSubscriptionsOptions|null $listSubscriptionsOptions The options
     *                                                                to list the subscriptions
     *
     * @throws Exception
     */
    public function listSubscriptions(string $topicPath, ListSubscriptionsOptions $listSubscriptionsOptions = null): ListSubscriptionsResult;

    /**
     * Creates a rule.
     *
     * @param string   $topicPath        The path of the topic
     * @param string   $subscriptionName The name of the subscription
     * @param RuleInfo $ruleInfo         The info of the rule
     *
     * @throws Exception
     */
    public function createRule(string $topicPath, string $subscriptionName, RuleInfo $ruleInfo): RuleInfo;

    /**
     * Deletes a rule.
     *
     * @param string $topicPath        The path of the topic
     * @param string $subscriptionName The name of the subscription
     * @param string $ruleName         The name of the rule
     *
     * @throws Exception
     */
    public function deleteRule(string $topicPath, string $subscriptionName, string $ruleName): void;

    /**
     * Gets a rule.
     *
     * @param string $topicPath        The path of the topic
     * @param string $subscriptionName The name of the subscription
     * @param string $ruleName         The name of the rule
     *
     * @throws Exception
     */
    public function getRule(string $topicPath, string $subscriptionName, string $ruleName): RuleInfo;

    /**
     * Lists rules.
     *
     * @param string                $topicPath        The path of the topic
     * @param string                $subscriptionName The name of the subscription
     * @param ListRulesOptions|null $listRulesOptions The options to list the rules
     *
     * @throws Exception
     */
    public function listRules(string $topicPath, string $subscriptionName, ListRulesOptions $listRulesOptions = null): ListRulesResult;
}
