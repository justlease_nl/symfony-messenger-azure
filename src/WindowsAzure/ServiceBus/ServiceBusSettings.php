<?php

declare(strict_types=1);

/**
 * LICENSE: Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * PHP version 5
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @see      https://github.com/windowsazure/azure-sdk-for-php
 */

namespace WindowsAzure\ServiceBus;

use WindowsAzure\Common\Internal\Filters\SASFilter;
use WindowsAzure\Common\Internal\IServiceFilter;
use WindowsAzure\Common\Internal\Resources;
use WindowsAzure\Common\Internal\ServiceSettings;
use WindowsAzure\Common\Internal\Utilities;
use WindowsAzure\Common\Internal\Validate;

/**
 * Represents the settings used to sign and access a request against the service
 * bus.
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @version   Release: 0.5.0_2016-11
 *
 * @see      https://github.com/windowsazure/azure-sdk-for-php
 */
class ServiceBusSettings extends ServiceSettings
{
    private string $_serviceBusEndpointUri;

    /**
     * Validator for the Endpoint setting. Must be a valid Uri.
     */
    private static array $_serviceBusEndpointSetting;

    private static array $_sasKeyNameSetting;

    private static array $_sasKeySetting;

    /**
     * Holds the expected setting keys.
     */
    private IServiceFilter $_filter;

    /**
     * Initializes static members of the class.
     */
    protected static function init(): void
    {
        self::$_serviceBusEndpointSetting = self::settingWithFunc(
            Resources::SERVICE_BUS_ENDPOINT_NAME,
            Validate::getIsValidUri()
        );

        self::$_sasKeyNameSetting = self::setting(
            Resources::SHARED_SHARED_ACCESS_KEY_NAME
        );

        self::$_sasKeySetting = self::setting(
            Resources::SHARED_SHARED_ACCESS_KEY
        );

        self::$validSettingKeys[] = Resources::SERVICE_BUS_ENDPOINT_NAME;
        self::$validSettingKeys[] = Resources::SHARED_SHARED_ACCESS_KEY_NAME;
        self::$validSettingKeys[] = Resources::SHARED_SHARED_ACCESS_KEY;
        self::$validSettingKeys[] = Resources::ENTITY_PATH;
    }

    public function __construct(
        string $serviceBusEndpoint,
        IServiceFilter $filter
    ) {
        $this->_serviceBusEndpointUri = $serviceBusEndpoint;
        $this->_filter = $filter;
    }

    private static function createServiceBusWithSasAuthentication(array $tokenizedSettings, string $connectionString = ''): self
    {
        $required = [
            self::$_serviceBusEndpointSetting,
            self::$_sasKeyNameSetting,
            self::$_sasKeySetting,
        ];
        $optional = [
        ];
        $matchedSpecs = self::getMatchedSpecs($tokenizedSettings, $required, $optional, $connectionString);

        $endpoint = Utilities::tryGetValueInsensitive(
            Resources::SERVICE_BUS_ENDPOINT_NAME,
            $tokenizedSettings
        );

        $sharedAccessKeyName = Utilities::tryGetValueInsensitive(
            Resources::SHARED_SHARED_ACCESS_KEY_NAME,
            $tokenizedSettings
        );
        $sharedAccessKey = Utilities::tryGetValueInsensitive(
            Resources::SHARED_SHARED_ACCESS_KEY,
            $tokenizedSettings
        );

        return new self($endpoint, new SASFilter(
            $sharedAccessKeyName,
            $sharedAccessKey
        ));
    }

    private static function getMatchedSpecs(array $tokenizedSettings, array $required, array $optional = [], string $connectionString = ''): bool
    {
        $matchedSpecs = self::matchedSpecification(
            $tokenizedSettings,
            self::allRequired(...$required),
            self::optional(...$optional)
        );

        if (!$matchedSpecs) {
            self::noMatch($connectionString);
        }

        return $matchedSpecs;
    }

    /**
     * Creates a ServiceBusSettings object from the given connection string.
     */
    public static function createFromConnectionString(string $connectionString): self
    {
        $tokenizedSettings = self::parseAndValidateKeys($connectionString);
        if (Utilities::inArrayInsensitive(Resources::SHARED_SHARED_ACCESS_KEY_NAME, array_keys($tokenizedSettings))) {
            return self::createServiceBusWithSasAuthentication($tokenizedSettings, $connectionString);
        }

        throw new \InvalidArgumentException('Only Sas auth is supported, others like Wrap are not implemented');
    }

    /**
     * Gets the Service Bus endpoint URI.
     */
    public function getServiceBusEndpointUri(): string
    {
        return $this->_serviceBusEndpointUri;
    }

    public function getFilter(): IServiceFilter
    {
        return $this->_filter;
    }
}
