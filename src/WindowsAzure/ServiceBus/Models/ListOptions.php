<?php

declare(strict_types=1);
/**
 * LICENSE: Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * PHP version 5
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @see      https://github.com/WindowsAzure/azure-sdk-for-php
 */

namespace WindowsAzure\ServiceBus\Models;

/**
 * The base class for the options for list request.
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @version   Release: 0.5.0_2016-11
 *
 * @see      https://github.com/WindowsAzure/azure-sdk-for-php
 */
class ListOptions
{
    /**
     * The skip query parameter for list API.
     */
    private int $_skip = 0;

    /**
     * The top query parameter for list API.
     */
    private int $_top = 0;

    /**
     * Creates a list option instance with default parameters.
     */
    public function __construct()
    {
    }

    /**
     * Gets the skip parameter.
     */
    public function getSkip(): int
    {
        return $this->_skip;
    }

    /**
     * Sets the skip parameter.
     *
     * @param int $skip value
     */
    public function setSkip(int $skip): void
    {
        $this->_skip = $skip;
    }

    /**
     * Gets the top parameter.
     */
    public function getTop(): int
    {
        return $this->_top;
    }

    /**
     * Sets the top parameter.
     *
     * @param int $top value
     */
    public function setTop(int $top): void
    {
        $this->_top = $top;
    }
}
