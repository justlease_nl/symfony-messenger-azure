<?php

declare(strict_types=1);

/**
 * LICENSE: Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * PHP version 5
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @see      https://github.com/WindowsAzure/azure-sdk-for-php
 */

namespace WindowsAzure\ServiceBus\Models;

use WindowsAzure\Common\Internal\Atom\Content;
use WindowsAzure\Common\Internal\Atom\Entry;
use WindowsAzure\Common\Internal\Resources;
use WindowsAzure\Common\Internal\Serialization\XmlSerializer;
use WindowsAzure\Common\Internal\Validate;

/**
 * The information of a queue.
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @version   Release: 0.5.0_2016-11
 *
 * @see      https://github.com/WindowsAzure/azure-sdk-for-php
 */
class QueueInfo
{
    /**
     * The entry of the queue info.
     */
    private Entry $_entry;

    /**
     * The description of the queue.
     */
    private QueueDescription $_queueDescription;

    /**
     * Creates a QueueInfo instance with specified parameters.
     *
     * @param string            $title            The name of the queue
     * @param ?QueueDescription $queueDescription The description of the queue
     */
    public function __construct(
        string $title = Resources::EMPTY_STRING,
        QueueDescription $queueDescription = null
    ) {
        Validate::isString($title, 'title');
        if (null === $queueDescription) {
            $queueDescription = new QueueDescription();
        }

        $this->_queueDescription = $queueDescription;
        $this->_entry = new Entry();
        $this->_entry->setTitle($title);
        $this->_entry->setAttribute(
            Resources::XMLNS,
            Resources::SERVICE_BUS_NAMESPACE
        );
    }

    /**
     * Populates the properties of the queue info instance with a
     * ATOM ENTRY XML string.
     *
     * @param string $entryXml An ATOM entry based XML string
     */
    public function parseXml(string $entryXml): void
    {
        $this->_entry->parseXml($entryXml);
        $content = $this->_entry->getContent();
        if (empty($content->getText())) {
            $this->_queueDescription = new QueueDescription();
        } else {
            $this->_queueDescription = QueueDescription::create($content->getText());
        }
    }

    /**
     * Returns a XML string based on ATOM ENTRY schema.
     *
     * @param \XMLWriter $xmlWriter The XML writer
     */
    public function writeXml(\XMLWriter $xmlWriter): void
    {
        $content = null;
        if (null !== $this->_queueDescription) {
            $content = new Content();
            $content->setText(
                XmlSerializer::objectSerialize(
                    $this->_queueDescription,
                    'QueueDescription'
                )
            );
            $content->setType(Resources::XML_CONTENT_TYPE);
        }
        $this->_entry->setContent($content);
        $this->_entry->writeXml($xmlWriter);
    }

    /**
     * Gets the description of the queue.
     */
    public function getQueueDescription(): QueueDescription
    {
        return $this->_queueDescription;
    }

    /**
     * Sets the description of the queue.
     *
     * @param QueueDescription $queueDescription The description of the queue
     */
    public function setQueueDescription(QueueDescription $queueDescription): void
    {
        $this->_queueDescription = $queueDescription;
    }

    /**
     * Gets the title.
     */
    public function getTitle(): string
    {
        return $this->_entry->getTitle();
    }

    /**
     * Sets the title.
     *
     * @param string $title The title of the queue info
     */
    public function setTitle(string $title): void
    {
        $this->_entry->setTitle($title);
    }

    /**
     * Gets the entry.
     */
    public function getEntry(): Entry
    {
        return $this->_entry;
    }

    /**
     * Sets the entry.
     *
     * @param Entry $entry The entry of the queue info
     */
    public function setEntry(Entry $entry): void
    {
        $this->_entry = $entry;
    }

    /**
     * Gets the lock duration.
     */
    public function getLockDuration(): string
    {
        return $this->_queueDescription->getLockDuration();
    }

    /**
     * Sets the lock duration.
     *
     * @param string $lockDuration The lock duration
     */
    public function setLockDuration(string $lockDuration): void
    {
        $this->_queueDescription->setLockDuration($lockDuration);
    }

    /**
     * gets the maximum size in mega bytes.
     */
    public function getMaxSizeInMegabytes(): int
    {
        return $this->_queueDescription->getMaxSizeInMegabytes();
    }

    /**
     * Sets the max size in mega bytes.
     *
     * @param int $maxSizeInMegabytes The max size in mega bytes
     */
    public function setMaxSizeInMegabytes(int $maxSizeInMegabytes): void
    {
        $this->_queueDescription->setMaxSizeInMegabytes($maxSizeInMegabytes);
    }

    /**
     * Gets requires duplicate detection.
     */
    public function getRequiresDuplicateDetection(): bool
    {
        return $this->_queueDescription->getRequiresDuplicateDetection();
    }

    /**
     * Sets requires duplicate detection.
     *
     * @param bool $requiresDuplicateDetection If duplicate detection is required
     */
    public function setRequiresDuplicateDetection(bool $requiresDuplicateDetection): void
    {
        $this->_queueDescription->setRequiresDuplicateDetection(
            $requiresDuplicateDetection
        );
    }

    /**
     * Gets the requires session.
     */
    public function getRequiresSession(): bool
    {
        return $this->_queueDescription->getRequiresSession();
    }

    /**
     * Sets the requires session.
     *
     * @param bool $requiresSession If session is required
     */
    public function setRequiresSession(bool $requiresSession): void
    {
        $this->_queueDescription->setRequiresSession($requiresSession);
    }

    /**
     * gets the default message time to live.
     */
    public function getDefaultMessageTimeToLive(): string
    {
        return $this->_queueDescription->getDefaultMessageTimeToLive();
    }

    /**
     * Sets the default message time to live.
     *
     * @param string $defaultMessageTimeToLive The default message time to live
     */
    public function setDefaultMessageTimeToLive(string $defaultMessageTimeToLive): void
    {
        $this->_queueDescription->setDefaultMessageTimeToLive(
            $defaultMessageTimeToLive
        );
    }

    /**
     * Gets dead lettering on message expiration.
     */
    public function getDeadLetteringOnMessageExpiration(): string
    {
        return $this->_queueDescription->getDeadLetteringOnMessageExpiration();
    }

    /**
     * Sets dead lettering on message expiration.
     *
     * @param string $deadLetteringOnMessageExpiration The dead lettering on
     *                                                 message expiration
     */
    public function setDeadLetteringOnMessageExpiration(
        string $deadLetteringOnMessageExpiration
    ): void {
        $this->_queueDescription->setDeadLetteringOnMessageExpiration(
            $deadLetteringOnMessageExpiration
        );
    }

    /**
     * Gets duplicate detection history time window.
     */
    public function getDuplicateDetectionHistoryTimeWindow(): string
    {
        return $this->_queueDescription->getDuplicateDetectionHistoryTimeWindow();
    }

    /**
     * Sets the duplicate detection history time window.
     *
     * @param string $duplicateDetectionHistoryTimeWindow The duplicate
     *                                                    detection history time window
     */
    public function setDuplicateDetectionHistoryTimeWindow(
        string $duplicateDetectionHistoryTimeWindow
    ): void {
        $this->_queueDescription->setDuplicateDetectionHistoryTimeWindow(
            $duplicateDetectionHistoryTimeWindow
        );
    }

    /**
     * Gets maximum delivery count.
     */
    public function getMaxDeliveryCount(): ?int
    {
        return $this->_queueDescription->getMaxDeliveryCount();
    }

    public function setMaxDeliveryCount(int $maxDeliveryCount): void
    {
        $this->_queueDescription->setMaxDeliveryCount($maxDeliveryCount);
    }

    /**
     * Gets enable batched operation.
     */
    public function getEnableBatchedOperations(): bool
    {
        return $this->_queueDescription->getEnableBatchedOperations();
    }

    /**
     * Sets enable batched operations.
     *
     * @param bool $enableBatchedOperations Enable batched operations
     */
    public function setEnableBatchedOperations(bool $enableBatchedOperations): void
    {
        $this->_queueDescription->setEnableBatchedOperations(
            $enableBatchedOperations
        );
    }

    /**
     * Gets the size in bytes.
     */
    public function getSizeInBytes(): int
    {
        return $this->_queueDescription->getSizeInBytes();
    }

    /**
     * Sets the size in bytes.
     *
     * @param int $sizeInBytes The size in bytes
     */
    public function setSizeInBytes(int $sizeInBytes): void
    {
        $this->_queueDescription->setSizeInBytes($sizeInBytes);
    }

    /**
     * Gets the message count.
     */
    public function getMessageCount(): int
    {
        return $this->_queueDescription->getMessageCount();
    }

    public function setMessageCount(int $messageCount): void
    {
        $this->_queueDescription->setMessageCount($messageCount);
    }
}
