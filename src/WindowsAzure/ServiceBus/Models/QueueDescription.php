<?php

declare(strict_types=1);

/**
 * LICENSE: Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * PHP version 5
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @see      https://github.com/WindowsAzure/azure-sdk-for-php
 */

namespace WindowsAzure\ServiceBus\Models;

/**
 * The description of a queue.
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @version   Release: 0.5.0_2016-11
 *
 * @see      https://github.com/WindowsAzure/azure-sdk-for-php
 */
class QueueDescription
{
    /**
     * The duration of the lock.
     */
    private string $_lockDuration = '';

    /**
     * The maximum size in mega bytes.
     */
    private int $_maxSizeInMegabytes = 0;

    /**
     * Requires duplicate detection for queue.
     */
    private bool $_requiresDuplicateDetection = false;

    /**
     * Requires session for the queue.
     */
    private bool $_requiresSession = false;

    /**
     * The default message time to live.
     */
    private string $_defaultMessageTimeToLive = '';

    /**
     * The dead lettering on message expiration.
     */
    private string $_deadLetteringOnMessageExpiration = '';

    /**
     * The duplicate detection history time window.
     */
    private string $_duplicateDetectionHistoryTimeWindow = '';

    /**
     * The maximum delivery count.
     */
    private ?int $_maxDeliveryCount = null;

    /**
     * Enables batched operations.
     */
    private bool $_enableBatchedOperations = false;

    /**
     * The size in bytes.
     */
    private int $_sizeInBytes = 0;

    /**
     * The count of the message.
     */
    private int $_messageCount = 0;

    // @codingStandardsIgnoreStart

    /**
     * Creates a queue description object with specified XML string.
     *
     * @param string $queueDescriptionXml A XML based string describing
     *                                    the queue
     */
    public static function create(string $queueDescriptionXml): self
    {
        $queueDescription = new self();
        $root = simplexml_load_string(
            $queueDescriptionXml
        );
        $queueDescriptionArray = (array) $root;
        if (\array_key_exists('LockDuration', $queueDescriptionArray)) {
            $queueDescription->setLockDuration(
                (string) $queueDescriptionArray['LockDuration']
            );
        }

        if (\array_key_exists('MaxSizeInMegabytes', $queueDescriptionArray)) {
            $queueDescription->setMaxSizeInMegabytes(
                (int) $queueDescriptionArray['MaxSizeInMegabytes']
            );
        }

        if (\array_key_exists(
            'RequiresDuplicateDetection',
            $queueDescriptionArray
        )
        ) {
            $queueDescription->setRequiresDuplicateDetection(
                (bool) $queueDescriptionArray['RequiresDuplicateDetection']
            );
        }

        if (\array_key_exists('RequiresSession', $queueDescriptionArray)) {
            $queueDescription->setRequiresSession(
                (bool) $queueDescriptionArray['RequiresSession']
            );
        }

        if (\array_key_exists(
            'DefaultMessageTimeToLive',
            $queueDescriptionArray
        )
        ) {
            $queueDescription->setDefaultMessageTimeToLive(
                (string) $queueDescriptionArray['DefaultMessageTimeToLive']
            );
        }

        if (\array_key_exists(
            'DeadLetteringOnMessageExpiration',
            $queueDescriptionArray
        )
        ) {
            $queueDescription->setDeadLetteringOnMessageExpiration(
                (string) $queueDescriptionArray['DeadLetteringOnMessageExpiration']
            );
        }

        if (\array_key_exists(
            'DuplicateDetectionHistoryTimeWindow',
            $queueDescriptionArray
        )
        ) {
            $queueDescription->setDuplicateDetectionHistoryTimeWindow(
                (string) $queueDescriptionArray['DuplicateDetectionHistoryTimeWindow']
            );
        }

        if (\array_key_exists('MaxDeliveryCount', $queueDescriptionArray)) {
            $queueDescription->setMaxDeliveryCount(
                (int) $queueDescriptionArray['MaxDeliveryCount']
            );
        }

        if (\array_key_exists('EnableBatchedOperations', $queueDescriptionArray)) {
            $queueDescription->setEnableBatchedOperations(
                (bool) $queueDescriptionArray['EnableBatchedOperations']
            );
        }

        if (\array_key_exists('SizeInBytes', $queueDescriptionArray)) {
            $queueDescription->setSizeInBytes(
                (int) $queueDescriptionArray['SizeInBytes']
            );
        }

        if (\array_key_exists('MessageCount', $queueDescriptionArray)) {
            $queueDescription->setMessageCount(
                (int) $queueDescriptionArray['MessageCount']
            );
        }

        return $queueDescription;
    }

    // @codingStandardsIgnoreEnd

    /**
     * Creates a queue description instance with default parameters.
     */
    public function __construct()
    {
    }

    /**
     * Gets the lock duration.
     */
    public function getLockDuration(): string
    {
        return $this->_lockDuration;
    }

    /**
     * Sets the lock duration.
     *
     * @param string $lockDuration The lock duration
     */
    public function setLockDuration(string $lockDuration): void
    {
        $this->_lockDuration = $lockDuration;
    }

    /**
     * gets the maximum size in mega bytes.
     */
    public function getMaxSizeInMegabytes(): int
    {
        return $this->_maxSizeInMegabytes;
    }

    /**
     * Sets the max size in mega bytes.
     *
     * @param int $maxSizeInMegabytes The max size in mega bytes
     */
    public function setMaxSizeInMegabytes(int $maxSizeInMegabytes): void
    {
        $this->_maxSizeInMegabytes = $maxSizeInMegabytes;
    }

    /**
     * Gets requires duplicate detection.
     */
    public function getRequiresDuplicateDetection(): bool
    {
        return $this->_requiresDuplicateDetection;
    }

    /**
     * Sets requires duplicate detection.
     *
     * @param bool $requiresDuplicateDetection If duplicate detection is required
     */
    public function setRequiresDuplicateDetection(bool $requiresDuplicateDetection): void
    {
        $this->_requiresDuplicateDetection = $requiresDuplicateDetection;
    }

    /**
     * Gets the requires session.
     */
    public function getRequiresSession(): bool
    {
        return $this->_requiresSession;
    }

    /**
     * Sets the requires session.
     *
     * @param bool $requiresSession If session is required
     */
    public function setRequiresSession(bool $requiresSession): void
    {
        $this->_requiresSession = $requiresSession;
    }

    /**
     * gets the default message time to live.
     */
    public function getDefaultMessageTimeToLive(): string
    {
        return $this->_defaultMessageTimeToLive;
    }

    /**
     * Sets the default message time to live.
     *
     * @param string $defaultMessageTimeToLive The default message time to live
     */
    public function setDefaultMessageTimeToLive(string $defaultMessageTimeToLive): void
    {
        $this->_defaultMessageTimeToLive = $defaultMessageTimeToLive;
    }

    /**
     * Gets dead lettering on message expiration.
     */
    public function getDeadLetteringOnMessageExpiration(): string
    {
        return $this->_deadLetteringOnMessageExpiration;
    }

    /**
     * Sets dead lettering on message expiration.
     *
     * @param string $deadLetteringOnMessageExpiration The dead lettering on
     *                                                 message expiration
     */
    public function setDeadLetteringOnMessageExpiration(
        string $deadLetteringOnMessageExpiration
    ): void {
        $this->_deadLetteringOnMessageExpiration = $deadLetteringOnMessageExpiration;
    }

    /**
     * Gets duplicate detection history time window.
     */
    public function getDuplicateDetectionHistoryTimeWindow(): string
    {
        return $this->_duplicateDetectionHistoryTimeWindow;
    }

    public function setDuplicateDetectionHistoryTimeWindow(
        string $duplicateDetectionHistoryTimeWindow
    ): void {
        $this->_duplicateDetectionHistoryTimeWindow = $duplicateDetectionHistoryTimeWindow;
    }

    public function getMaxDeliveryCount(): ?int
    {
        return $this->_maxDeliveryCount;
    }

    public function setMaxDeliveryCount(int $maxDeliveryCount): void
    {
        $this->_maxDeliveryCount = $maxDeliveryCount;
    }

    /**
     * Gets enable batched operation.
     */
    public function getEnableBatchedOperations(): bool
    {
        return $this->_enableBatchedOperations;
    }

    /**
     * Sets enable batched operations.
     *
     * @param bool $enableBatchedOperations Enable batched operations
     */
    public function setEnableBatchedOperations(bool $enableBatchedOperations): void
    {
        $this->_enableBatchedOperations = $enableBatchedOperations;
    }

    /**
     * Gets the size in bytes.
     */
    public function getSizeInBytes(): int
    {
        return $this->_sizeInBytes;
    }

    /**
     * Sets the size in bytes.
     *
     * @param int $sizeInBytes The size in bytes
     */
    public function setSizeInBytes(int $sizeInBytes): void
    {
        $this->_sizeInBytes = $sizeInBytes;
    }

    /**
     * Gets the message count.
     */
    public function getMessageCount(): int
    {
        return $this->_messageCount;
    }

    public function setMessageCount(int $messageCount): void
    {
        $this->_messageCount = $messageCount;
    }
}
