<?php

declare(strict_types=1);

/**
 * LICENSE: Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * PHP version 5
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @see      https://github.com/WindowsAzure/azure-sdk-for-php
 */

namespace WindowsAzure\ServiceBus\Models;

use WindowsAzure\Common\Internal\Atom\Content;
use WindowsAzure\Common\Internal\Atom\Entry;
use WindowsAzure\Common\Internal\Resources;
use WindowsAzure\Common\Internal\Serialization\XmlSerializer;
use WindowsAzure\Common\Internal\Validate;
use WindowsAzure\ServiceBus\Internal\Action;
use WindowsAzure\ServiceBus\Internal\Filter;

/**
 * The information of a rule.
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @version   Release: 0.5.0_2016-11
 *
 * @see      https://github.com/WindowsAzure/azure-sdk-for-php
 */
class RuleInfo
{
    /**
     * The entry of the rule info.
     */
    private Entry $_entry;

    /**
     * The description of the rule.
     */
    private RuleDescription $_ruleDescription;

    /**
     * Creates an RuleInfo with specified parameters.
     *
     * @param string           $title           The title of the rule
     * @param ?RuleDescription $ruleDescription The description of the rule
     */
    public function __construct(
        string $title = Resources::EMPTY_STRING,
        RuleDescription $ruleDescription = null
    ) {
        Validate::isString($title, 'title');

        if (null === $ruleDescription) {
            $ruleDescription = new RuleDescription();
            $ruleDescription->setFilter(new TrueFilter());
            $ruleDescription->setAction(new EmptyRuleAction());
        }
        $this->_ruleDescription = $ruleDescription;
        $this->_entry = new Entry();
        $this->_entry->setTitle($title);
        $this->_entry->setAttribute(
            Resources::XMLNS,
            Resources::SERVICE_BUS_NAMESPACE
        );
    }

    /**
     * Populates the properties with a specified XML string based on ATOM
     * ENTRY schema.
     *
     * @param string $xmlString An XML string representing a rule info instance
     */
    public function parseXml(string $xmlString): void
    {
        $this->_entry->parseXml($xmlString);
        $content = $this->_entry->getContent();
        if (empty($content->getText())) {
            $this->_ruleDescription = new RuleDescription();
        } else {
            $this->_ruleDescription = RuleDescription::create($content->getText());
        }
    }

    /**
     * Writes an XML string representing the rule info instance.
     *
     * @param \XMLWriter $xmlWriter The XML writer
     */
    public function writeXml(\XMLWriter $xmlWriter): void
    {
        $content = null;
        if (null !== $this->_ruleDescription) {
            $content = new Content();
            $content->setText(
                XmlSerializer::objectSerialize(
                    $this->_ruleDescription,
                    'RuleDescription'
                )
            );
        }
        $this->_entry->setContent($content);
        $this->_entry->writeXml($xmlWriter);
    }

    /**
     * Gets the entry.
     */
    public function getEntry(): Entry
    {
        return $this->_entry;
    }

    /**
     * Sets the entry.
     *
     * @param Entry $entry The entry of the queue info
     */
    public function setEntry(Entry $entry): void
    {
        $this->_entry = $entry;
    }

    /**
     * Gets the title.
     */
    public function getTitle(): string
    {
        return $this->_entry->getTitle();
    }

    /**
     * Sets the title.
     *
     * @param string $title The title of the rule info
     */
    public function setTitle(string $title): void
    {
        $this->_entry->setTitle($title);
    }

    /**
     * Gets the filter.
     */
    public function getFilter(): Filter
    {
        return $this->_ruleDescription->getFilter();
    }

    /**
     * Sets the filter.
     *
     * @param Filter $filter The filter
     */
    public function setFilter(Filter $filter): void
    {
        $this->_ruleDescription->setFilter($filter);
    }

    /**
     * Gets the action.
     */
    public function getAction(): Action
    {
        return $this->_ruleDescription->getAction();
    }

    /**
     * Sets the action.
     *
     * @param Action $action The action
     */
    public function setAction(Action $action): void
    {
        $this->_ruleDescription->setAction($action);
    }

    /**
     * Gets the description of the rule.
     */
    public function getRuleDescription(): RuleDescription
    {
        return $this->_ruleDescription;
    }

    /**
     * Sets the rule description.
     *
     * @param RuleDescription $ruleDescription The description of the rule
     */
    public function setRuleDescription(RuleDescription $ruleDescription): void
    {
        $this->_ruleDescription = $ruleDescription;
    }

    /**
     * With correlation ID filter.
     *
     * @param string $correlationId The ID of the correlation
     */
    public function withCorrelationFilter(string $correlationId): void
    {
        $filter = new CorrelationFilter();
        $filter->setCorrelationId($correlationId);
        $this->_ruleDescription->setFilter($filter);
    }

    /**
     * With sql expression filter.
     *
     * @param string $sqlExpression The SQL expression of the filter
     */
    public function withSqlFilter(string $sqlExpression): void
    {
        $filter = new SqlFilter();
        $filter->setSqlExpression($sqlExpression);
        $filter->setCompatibilityLevel(20);
        $this->_ruleDescription->setFilter($filter);
    }

    /**
     * With true filter.
     */
    public function withTrueFilter(): void
    {
        $filter = new TrueFilter();
        $this->_ruleDescription->setFilter($filter);
    }

    /**
     * With false filter.
     */
    public function withFalseFilter(): void
    {
        $filter = new FalseFilter();
        $this->_ruleDescription->setFilter($filter);
    }

    /**
     * With empty rule action.
     */
    public function withEmptyRuleAction(): void
    {
        $action = new EmptyRuleAction();
        $this->_ruleDescription->setAction($action);
    }

    /**
     * With SQL rule action.
     *
     * @param string $sqlExpression The SQL expression
     *                              of the rule action
     */
    public function withSqlRuleAction(string $sqlExpression): void
    {
        $action = new SqlRuleAction();
        $action->setCompatibilityLevel('20');
        $action->setSqlExpression($sqlExpression);
        $this->_ruleDescription->setAction($action);
    }

    /**
     * Gets the name of the rule description.
     */
    public function getName(): string
    {
        return $this->_ruleDescription->getName();
    }

    /**
     * Sets the name of the rule description.
     *
     * @param string $name The name of the rule description
     */
    public function setName(string $name): void
    {
        $this->_ruleDescription->setName($name);
    }
}
