<?php

declare(strict_types=1);

/**
 * LICENSE: Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * PHP version 5
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @see      https://github.com/WindowsAzure/azure-sdk-for-php
 */

namespace WindowsAzure\ServiceBus\Models;

/**
 * The subscription description.
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @version   Release: 0.5.0_2016-11
 *
 * @see      http://msdn.microsoft.com/en-us/library/windowsazure/hh780763
 */
class SubscriptionDescription
{
    /**
     * The duration of the lock.
     */
    private int $_lockDuration = 0;

    /**
     * Requires session.
     */
    private bool $_requiresSession = false;

    /**
     * The default message time to live.
     */
    private string $_defaultMessageTimeToLive = '';

    /**
     * The dead lettering on message expiration.
     */
    private string $_deadLetteringOnMessageExpiration = '';

    /**
     * The dead lettering on filter evaluation exception.
     */
    private string $_deadLetteringOnFilterEvaluationExceptions = '';

    /**
     * The description of the default rule.
     */
    private string $_defaultRuleDescription = '';

    /**
     * The count of the message.
     */
    private int $_messageCount = 0;

    /**
     * The count of the delivery.
     */
    private int $_maxDeliveryCount = 0;

    /**
     * Enables Batched operations.
     */
    private bool $_enableBatchedOperations = false;

    /**
     * Creates a subscription description instance with default
     * parameter.
     */
    public function __construct()
    {
    }

    /**
     * Creates a subscription description with specified XML string.
     *
     * @param string $subscriptionDescriptionXml An XML based subscription
     *                                           description
     */
    public static function create(string $subscriptionDescriptionXml): self
    {
        $subscriptionDescription = new self();
        $root = simplexml_load_string(
            $subscriptionDescriptionXml
        );
        $subscriptionDescriptionArray = (array) $root;
        if (\array_key_exists('LockDuration', $subscriptionDescriptionArray)) {
            $subscriptionDescription->setLockDuration(
                (int) $subscriptionDescriptionArray['LockDuration']
            );
        }

        if (\array_key_exists('RequiresSession', $subscriptionDescriptionArray)) {
            $subscriptionDescription->setRequiresSession(
                (bool) $subscriptionDescriptionArray['RequiresSession']
            );
        }

        if (\array_key_exists(
            'DefaultMessageTimeToLive',
            $subscriptionDescriptionArray
        )
        ) {
            $subscriptionDescription->setDefaultMessageTimeToLive(
                (string) $subscriptionDescriptionArray['DefaultMessageTimeToLive']
            );
        }

        if (\array_key_exists(
            'DeadLetteringOnMessageExpiration',
            $subscriptionDescriptionArray
        )
        ) {
            $subscriptionDescription->setDeadLetteringOnMessageExpiration(
                (string) $subscriptionDescriptionArray[
                'DeadLetteringOnMessageExpiration'
                ]
            );
        }

        if (\array_key_exists(
            'DeadLetteringOnFilterEvaluationException',
            $subscriptionDescriptionArray
        )
        ) {
            $subscriptionDescription->setDeadLetteringOnFilterEvaluationExceptions(
                (string) $subscriptionDescriptionArray[
                    'DeadLetteringOnFilterEvaluationException'
                ]
            );
        }

        if (\array_key_exists(
            'DefaultRuleDescription',
            $subscriptionDescriptionArray
        )
        ) {
            $subscriptionDescription->setDefaultRuleDescription(
                (string) $subscriptionDescriptionArray['DefaultRuleDescription']
            );
        }

        if (\array_key_exists('MessageCount', $subscriptionDescriptionArray)) {
            $subscriptionDescription->setMessageCount(
                (int) $subscriptionDescriptionArray['MessageCount']
            );
        }

        if (\array_key_exists('MaxDeliveryCount', $subscriptionDescriptionArray)) {
            $subscriptionDescription->setMaxDeliveryCount(
                (int) $subscriptionDescriptionArray['MaxDeliveryCount']
            );
        }

        if (\array_key_exists(
            'EnableBatchedOperations',
            $subscriptionDescriptionArray
        )
        ) {
            $subscriptionDescription->setEnableBatchedOperations(
                (bool) $subscriptionDescriptionArray['EnableBatchedOperations']
            );
        }

        return $subscriptionDescription;
    }

    public function getLockDuration(): int
    {
        return $this->_lockDuration;
    }

    public function setLockDuration(int $lockDuration): void
    {
        $this->_lockDuration = $lockDuration;
    }

    /**
     * Gets requires session.
     */
    public function getRequiresSession(): bool
    {
        return $this->_requiresSession;
    }

    /**
     * Sets the requires session.
     *
     * @param bool $requiresSession The requires session
     */
    public function setRequiresSession(bool $requiresSession): void
    {
        $this->_requiresSession = $requiresSession;
    }

    /**
     * Gets default message time to live.
     */
    public function getDefaultMessageTimeToLive(): string
    {
        return $this->_defaultMessageTimeToLive;
    }

    /**
     * Sets default message time to live.
     *
     * @param string $defaultMessageTimeToLive The default message time to live
     */
    public function setDefaultMessageTimeToLive(string $defaultMessageTimeToLive): void
    {
        $this->_defaultMessageTimeToLive = $defaultMessageTimeToLive;
    }

    /**
     * Gets dead lettering on message expiration.
     */
    public function getDeadLetteringOnMessageExpiration(): string
    {
        return $this->_deadLetteringOnMessageExpiration;
    }

    /**
     * Sets dead lettering on message expiration.
     *
     * @param string $deadLetteringOnMessageExpiration The dead lettering
     *                                                 on message expiration
     */
    public function setDeadLetteringOnMessageExpiration(
        string $deadLetteringOnMessageExpiration
    ): void {
        $this->_deadLetteringOnMessageExpiration = $deadLetteringOnMessageExpiration;
    }

    /**
     * Gets dead lettering on filter evaluation exceptions.
     */
    public function getDeadLetteringOnFilterEvaluationExceptions(): string
    {
        return $this->_deadLetteringOnFilterEvaluationExceptions;
    }

    /**
     * Sets dead lettering on filter evaluation exceptions.
     *
     * @param string $deadLetteringOnFilterEvaluationExceptions Sets dead lettering
     *                                                          on filter evaluation exceptions
     */
    public function setDeadLetteringOnFilterEvaluationExceptions(
        string $deadLetteringOnFilterEvaluationExceptions
    ): void {
        $value = $deadLetteringOnFilterEvaluationExceptions;

        $this->_deadLetteringOnFilterEvaluationExceptions = $value;
    }

    /**
     * Gets the default rule description.
     */
    public function getDefaultRuleDescription(): string
    {
        return $this->_defaultRuleDescription;
    }

    /**
     * Sets the default rule description.
     *
     * @param string $defaultRuleDescription The default rule description
     */
    public function setDefaultRuleDescription(string $defaultRuleDescription): void
    {
        $this->_defaultRuleDescription = $defaultRuleDescription;
    }

    public function getMessageCount(): int
    {
        return $this->_messageCount;
    }

    public function setMessageCount(int $messageCount): void
    {
        $this->_messageCount = $messageCount;
    }

    /**
     * Gets maximum delivery count.
     */
    public function getMaxDeliveryCount(): int
    {
        return $this->_maxDeliveryCount;
    }

    /**
     * Sets maximum delivery count.
     *
     * @param int $maxDeliveryCount The maximum delivery count
     */
    public function setMaxDeliveryCount(int $maxDeliveryCount): void
    {
        $this->_maxDeliveryCount = $maxDeliveryCount;
    }

    /**
     * Gets enable batched operations.
     */
    public function getEnableBatchedOperations(): bool
    {
        return $this->_enableBatchedOperations;
    }

    /**
     * Sets enable batched operations.
     *
     * @param bool $enableBatchedOperations Enable batched operations
     */
    public function setEnableBatchedOperations(bool $enableBatchedOperations): void
    {
        $this->_enableBatchedOperations = $enableBatchedOperations;
    }
}
