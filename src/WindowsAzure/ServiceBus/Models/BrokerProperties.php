<?php

declare(strict_types=1);
/**
 * LICENSE: Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * PHP version 5
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @see      https://github.com/WindowsAzure/azure-sdk-for-php
 */

namespace WindowsAzure\ServiceBus\Models;

use WindowsAzure\Common\Internal\Resources;
use WindowsAzure\Common\Internal\Validate;

/**
 * The properties of the broker of a brokered message.
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @version   Release: 0.5.0_2016-11
 *
 * @see      https://github.com/WindowsAzure/azure-sdk-for-php
 */
class BrokerProperties
{
    /**
     * The correlation ID.
     */
    private ?string $_correlationId = null;

    /**
     * The session ID.
     */
    private ?string $_sessionId = null;

    /**
     * The delivery count.
     */
    private ?int $_deliveryCount = null;

    /**
     * The locked until time.
     */
    private ?\DateTime $_lockedUntilUtc = null;

    /**
     * The lock token.
     */
    private ?string $_lockToken = null;

    /**
     * The message Id.
     */
    private string $_messageId = '';

    /**
     * The label.
     */
    private ?string $_label = '';

    /**
     * The reply to.
     */
    private string $_replyTo = '';

    private ?int $_sequenceNumber = null;

    /**
     * The time to live.
     */
    private ?float $_timeToLive = null;

    /**
     * The to.
     */
    private ?string $_to = null;

    /**
     * The scheduled enqueue time.
     */
    private ?\DateTime $_scheduledEnqueueTimeUtc = null;

    /**
     * The reply to session ID.
     */
    private ?string $_replyToSessionId = null;

    /**
     * The location of the message.
     */
    private ?string $_messageLocation = null;

    /**
     * The location of the lock.
     */
    private ?string $_lockLocation = null;

    /**
     * Creates a broker properties instance with default parameters.
     */
    public function __construct()
    {
    }

    // @codingStandardsIgnoreStart

    /**
     * Creates a broker properties instance with specified JSON message.
     *
     * @param string $brokerPropertiesJson A JSON message representing a
     *                                     broker properties
     */
    public static function create(string $brokerPropertiesJson): self
    {
        Validate::isString($brokerPropertiesJson, 'brokerPropertiesJson');

        $brokerProperties = new self();

        $brokerPropertiesArray = json_decode($brokerPropertiesJson, true, 512, \JSON_THROW_ON_ERROR);

        if (\array_key_exists('CorrelationId', $brokerPropertiesArray)) {
            $brokerProperties->setCorrelationId(
                $brokerPropertiesArray['CorrelationId']
            );
        }

        if (\array_key_exists('SessionId', $brokerPropertiesArray)) {
            $brokerProperties->setSessionId(
                $brokerPropertiesArray['SessionId']
            );
        }

        if (\array_key_exists('DeliveryCount', $brokerPropertiesArray)) {
            $brokerProperties->setDeliveryCount(
                $brokerPropertiesArray['DeliveryCount']
            );
        }

        if (\array_key_exists('LockedUntilUtc', $brokerPropertiesArray)) {
            $brokerProperties->setLockedUntilUtc(
                /* @phpstan-ignore-next-line */
                \DateTime::createFromFormat(
                    Resources::AZURE_DATE_FORMAT,
                    $brokerPropertiesArray['LockedUntilUtc']
                )
            );
        }

        if (\array_key_exists('LockToken', $brokerPropertiesArray)) {
            $brokerProperties->setLockToken(
                $brokerPropertiesArray['LockToken']
            );
        }

        if (\array_key_exists('MessageId', $brokerPropertiesArray)) {
            $brokerProperties->setMessageId(
                $brokerPropertiesArray['MessageId']
            );
        }

        if (\array_key_exists('Label', $brokerPropertiesArray)) {
            $brokerProperties->setLabel($brokerPropertiesArray['Label']);
        }

        if (\array_key_exists('ReplyTo', $brokerPropertiesArray)) {
            $brokerProperties->setReplyTo($brokerPropertiesArray['ReplyTo']);
        }

        if (\array_key_exists('SequenceNumber', $brokerPropertiesArray)) {
            $brokerProperties->setSequenceNumber(
                $brokerPropertiesArray['SequenceNumber']
            );
        }

        if (\array_key_exists('TimeToLive', $brokerPropertiesArray)) {
            $brokerProperties->setTimeToLive(
                (float) ($brokerPropertiesArray['TimeToLive'])
            );
        }

        if (\array_key_exists('To', $brokerPropertiesArray)) {
            $brokerProperties->setTo($brokerPropertiesArray['To']);
        }

        if (\array_key_exists(
            'ScheduledEnqueueTimeUtc',
            $brokerPropertiesArray
        )
        ) {
            $brokerProperties->setScheduledEnqueueTimeUtc(
                /* @phpstan-ignore-next-line */
                \DateTime::createFromFormat(
                    Resources::AZURE_DATE_FORMAT,
                    $brokerPropertiesArray['ScheduledEnqueueTimeUtc']
                )
            );
        }

        if (\array_key_exists('ReplyToSessionId', $brokerPropertiesArray)) {
            $brokerProperties->setReplyToSessionId(
                $brokerPropertiesArray['ReplyToSessionId']
            );
        }

        if (\array_key_exists('MessageLocation', $brokerPropertiesArray)) {
            $brokerProperties->setMessageLocation(
                $brokerPropertiesArray['MessageLocation']
            );
        }

        if (\array_key_exists('LockLocation', $brokerPropertiesArray)) {
            $brokerProperties->setLockLocation(
                $brokerPropertiesArray['LockLocation']
            );
        }

        return $brokerProperties;
    }

    // @codingStandardsIgnoreEnd

    /**
     * Gets the correlation ID.
     */
    public function getCorrelationId(): ?string
    {
        return $this->_correlationId;
    }

    /**
     * Sets the correlation ID.
     *
     * @param string $correlationId The correlation ID
     */
    public function setCorrelationId(string $correlationId): void
    {
        $this->_correlationId = $correlationId;
    }

    /**
     * Gets the session ID.
     */
    public function getSessionId(): ?string
    {
        return $this->_sessionId;
    }

    /**
     * Sets the session ID.
     *
     * @param string $sessionId The ID of the session
     */
    public function setSessionId(string $sessionId): void
    {
        $this->_sessionId = $sessionId;
    }

    /**
     * Gets the delivery count.
     */
    public function getDeliveryCount(): ?int
    {
        return $this->_deliveryCount;
    }

    /**
     * Sets the delivery count.
     *
     * @param int $deliveryCount The count of the delivery
     */
    public function setDeliveryCount(int $deliveryCount): void
    {
        $this->_deliveryCount = $deliveryCount;
    }

    /**
     * Gets the locked until time.
     */
    public function getLockedUntilUtc(): ?\DateTime
    {
        return $this->_lockedUntilUtc;
    }

    /**
     * Sets the locked until time.
     *
     * @param \DateTime $lockedUntilUtc The locked until time
     */
    public function setLockedUntilUtc(\DateTime $lockedUntilUtc): void
    {
        $this->_lockedUntilUtc = $lockedUntilUtc;
    }

    public function getLockToken(): ?string
    {
        return $this->_lockToken;
    }

    /**
     * Sets the lock token.
     *
     * @param string $lockToken The lock token
     */
    public function setLockToken(string $lockToken): void
    {
        $this->_lockToken = $lockToken;
    }

    /**
     * Gets the message ID.
     */
    public function getMessageId(): string
    {
        return $this->_messageId;
    }

    /**
     * Sets the message ID.
     *
     * @param string $messageId The ID of the message
     */
    public function setMessageId(string $messageId): void
    {
        $this->_messageId = $messageId;
    }

    /**
     * Gets the label.
     */
    public function getLabel(): string
    {
        return $this->_label ?? '';
    }

    /**
     * Sets the label.
     *
     * @param string $label The label of the broker property
     */
    public function setLabel(?string $label): void
    {
        $this->_label = $label ?? '';
    }

    /**
     * Gets the reply to.
     */
    public function getReplyTo(): string
    {
        return $this->_replyTo;
    }

    /**
     * Sets the reply to.
     *
     * @param string $replyTo The reply to
     */
    public function setReplyTo(string $replyTo): void
    {
        $this->_replyTo = $replyTo;
    }

    /**
     * Gets the sequence number.
     */
    public function getSequenceNumber(): ?int
    {
        return $this->_sequenceNumber;
    }

    /**
     * Sets the sequence number.
     *
     * @param int $sequenceNumber The sequence number
     */
    public function setSequenceNumber(int $sequenceNumber): void
    {
        $this->_sequenceNumber = $sequenceNumber;
    }

    /**
     * Gets time to live.
     */
    public function getTimeToLive(): ?float
    {
        return $this->_timeToLive;
    }

    public function setTimeToLive(float $timeToLive): void
    {
        $this->_timeToLive = $timeToLive;
    }

    /**
     * Gets to.
     */
    public function getTo(): ?string
    {
        return $this->_to;
    }

    /**
     * Sets to.
     *
     * @param string $to To
     */
    public function setTo(string $to): void
    {
        $this->_to = $to;
    }

    /**
     * Gets scheduled enqueue time UTC.
     */
    public function getScheduledEnqueueTimeUtc(): ?\DateTime
    {
        return $this->_scheduledEnqueueTimeUtc;
    }

    /**
     * Sets scheduled enqueue time UTC.
     *
     * @param \DateTime $scheduledEnqueueTimeUtc The scheduled enqueue time
     */
    public function setScheduledEnqueueTimeUtc(\DateTime $scheduledEnqueueTimeUtc): void
    {
        $this->_scheduledEnqueueTimeUtc = $scheduledEnqueueTimeUtc;
    }

    /**
     * Gets reply to session ID.
     */
    public function getReplyToSessionId(): ?string
    {
        return $this->_replyToSessionId;
    }

    /**
     * Sets reply to session.
     *
     * @param string $replyToSessionId reply to session
     */
    public function setReplyToSessionId(string $replyToSessionId): void
    {
        $this->_replyToSessionId = $replyToSessionId;
    }

    /**
     * Gets message location.
     */
    public function getMessageLocation(): ?string
    {
        return $this->_messageLocation;
    }

    /**
     * Sets the location of the message.
     *
     * @param string $messageLocation The location of the message
     */
    public function setMessageLocation(string $messageLocation): void
    {
        $this->_messageLocation = $messageLocation;
    }

    /**
     * Gets the location of the lock.
     */
    public function getLockLocation(): ?string
    {
        return $this->_lockLocation;
    }

    /**
     * Sets the location of the lock.
     *
     * @param string $lockLocation The location of the lock
     */
    public function setLockLocation(string $lockLocation): void
    {
        $this->_lockLocation = $lockLocation;
    }

    /**
     * Gets a string representing the broker property.
     */
    public function toString(): string
    {
        $value = [];

        $this->setValueArrayString(
            $value,
            'CorrelationId',
            $this->_correlationId
        );

        $this->setValueArrayString(
            $value,
            'SessionId',
            $this->_sessionId
        );

        $this->setValueArrayInt(
            $value,
            'DeliveryCount',
            $this->_deliveryCount
        );

        $this->setValueArrayDateTime(
            $value,
            'LockedUntilUtc',
            $this->_lockedUntilUtc
        );

        $this->setValueArrayString(
            $value,
            'LockToken',
            $this->_lockToken
        );

        $this->setValueArrayString(
            $value,
            'MessageId',
            $this->_messageId
        );

        $this->setValueArrayString(
            $value,
            'Label',
            $this->_label
        );

        $this->setValueArrayString(
            $value,
            'ReplyTo',
            $this->_replyTo
        );

        $this->setValueArrayInt(
            $value,
            'SequenceNumber',
            $this->_sequenceNumber
        );

        $this->setValueArrayFloat(
            $value,
            'TimeToLive',
            $this->_timeToLive
        );

        $this->setValueArrayString(
            $value,
            'To',
            $this->_to
        );

        $this->setValueArrayDateTime(
            $value,
            'ScheduledEnqueueTimeUtc',
            $this->_scheduledEnqueueTimeUtc
        );

        $this->setValueArrayString(
            $value,
            'ReplyToSessionId',
            $this->_replyToSessionId
        );

        $this->setValueArrayString(
            $value,
            'MessageLocation',
            $this->_messageLocation
        );

        $this->setValueArrayString(
            $value,
            'LockLocation',
            $this->_lockLocation
        );

        return json_encode($value, \JSON_THROW_ON_ERROR);
    }

    public function setValueArrayString(array &$valueArray, string $key, ?string $value): void
    {
        Validate::isString($key, 'key');

        if (!empty($value) && null !== $value) {
            Validate::isString($value, 'value');
            $valueArray[$key] = $value;
        }
    }

    public function setValueArrayInt(array &$valueArray, string $key, ?int $value): void
    {
        Validate::isString($key, 'key');

        if (null !== $value) {
            $valueArray[$key] = $value;
        }
    }

    public function setValueArrayFloat(array &$valueArray, string $key, ?float $value): void
    {
        Validate::isString($key, 'key');

        if (null !== $value) {
            $valueArray[$key] = $value;
        }
    }

    public function setValueArrayDateTime(array &$valueArray, string $key, ?\DateTime $value): void
    {
        Validate::isString($key, 'key');

        if (null !== $value) {
            $valueArray[$key] = gmdate(
                Resources::AZURE_DATE_FORMAT,
                $value->getTimestamp()
            );
        }
    }
}
