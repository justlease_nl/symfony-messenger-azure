<?php

declare(strict_types=1);

/**
 * LICENSE: Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * PHP version 5
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @see      https://github.com/WindowsAzure/azure-sdk-for-php
 */

namespace WindowsAzure\ServiceBus\Models;

use WindowsAzure\Common\Internal\Atom\Content;
use WindowsAzure\Common\Internal\Atom\Entry;
use WindowsAzure\Common\Internal\Resources;
use WindowsAzure\Common\Internal\Serialization\XmlSerializer;
use WindowsAzure\Common\Internal\Validate;

/**
 * The description of a topic.
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @version   Release: 0.5.0_2016-11
 *
 * @see      https://github.com/WindowsAzure/azure-sdk-for-php
 */
class TopicInfo extends Entry
{
    /**
     * The entry of the topic info.
     */
    private Entry $_entry;

    private TopicDescription $_topicDescription;

    /**
     * Creates a TopicInfo with specified parameters.
     *
     * @param string            $title            The name of the topic
     * @param ?TopicDescription $topicDescription The description of the
     *                                            topic
     */
    public function __construct(
        string $title = Resources::EMPTY_STRING,
        TopicDescription $topicDescription = null
    ) {
        Validate::isString($title, 'title');
        if (null === $topicDescription) {
            $topicDescription = new TopicDescription();
        }
        $this->title = $title;
        $this->_topicDescription = $topicDescription;
        $this->_entry = new Entry();
        $this->_entry->setTitle($title);
        $this->_entry->setAttribute(
            Resources::XMLNS,
            Resources::SERVICE_BUS_NAMESPACE
        );
    }

    /**
     * Populates properties with a specified XML string.
     *
     * @param string $xmlString An XML string representing the topic information
     */
    public function parseXml(string $xmlString): void
    {
        $this->_entry->parseXml($xmlString);
        $content = $this->_entry->getContent();
        if (empty($content->getText())) {
            $this->_topicDescription = new TopicDescription();
        } else {
            $this->_topicDescription = TopicDescription::create($content->getText());
        }
    }

    /**
     * Writes an XML string.
     *
     * @param \XMLWriter $xmlWriter The XML writer
     */
    public function writeXml(\XMLWriter $xmlWriter): void
    {
        $content = null;
        if (null !== $this->_topicDescription) {
            $content = new Content();
            $content->setText(
                XmlSerializer::objectSerialize(
                    $this->_topicDescription,
                    'TopicDescription'
                )
            );
            $content->setType(Resources::XML_CONTENT_TYPE);
        }
        $this->_entry->setContent($content);
        $this->_entry->writeXml($xmlWriter);
    }

    /**
     * Gets the title.
     */
    public function getTitle(): string
    {
        return $this->_entry->getTitle();
    }

    /**
     * Sets the title.
     *
     * @param string $title The title of the queue info
     */
    public function setTitle(string $title): void
    {
        $this->_entry->setTitle($title);
    }

    /**
     * Gets the entry.
     */
    public function getEntry(): Entry
    {
        return $this->_entry;
    }

    /**
     * Sets the entry.
     *
     * @param Entry $entry The entry of the queue info
     */
    public function setEntry(Entry $entry): void
    {
        $this->_entry = $entry;
    }

    /**
     * Gets the descriptions of the topic.
     */
    public function getTopicDescription(): TopicDescription
    {
        return $this->_topicDescription;
    }

    /**
     * Sets the descriptions of the topic.
     *
     * @param TopicDescription $topicDescription The description of the topic
     */
    public function setTopicDescription(TopicDescription $topicDescription): void
    {
        $this->_topicDescription = $topicDescription;
    }

    /**
     * Gets default message time to live.
     */
    public function getDefaultMessageTimeToLive(): string
    {
        return $this->_topicDescription->getDefaultMessageTimeToLive();
    }

    /**
     * Sets the default message to live.
     *
     * @param string $defaultMessageTimeToLive The default message time to live
     */
    public function setDefaultMessageTimeToLive(string $defaultMessageTimeToLive): void
    {
        $this->_topicDescription->setDefaultMessageTimeToLive(
            $defaultMessageTimeToLive
        );
    }

    /**
     * Gets the msax size in mega bytes.
     */
    public function getMaxSizeInMegabytes(): int
    {
        return $this->_topicDescription->getMaxSizeInMegabytes();
    }

    /**
     * Sets max size in mega bytes.
     *
     * @param int $maxSizeInMegabytes The maximum size in mega bytes
     */
    public function setMaxSizeInMegabytes(int $maxSizeInMegabytes): void
    {
        $this->_topicDescription->setMaxSizeInMegabytes($maxSizeInMegabytes);
    }

    /**
     * Gets requires duplicate detection.
     */
    public function getRequiresDuplicateDetection(): bool
    {
        return $this->_topicDescription->getRequiresDuplicateDetection();
    }

    /**
     * Sets requires duplicate detection.
     *
     * @param bool $requiresDuplicateDetection Sets requires duplicate detection
     */
    public function setRequiresDuplicateDetection(bool $requiresDuplicateDetection): void
    {
        $this->_topicDescription->setRequiresDuplicateDetection(
            $requiresDuplicateDetection
        );
    }

    /**
     * Gets duplicate detection history time window.
     */
    public function getDuplicateDetectionHistoryTimeWindow(): string
    {
        return $this->_topicDescription->getDuplicateDetectionHistoryTimeWindow();
    }

    /**
     * Sets duplicate detection history time window.
     *
     * @param string $duplicateDetectionHistoryTimeWindow The duplicate
     *                                                    detection history time window
     */
    public function setDuplicateDetectionHistoryTimeWindow(
        string $duplicateDetectionHistoryTimeWindow
    ): void {
        $this->_topicDescription->setDuplicateDetectionHistoryTimeWindow(
            $duplicateDetectionHistoryTimeWindow
        );
    }

    /**
     * Gets enable batched operations.
     */
    public function getEnableBatchedOperations(): bool
    {
        return $this->_topicDescription->getEnableBatchedOperations();
    }

    /**
     * Sets enable batched operations.
     *
     * @param bool $enableBatchedOperations Enables batched operations
     */
    public function setEnableBatchedOperations(bool $enableBatchedOperations): void
    {
        $this->_topicDescription->setEnableBatchedOperations(
            $enableBatchedOperations
        );
    }

    /**
     * Gets size in bytes.
     */
    public function getSizeInBytes(): int
    {
        return $this->_topicDescription->getSizeInBytes();
    }

    /**
     * Sets size in bytes.
     *
     * @param int $sizeInBytes The size in bytes
     */
    public function setSizeInBytes(int $sizeInBytes): void
    {
        $this->_topicDescription->setSizeInBytes($sizeInBytes);
    }
}
