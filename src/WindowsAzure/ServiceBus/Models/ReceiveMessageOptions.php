<?php

declare(strict_types=1);
/**
 * LICENSE: Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * PHP version 5
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @see      https://github.com/WindowsAzure/azure-sdk-for-php
 */

namespace WindowsAzure\ServiceBus\Models;

/**
 * The options for a receive message request.
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @version   Release: 0.5.0_2016-11
 *
 * @see      https://github.com/WindowsAzure/azure-sdk-for-php
 */
class ReceiveMessageOptions
{
    /**
     * The timeout value of receiving message.
     */
    private int $_timeout = 0;

    /**
     * The mode of receiving message.
     */
    private int $_receiveMode = ReceiveMode::RECEIVE_AND_DELETE;

    /**
     * Gets the timeout of the receive message request.
     */
    public function getTimeout(): int
    {
        return $this->_timeout;
    }

    /**
     * Sets the timeout of the receive message request.
     *
     * @param int $timeout The timeout of the receive message request
     */
    public function setTimeout(int $timeout): void
    {
        $this->_timeout = $timeout;
    }

    /**
     * Gets the receive mode.
     */
    public function getReceiveMode(): int
    {
        return $this->_receiveMode;
    }

    /**
     * Sets the receive mode.
     *
     * @param int $receiveMode The mode of receiving the message
     */
    public function setReceiveMode(int $receiveMode): void
    {
        $this->_receiveMode = $receiveMode;
    }

    /**
     * Gets is receive and delete.
     */
    public function getIsReceiveAndDelete(): bool
    {
        return ReceiveMode::RECEIVE_AND_DELETE === $this->_receiveMode;
    }

    /**
     * Sets whether the mode of receiving is receive and delete.
     */
    public function setReceiveAndDelete(): void
    {
        $this->_receiveMode = ReceiveMode::RECEIVE_AND_DELETE;
    }

    /**
     * Gets peek lock.
     */
    public function getIsPeekLock(): bool
    {
        return ReceiveMode::PEEK_LOCK === $this->_receiveMode;
    }

    /**
     * Sets peek lock.
     */
    public function setPeekLock(): void
    {
        $this->_receiveMode = ReceiveMode::PEEK_LOCK;
    }
}
