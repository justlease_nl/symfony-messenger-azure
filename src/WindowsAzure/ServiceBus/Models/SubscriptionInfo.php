<?php

declare(strict_types=1);

/**
 * LICENSE: Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * PHP version 5
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @see      https://github.com/WindowsAzure/azure-sdk-for-php
 */

namespace WindowsAzure\ServiceBus\Models;

use WindowsAzure\Common\Internal\Atom\Content;
use WindowsAzure\Common\Internal\Atom\Entry;
use WindowsAzure\Common\Internal\Resources;
use WindowsAzure\Common\Internal\Serialization\XmlSerializer;
use WindowsAzure\Common\Internal\Validate;

/**
 * The information of a subscription.
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @version   Release: 0.5.0_2016-11
 *
 * @see      https://github.com/WindowsAzure/azure-sdk-for-php
 */
class SubscriptionInfo extends Entry
{
    /**
     * The entry of the subscription info.
     */
    private Entry $_entry;

    private SubscriptionDescription $_subscriptionDescription;

    /**
     * Creates a SubscriptionInfo instance with specified parameters.
     *
     * @param string                   $title                   The title of
     *                                                          the subscription
     * @param ?SubscriptionDescription $subscriptionDescription The description
     *                                                          of the subscription
     */
    public function __construct(
        string $title = Resources::EMPTY_STRING,
        SubscriptionDescription $subscriptionDescription = null
    ) {
        Validate::isString($title, 'title');
        if (null === $subscriptionDescription) {
            $subscriptionDescription = new SubscriptionDescription();
        }
        $this->_subscriptionDescription = $subscriptionDescription;
        $this->_entry = new Entry();
        $this->_entry->setTitle($title);
        $this->_entry->setAttribute(
            Resources::XMLNS,
            Resources::SERVICE_BUS_NAMESPACE
        );
    }

    /**
     * Populates the properties of the subscription info instance with a XML string.
     *
     * @param string $entryXml A XML string representing a subscription
     *                         information instance
     */
    public function parseXml(string $entryXml): void
    {
        $this->_entry->parseXml($entryXml);
        $content = $this->_entry->getContent();
        if (empty($content->getText())) {
            $this->_subscriptionDescription = new SubscriptionDescription();
        } else {
            $this->_subscriptionDescription = SubscriptionDescription::create(
                $content->getText()
            );
        }
    }

    /**
     * Writes XML based on the subscription information.
     *
     * @param \XMLWriter $xmlWriter The XML writer
     */
    public function writeXml(\XMLWriter $xmlWriter): void
    {
        $content = null;
        if (null !== $this->_subscriptionDescription) {
            $content = new Content();
            $content->setText(
                XmlSerializer::objectSerialize(
                    $this->_subscriptionDescription,
                    'SubscriptionDescription'
                )
            );
        }
        $this->_entry->setContent($content);
        $this->_entry->writeXml($xmlWriter);
    }

    /**
     * Gets the entry of the subscription info.
     */
    public function getEntry(): Entry
    {
        return $this->_entry;
    }

    /**
     * Sets the entry of the subscription info.
     *
     * @param Entry $entry The entry of the subscription info
     */
    public function setEntry(Entry $entry): void
    {
        $this->_entry = $entry;
    }

    /**
     * Gets the title.
     */
    public function getTitle(): string
    {
        return $this->_entry->getTitle();
    }

    /**
     * Sets the title.
     *
     * @param string $title The title of the queue info
     */
    public function setTitle(string $title): void
    {
        $this->_entry->setTitle($title);
    }

    /**
     * Gets the subscription description.
     */
    public function getSubscriptionDescription(): SubscriptionDescription
    {
        return $this->_subscriptionDescription;
    }

    /**
     * Sets the subscription description.
     *
     * @param SubscriptionDescription $subscriptionDescription The description of
     *                                                         the subscription
     */
    public function setSubscriptionDescription(SubscriptionDescription $subscriptionDescription): void
    {
        $this->_subscriptionDescription = $subscriptionDescription;
    }

    /**
     * Gets the lock duration.
     */
    public function getLockDuration(): int
    {
        return $this->_subscriptionDescription->getLockDuration();
    }

    public function setLockDuration(int $lockDuration): void
    {
        $this->_subscriptionDescription->setLockDuration($lockDuration);
    }

    /**
     * Gets requires session.
     */
    public function getRequiresSession(): bool
    {
        return $this->_subscriptionDescription->getRequiresSession();
    }

    /**
     * Sets the requires session.
     *
     * @param bool $requiresSession The requires session
     */
    public function setRequiresSession(bool $requiresSession): void
    {
        $this->_subscriptionDescription->setRequiresSession($requiresSession);
    }

    /**
     * Gets default message time to live.
     */
    public function getDefaultMessageTimeToLive(): string
    {
        return $this->_subscriptionDescription->getDefaultMessageTimeToLive();
    }

    /**
     * Sets default message time to live.
     *
     * @param string $defaultMessageTimeToLive The default message time to live
     */
    public function setDefaultMessageTimeToLive(string $defaultMessageTimeToLive): void
    {
        $this->_subscriptionDescription->setDefaultMessageTimeToLive(
            $defaultMessageTimeToLive
        );
    }

    /**
     * Gets dead lettering on message expiration.
     */
    public function getDeadLetteringOnMessageExpiration(): string
    {
        $subscriptionDesc = $this->_subscriptionDescription;

        return $subscriptionDesc->getDeadLetteringOnMessageExpiration();
    }

    /**
     * Sets dead lettering on message expiration.
     *
     * @param string $deadLetteringOnMessageExpiration The dead lettering
     *                                                 on message expiration
     */
    public function setDeadLetteringOnMessageExpiration(
        string $deadLetteringOnMessageExpiration
    ): void {
        $this->_subscriptionDescription->setDeadLetteringOnMessageExpiration(
            $deadLetteringOnMessageExpiration
        );
    }

    /**
     * Gets dead lettering on filter evaluation exceptions.
     */
    public function getDeadLetteringOnFilterEvaluationExceptions(): string
    {
        $subscriptionDesc = $this->_subscriptionDescription;

        return $subscriptionDesc->getDeadLetteringOnFilterEvaluationExceptions();
    }

    /**
     * Sets dead lettering on filter evaluation exceptions.
     *
     * @param string $deadLetteringOnFilterEvaluationExceptions Sets dead lettering
     *                                                          on filter evaluation exceptions
     */
    public function setDeadLetteringOnFilterEvaluationExceptions(
        string $deadLetteringOnFilterEvaluationExceptions
    ): void {
        $subscriptionDesc = $this->_subscriptionDescription;
        $subscriptionDesc->setDeadLetteringOnFilterEvaluationExceptions(
            $deadLetteringOnFilterEvaluationExceptions
        );
    }

    /**
     * Gets the default rule description.
     */
    public function getDefaultRuleDescription(): string
    {
        return $this->_subscriptionDescription->getDefaultRuleDescription();
    }

    /**
     * Sets the default rule description.
     *
     * @param string $defaultRuleDescription The default rule description
     */
    public function setDefaultRuleDescription(string $defaultRuleDescription): void
    {
        $this->_subscriptionDescription->setDefaultRuleDescription(
            $defaultRuleDescription
        );
    }

    /**
     * Gets the count of the message.
     */
    public function getMessageCount(): int
    {
        return $this->_subscriptionDescription->getMessageCount();
    }

    public function setMessageCount(int $messageCount): void
    {
        $this->_subscriptionDescription->setMessageCount($messageCount);
    }

    /**
     * Gets maximum delivery count.
     */
    public function getMaxDeliveryCount(): int
    {
        return $this->_subscriptionDescription->getMaxDeliveryCount();
    }

    /**
     * Sets maximum delivery count.
     *
     * @param int $maxDeliveryCount The maximum delivery count
     */
    public function setMaxDeliveryCount(int $maxDeliveryCount): void
    {
        $this->_subscriptionDescription->setMaxDeliveryCount($maxDeliveryCount);
    }

    /**
     * Gets enable batched operations.
     */
    public function getEnableBatchedOperations(): bool
    {
        return $this->_subscriptionDescription->getEnableBatchedOperations();
    }

    /**
     * Sets enable batched operations.
     *
     * @param bool $enableBatchedOperations Enable batched operations
     */
    public function setEnableBatchedOperations(bool $enableBatchedOperations): void
    {
        $this->_subscriptionDescription->setEnableBatchedOperations(
            $enableBatchedOperations
        );
    }
}
