<?php

declare(strict_types=1);

/**
 * LICENSE: Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * PHP version 5
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @see      https://github.com/WindowsAzure/azure-sdk-for-php
 */

namespace WindowsAzure\ServiceBus;

use Psr\Http\Message\ResponseInterface;
use WindowsAzure\Common\Internal\Atom\Content;
use WindowsAzure\Common\Internal\Atom\Entry;
use WindowsAzure\Common\Internal\Http\HttpCallContext;
use WindowsAzure\Common\Internal\Resources;
use WindowsAzure\Common\Internal\Serialization\XmlSerializer;
use WindowsAzure\Common\Internal\ServiceRestProxy;
use WindowsAzure\Common\Internal\Validate;
use WindowsAzure\ServiceBus\Internal\IServiceBusManagement;
use WindowsAzure\ServiceBus\Models\ListOptions;
use WindowsAzure\ServiceBus\Models\ListQueuesOptions;
use WindowsAzure\ServiceBus\Models\ListQueuesResult;
use WindowsAzure\ServiceBus\Models\ListRulesOptions;
use WindowsAzure\ServiceBus\Models\ListRulesResult;
use WindowsAzure\ServiceBus\Models\ListSubscriptionsOptions;
use WindowsAzure\ServiceBus\Models\ListSubscriptionsResult;
use WindowsAzure\ServiceBus\Models\ListTopicsOptions;
use WindowsAzure\ServiceBus\Models\ListTopicsResult;
use WindowsAzure\ServiceBus\Models\QueueInfo;
use WindowsAzure\ServiceBus\Models\RuleInfo;
use WindowsAzure\ServiceBus\Models\SubscriptionInfo;
use WindowsAzure\ServiceBus\Models\TopicInfo;

/**
 * This class constructs HTTP requests and receive HTTP responses
 * for Service Bus.
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @version   Release: 0.5.0_2016-11
 *
 * @see      https://github.com/WindowsAzure/azure-sdk-for-php
 */
class ServiceBusManagementRestProxy extends ServiceRestProxy implements IServiceBusManagement
{
    /**
     * Creates a queue with a specified queue information.
     *
     * @see http://msdn.microsoft.com/en-us/library/windowsazure/hh780716
     *
     * @param QueueInfo $queueInfo The information of the queue
     */
    public function createQueue(QueueInfo $queueInfo): QueueInfo
    {
        $httpCallContext = new HttpCallContext();
        $httpCallContext->setMethod(Resources::HTTP_PUT);
        $httpCallContext->setPath($queueInfo->getTitle());
        $httpCallContext->addHeader(
            Resources::CONTENT_TYPE,
            Resources::ATOM_ENTRY_CONTENT_TYPE
        );
        $httpCallContext->addStatusCode(Resources::STATUS_CREATED);

        $xmlWriter = new \XMLWriter();
        $xmlWriter->openMemory();
        $queueInfo->writeXml($xmlWriter);
        $body = $xmlWriter->outputMemory();
        $httpCallContext->setBody($body);

        $response = $this->sendHttpContext($httpCallContext);
        $queueInfo = new QueueInfo();
        $queueInfo->parseXml((string) $response->getBody());

        return $queueInfo;
    }

    /**
     * Deletes a queue.
     *
     * @see http://msdn.microsoft.com/en-us/library/windowsazure/hh780747
     *
     * @param string $queuePath The path of the queue
     */
    public function deleteQueue(string $queuePath): void
    {
        Validate::isString($queuePath, 'queuePath');
        Validate::notNullOrEmpty($queuePath, 'queuePath');

        $httpCallContext = new HttpCallContext();
        $httpCallContext->setMethod(Resources::HTTP_DELETE);
        $httpCallContext->addStatusCode(Resources::STATUS_OK);
        $httpCallContext->setPath($queuePath);

        $this->sendHttpContext($httpCallContext);
    }

    /**
     * Gets a queue with specified path.
     *
     * @see http://msdn.microsoft.com/en-us/library/windowsazure/hh780764
     *
     * @param string $queuePath The path of the queue
     */
    public function getQueue(string $queuePath): QueueInfo
    {
        $httpCallContext = new HttpCallContext();
        $httpCallContext->setPath($queuePath);
        $httpCallContext->setMethod(Resources::HTTP_GET);
        $httpCallContext->addStatusCode(Resources::STATUS_OK);
        $response = $this->sendHttpContext($httpCallContext);
        $queueInfo = new QueueInfo();
        $queueInfo->parseXml((string) $response->getBody());

        return $queueInfo;
    }

    /**
     * Lists a queue.
     *
     * @see http://msdn.microsoft.com/en-us/library/windowsazure/hh780759
     *
     * @param ListQueuesOptions|null $listQueueOptions The options to list the
     *                                                 queues
     */
    public function listQueues(ListQueuesOptions $listQueueOptions = null): ListQueuesResult
    {
        $response = $this->_listOptions(
            $listQueueOptions,
            Resources::LIST_QUEUES_PATH
        );

        $listQueuesResult = new ListQueuesResult();
        $listQueuesResult->parseXml((string) $response->getBody());

        return $listQueuesResult;
    }

    /**
     * The base method of all the list operations.
     */
    private function _listOptions(?ListOptions $listOptions, string $path): ResponseInterface
    {
        if (null === $listOptions) {
            $listOptions = new ListOptions();
        }

        $httpCallContext = new HttpCallContext();
        $httpCallContext->setMethod(Resources::HTTP_GET);
        $httpCallContext->setPath($path);
        $httpCallContext->addStatusCode(Resources::STATUS_OK);
        $top = $listOptions->getTop();
        $skip = $listOptions->getSkip();

        if (!empty($top)) {
            $httpCallContext->addQueryParameter(Resources::QP_TOP, (string) $top);
        }

        if (!empty($skip)) {
            $httpCallContext->addQueryParameter(Resources::QP_SKIP, (string) $skip);
        }

        return $this->sendHttpContext($httpCallContext);
    }

    /**
     * Creates a topic with specified topic info.
     *
     * @see http://msdn.microsoft.com/en-us/library/windowsazure/hh780728
     *
     * @param TopicInfo $topicInfo The information of the topic
     */
    public function createTopic(TopicInfo $topicInfo): TopicInfo
    {
        Validate::notNullOrEmpty($topicInfo, 'topicInfo');
        $httpCallContext = new HttpCallContext();
        $httpCallContext->setMethod(Resources::HTTP_PUT);
        $httpCallContext->setPath($topicInfo->getTitle());
        $httpCallContext->addHeader(
            Resources::CONTENT_TYPE,
            Resources::ATOM_ENTRY_CONTENT_TYPE
        );
        $httpCallContext->addStatusCode(Resources::STATUS_CREATED);

        $topicDescriptionXml = XmlSerializer::objectSerialize(
            $topicInfo->getTopicDescription(),
            'TopicDescription'
        );

        $entry = new Entry();
        $content = new Content($topicDescriptionXml);
        $content->setType(Resources::XML_CONTENT_TYPE);
        $entry->setContent($content);

        $entry->setAttribute(
            Resources::XMLNS,
            Resources::SERVICE_BUS_NAMESPACE
        );

        $xmlWriter = new \XMLWriter();
        $xmlWriter->openMemory();
        $entry->writeXml($xmlWriter);
        $httpCallContext->setBody($xmlWriter->outputMemory());

        $response = $this->sendHttpContext($httpCallContext);
        $topicInfo = new TopicInfo();
        $topicInfo->parseXml((string) $response->getBody());

        return $topicInfo;
    }

    /**
     * Deletes a topic with specified topic path.
     *
     * @see http://msdn.microsoft.com/en-us/library/windowsazure/hh780721
     *
     * @param string $topicPath The path of the topic
     */
    public function deleteTopic(string $topicPath): void
    {
        $httpCallContext = new HttpCallContext();
        $httpCallContext->setMethod(Resources::HTTP_DELETE);
        $httpCallContext->setPath($topicPath);
        $httpCallContext->addStatusCode(Resources::STATUS_OK);

        $this->sendHttpContext($httpCallContext);
    }

    /**
     * Gets a topic.
     *
     * @see http://msdn.microsoft.com/en-us/library/windowsazure/hh780769
     *
     * @param string $topicPath The path of the topic
     */
    public function getTopic(string $topicPath): TopicInfo
    {
        $httpCallContext = new HttpCallContext();
        $httpCallContext->setMethod(Resources::HTTP_GET);
        $httpCallContext->setPath($topicPath);
        $httpCallContext->addStatusCode(Resources::STATUS_OK);
        $response = $this->sendHttpContext($httpCallContext);
        $topicInfo = new TopicInfo();
        $topicInfo->parseXml((string) $response->getBody());

        return $topicInfo;
    }

    /**
     * Lists topics.
     *
     * @see http://msdn.microsoft.com/en-us/library/windowsazure/hh780744
     *
     * @param ListTopicsOptions $listTopicsOptions The options to list
     *                                             the topics
     */
    public function listTopics(ListTopicsOptions $listTopicsOptions = null): ListTopicsResult
    {
        $response = $this->_listOptions(
            $listTopicsOptions,
            Resources::LIST_TOPICS_PATH
        );

        $listTopicsResult = new ListTopicsResult();
        $listTopicsResult->parseXml((string) $response->getBody());

        return $listTopicsResult;
    }

    /**
     * Creates a subscription with specified topic path and
     * subscription info.
     *
     * @see http://msdn.microsoft.com/en-us/library/windowsazure/hh780748
     *
     * @param string           $topicPath        The path of
     *                                           the topic
     * @param SubscriptionInfo $subscriptionInfo The information
     *                                           of the subscription
     */
    public function createSubscription(string $topicPath, SubscriptionInfo $subscriptionInfo): SubscriptionInfo
    {
        $httpCallContext = new HttpCallContext();
        $httpCallContext->setMethod(Resources::HTTP_PUT);
        $subscriptionPath = sprintf(
            Resources::SUBSCRIPTION_PATH,
            $topicPath,
            $subscriptionInfo->getTitle()
        );
        $httpCallContext->setPath($subscriptionPath);
        $httpCallContext->addHeader(
            Resources::CONTENT_TYPE,
            Resources::ATOM_ENTRY_CONTENT_TYPE
        );
        $httpCallContext->addStatusCode(Resources::STATUS_CREATED);

        $subscriptionDescriptionXml = XmlSerializer::objectSerialize(
            $subscriptionInfo->getSubscriptionDescription(),
            'SubscriptionDescription'
        );

        $entry = new Entry();
        $content = new Content($subscriptionDescriptionXml);
        $content->setType(Resources::XML_CONTENT_TYPE);
        $entry->setContent($content);

        $entry->setAttribute(
            Resources::XMLNS,
            Resources::SERVICE_BUS_NAMESPACE
        );

        $xmlWriter = new \XMLWriter();
        $xmlWriter->openMemory();
        $entry->writeXml($xmlWriter);
        $httpCallContext->setBody($xmlWriter->outputMemory());

        $response = $this->sendHttpContext($httpCallContext);
        $subscriptionInfo = new SubscriptionInfo();
        $subscriptionInfo->parseXml((string) $response->getBody());

        return $subscriptionInfo;
    }

    /**
     * Deletes a subscription.
     *
     * @see http://msdn.microsoft.com/en-us/library/windowsazure/hh780740
     *
     * @param string $topicPath        The path of the topic
     * @param string $subscriptionName The name of the subscription
     */
    public function deleteSubscription(string $topicPath, string $subscriptionName): void
    {
        $httpCallContext = new HttpCallContext();
        $httpCallContext->setMethod(Resources::HTTP_DELETE);
        $httpCallContext->addStatusCode(Resources::STATUS_OK);
        $subscriptionPath = sprintf(
            Resources::SUBSCRIPTION_PATH,
            $topicPath,
            $subscriptionName
        );
        $httpCallContext->setPath($subscriptionPath);
        $this->sendHttpContext($httpCallContext);
    }

    /**
     * Gets a subscription.
     *
     * @see http://msdn.microsoft.com/en-us/library/windowsazure/hh780741
     *
     * @param string $topicPath        The path of the topic
     * @param string $subscriptionName The name of the subscription
     */
    public function getSubscription(string $topicPath, string $subscriptionName): SubscriptionInfo
    {
        $httpCallContext = new HttpCallContext();
        $httpCallContext->setMethod(Resources::HTTP_GET);
        $httpCallContext->addStatusCode(Resources::STATUS_OK);
        $subscriptionPath = sprintf(
            Resources::SUBSCRIPTION_PATH,
            $topicPath,
            $subscriptionName
        );
        $httpCallContext->setPath($subscriptionPath);
        $response = $this->sendHttpContext($httpCallContext);
        $subscriptionInfo = new SubscriptionInfo();
        $subscriptionInfo->parseXml((string) $response->getBody());

        return $subscriptionInfo;
    }

    /**
     * Lists subscription.
     *
     * @see http://msdn.microsoft.com/en-us/library/windowsazure/hh780766
     *
     * @param string                        $topicPath                The path of
     *                                                                the topic
     * @param ListSubscriptionsOptions|null $listSubscriptionsOptions The options
     *                                                                to list the subscription
     */
    public function listSubscriptions(
        string $topicPath,
        ListSubscriptionsOptions $listSubscriptionsOptions = null
    ): ListSubscriptionsResult {
        $listSubscriptionsPath = sprintf(
            Resources::LIST_SUBSCRIPTIONS_PATH,
            $topicPath
        );
        $response = $this->_listOptions(
            $listSubscriptionsOptions,
            $listSubscriptionsPath
        );
        $listSubscriptionsResult = new ListSubscriptionsResult();
        $listSubscriptionsResult->parseXml((string) $response->getBody());

        return $listSubscriptionsResult;
    }

    /**
     * Creates a rule.
     *
     * @see http://msdn.microsoft.com/en-us/library/windowsazure/hh780774
     *
     * @param string   $topicPath        The path of the topic
     * @param string   $subscriptionName The name of the subscription
     * @param RuleInfo $ruleInfo         The information of the rule
     */
    public function createRule(string $topicPath, string $subscriptionName, RuleInfo $ruleInfo): RuleInfo
    {
        $httpCallContext = new HttpCallContext();
        $httpCallContext->setMethod(Resources::HTTP_PUT);
        $httpCallContext->addStatusCode(Resources::STATUS_CREATED);
        $httpCallContext->addHeader(
            Resources::CONTENT_TYPE,
            Resources::ATOM_ENTRY_CONTENT_TYPE
        );
        $rulePath = sprintf(
            Resources::RULE_PATH,
            $topicPath,
            $subscriptionName,
            $ruleInfo->getTitle()
        );

        $ruleDescriptionXml = XmlSerializer::objectSerialize(
            $ruleInfo->getRuleDescription(),
            'RuleDescription'
        );

        $entry = new Entry();
        $content = new Content($ruleDescriptionXml);
        $content->setType(Resources::XML_CONTENT_TYPE);
        $entry->setContent($content);

        $entry->setAttribute(
            Resources::XMLNS,
            Resources::SERVICE_BUS_NAMESPACE
        );

        $xmlWriter = new \XMLWriter();
        $xmlWriter->openMemory();
        $entry->writeXml($xmlWriter);
        $httpCallContext->setBody($xmlWriter->outputMemory());

        $httpCallContext->setPath($rulePath);
        $response = $this->sendHttpContext($httpCallContext);
        $ruleInfo = new ruleInfo();
        $ruleInfo->parseXml((string) $response->getBody());

        return $ruleInfo;
    }

    /**
     * Deletes a rule.
     *
     * @see http://msdn.microsoft.com/en-us/library/windowsazure/hh780751
     *
     * @param string $topicPath        The path of the topic
     * @param string $subscriptionName The name of the subscription
     * @param string $ruleName         The name of the rule
     */
    public function deleteRule(string $topicPath, string $subscriptionName, string $ruleName): void
    {
        $httpCallContext = new HttpCallContext();
        $httpCallContext->addStatusCode(Resources::STATUS_OK);
        $httpCallContext->setMethod(Resources::HTTP_DELETE);
        $rulePath = sprintf(
            Resources::RULE_PATH,
            $topicPath,
            $subscriptionName,
            $ruleName
        );
        $httpCallContext->setPath($rulePath);
        $this->sendHttpContext($httpCallContext);
    }

    /**
     * Gets a rule.
     *
     * @see http://msdn.microsoft.com/en-us/library/windowsazure/hh780772
     *
     * @param string $topicPath        The path of the topic
     * @param string $subscriptionName The name of the subscription
     * @param string $ruleName         The name of the rule
     */
    public function getRule(string $topicPath, string $subscriptionName, string $ruleName): RuleInfo
    {
        $httpCallContext = new HttpCallContext();
        $httpCallContext->setMethod(Resources::HTTP_GET);
        $httpCallContext->addStatusCode(Resources::STATUS_OK);
        $rulePath = sprintf(
            Resources::RULE_PATH,
            $topicPath,
            $subscriptionName,
            $ruleName
        );
        $httpCallContext->setPath($rulePath);
        $response = $this->sendHttpContext($httpCallContext);
        $ruleInfo = new RuleInfo();
        $ruleInfo->parseXml((string) $response->getBody());

        return $ruleInfo;
    }

    /**
     * Lists rules.
     *
     * @see http://msdn.microsoft.com/en-us/library/windowsazure/hh780732
     *
     * @param string                $topicPath        The path of the topic
     * @param string                $subscriptionName The name of the subscription
     * @param ListRulesOptions|null $listRulesOptions The options to list the rules
     */
    public function listRules(
        string $topicPath,
        string $subscriptionName,
        ListRulesOptions $listRulesOptions = null
    ): ListRulesResult {
        $listRulesPath = sprintf(
            Resources::LIST_RULES_PATH,
            $topicPath,
            $subscriptionName
        );

        $response = $this->_listOptions(
            $listRulesOptions,
            $listRulesPath
        );

        $listRulesResult = new ListRulesResult();
        $listRulesResult->parseXml((string) $response->getBody());

        return $listRulesResult;
    }
}
