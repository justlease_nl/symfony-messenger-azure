<?php

declare(strict_types=1);

/**
 * LICENSE: Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * PHP version 5
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @see      https://github.com/windowsazure/azure-sdk-for-php
 */

namespace WindowsAzure\Common\Internal\Filters;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use WindowsAzure\Common\Internal\IServiceFilter;
use WindowsAzure\Common\Internal\Resources;
use WindowsAzure\Common\Internal\Utilities;

/**
 * Adds SAS authentication header to the http request object.
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @version   Release: 0.5.0_2016-11
 *
 * @see      https://github.com/windowsazure/azure-sdk-for-php
 */
class SASFilter implements IServiceFilter
{
    private string $sharedAccessKeyName;
    private string $sharedAccessKey;

    public function __construct(
        string $sharedAccessKeyName,
        string $sharedAccessKey
    ) {
        $this->sharedAccessKeyName = $sharedAccessKeyName;
        $this->sharedAccessKey = $sharedAccessKey;
    }

    /**
     * Adds SAS authentication header to the request headers.
     */
    public function handleRequest(RequestInterface $request): RequestInterface
    {
        $token = $this->getAuthorization(
            $request->getUri()->__toString(),
            $this->sharedAccessKeyName,
            $this->sharedAccessKey
        );

        $request = $request->withAddedHeader(Resources::AUTHENTICATION, $token);

        return $request;
    }

    private function getAuthorization(string $url, string $sharedAccessKeyName, string $sharedAccessKey): string
    {
        $expiry = time() + 3600;
        $encodedUrl = Utilities::lowerUrlencode($url);
        $scope = $encodedUrl."\n".$expiry;
        $signature = base64_encode(hash_hmac('sha256', $scope, $sharedAccessKey, true));

        return sprintf(
            Resources::SAS_AUTHORIZATION,
            Utilities::lowerUrlencode($signature),
            $expiry,
            $sharedAccessKeyName,
            $encodedUrl
        );
    }

    public function handleResponse(RequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        return $response;
    }
}
