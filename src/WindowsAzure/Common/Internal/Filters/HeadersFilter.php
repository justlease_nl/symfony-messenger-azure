<?php

declare(strict_types=1);

/**
 * LICENSE: Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * PHP version 5
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @see      https://github.com/windowsazure/azure-sdk-for-php
 */

namespace WindowsAzure\Common\Internal\Filters;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use WindowsAzure\Common\Internal\IServiceFilter;

/**
 * Adds all passed headers to the HTTP request headers.
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @version   Release: 0.5.0_2016-11
 *
 * @see      https://github.com/windowsazure/azure-sdk-for-php
 */
class HeadersFilter implements IServiceFilter
{
    private array $_headers;

    /**
     * Constructor.
     *
     * @param array $headers static headers to be added
     */
    public function __construct(array $headers)
    {
        $this->_headers = $headers;
    }

    /**
     * Adds static header(s) to the HTTP request headers.
     */
    public function handleRequest(RequestInterface $request): RequestInterface
    {
        foreach ($this->_headers as $key => $value) {
            $headers = $request->getHeaders();
            if (!\array_key_exists($key, $headers)) {
                $request = $request->withAddedHeader($key, $value);
            }
        }

        return $request;
    }

    public function handleResponse(RequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        // Do nothing with the response.
        return $response;
    }
}
