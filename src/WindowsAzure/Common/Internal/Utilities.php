<?php

declare(strict_types=1);

/**
 * LICENSE: Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * PHP version 5
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @see      https://github.com/windowsazure/azure-sdk-for-php
 */

namespace WindowsAzure\Common\Internal;

/**
 * Utilities for the project.
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @version   Release: 0.5.0_2016-11
 *
 * @see      https://github.com/windowsazure/azure-sdk-for-php
 */
class Utilities
{
    /**
     * Returns the specified value of the $key passed from $array and in case that
     * this $key doesn't exist, the default value is returned.
     *
     * @param array|null $array   The array to be used
     * @param mixed      $key     The array key
     * @param mixed      $default The value to return if $key is not found in $array
     *
     * @static
     *
     * @return mixed
     */
    public static function tryGetValue(array $array = null, $key, $default = null)
    {
        return \is_array($array) && \array_key_exists($key, $array)
            ? $array[$key]
            : $default;
    }

    /**
     * Checks if the passed $string starts with $prefix.
     *
     * @param string $string     word to search in
     * @param string $prefix     prefix to be matched
     * @param bool   $ignoreCase true to ignore case during the comparison;
     *                           otherwise, false
     *
     * @static
     */
    public static function startsWith(string $string, string $prefix, bool $ignoreCase = false): bool
    {
        if ($ignoreCase) {
            $string = strtolower($string);
            $prefix = strtolower($prefix);
        }

        return $prefix == substr($string, 0, \strlen($prefix));
    }

    /**
     * Converts string into boolean value.
     *
     * @param string $obj boolean value in string format
     *
     * @static
     */
    public static function toBoolean(string $obj): bool
    {
        return filter_var($obj, \FILTER_VALIDATE_BOOLEAN);
    }

    /**
     * Converts string into boolean value.
     *
     * @param bool $obj boolean value to convert
     *
     * @static
     */
    public static function booleanToString(bool $obj): string
    {
        return $obj ? 'true' : 'false';
    }

    /**
     * Checks if a value exists in an array. The comparison is done in a case
     * insensitive manner.
     *
     * @param string $needle   The searched value
     * @param array  $haystack The array
     *
     * @static
     */
    public static function inArrayInsensitive(string $needle, array $haystack): bool
    {
        return \in_array(strtolower($needle), array_map('strtolower', $haystack));
    }

    /**
     * Returns the specified value of the $key passed from $array and in case that
     * this $key doesn't exist, the default value is returned. The key matching is
     * done in a case insensitive manner.
     *
     * @param string $key      The array key
     * @param array  $haystack The array to be used
     * @param mixed  $default  The value to return if $key is not found in $array
     *
     * @static
     *
     * @return mixed
     */
    public static function tryGetValueInsensitive(string $key, array $haystack, $default = null)
    {
        $array = array_change_key_case($haystack);

        return self::tryGetValue($array, strtolower($key), $default);
    }

    public static function lowerUrlencode(string $str): ?string
    {
        return preg_replace_callback(
            '/%[0-9A-F]{2}/',
            fn (array $matches) => strtolower($matches[0]),
            urlencode($str)
        );
    }
}
