<?php

declare(strict_types=1);

/**
 * LICENSE: Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * PHP version 5
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @see      https://github.com/WindowsAzure/azure-sdk-for-php
 */

namespace WindowsAzure\Common\Internal\Atom;

use WindowsAzure\Common\Internal\Resources;
use WindowsAzure\Common\Internal\Validate;

/**
 * This link defines a reference from an entry or feed to a Web resource.
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @version   Release: 0.5.0_2016-11
 *
 * @see      https://github.com/WindowsAzure/azure-sdk-for-php
 */
class AtomLink extends AtomBase
{
    protected ?string $undefinedContent = null;

    /**
     * The HREF of the link.
     */
    protected string $href = '';

    /**
     * The rel attribute of the link.
     */
    protected string $rel = '';

    /**
     * The media type of the link.
     */
    protected string $type = '';

    /**
     * The language of HREF.
     */
    protected string $hreflang = '';

    /**
     * The title of the link.
     */
    protected string $title = '';

    /**
     * The length of the link.
     */
    protected int $length = 0;

    /**
     * Parse an ATOM Link xml.
     *
     * @param string $xmlString an XML based string of ATOM Link
     */
    public function parseXml(string $xmlString): void
    {
        $atomLinkXml = simplexml_load_string($xmlString);
        $attributes = $atomLinkXml->attributes();

        if (!empty($attributes['href'])) {
            $this->href = (string) $attributes['href'];
        }

        if (!empty($attributes['rel'])) {
            $this->rel = (string) $attributes['rel'];
        }

        if (!empty($attributes['type'])) {
            $this->type = (string) $attributes['type'];
        }

        if (!empty($attributes['hreflang'])) {
            $this->hreflang = (string) $attributes['hreflang'];
        }

        if (!empty($attributes['title'])) {
            $this->title = (string) $attributes['title'];
        }

        if (!empty($attributes['length'])) {
            $this->length = (int) $attributes['length'];
        }

        $undefinedContent = (string) $atomLinkXml;
        if (empty($undefinedContent)) {
            $this->undefinedContent = null;
        } else {
            $this->undefinedContent = (string) $atomLinkXml;
        }
    }

    /**
     * Gets the href of the link.
     */
    public function getHref(): string
    {
        return $this->href;
    }

    /**
     * Sets the href of the link.
     *
     * @param string $href The href of the link
     */
    public function setHref(string $href): void
    {
        $this->href = $href;
    }

    /**
     * Gets the rel of the atomLink.
     */
    public function getRel(): string
    {
        return $this->rel;
    }

    /**
     * Sets the rel of the link.
     *
     * @param string $rel The rel of the atomLink
     */
    public function setRel(string $rel): void
    {
        $this->rel = $rel;
    }

    /**
     * Gets the type of the link.
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * Sets the type of the link.
     *
     * @param string $type The type of the link
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * Gets the language of the href.
     */
    public function getHreflang(): string
    {
        return $this->hreflang;
    }

    /**
     * Sets the language of the href.
     *
     * @param string $hreflang The language of the href
     */
    public function setHreflang(string $hreflang): void
    {
        $this->hreflang = $hreflang;
    }

    /**
     * Gets the title of the link.
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * Sets the title of the link.
     *
     * @param string $title The title of the link
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * Gets the length of the link.
     */
    public function getLength(): int
    {
        return $this->length;
    }

    /**
     * Sets the length of the link.
     *
     * @param int $length The length of the link
     */
    public function setLength(int $length): void
    {
        $this->length = $length;
    }

    /**
     * Gets the undefined content.
     */
    public function getUndefinedContent(): ?string
    {
        return $this->undefinedContent;
    }

    /**
     * Sets the undefined content.
     *
     * @param string $undefinedContent The undefined content
     */
    public function setUndefinedContent(string $undefinedContent): void
    {
        $this->undefinedContent = $undefinedContent;
    }

    /**
     * Writes an XML representing the ATOM link item.
     *
     * @param \XMLWriter $xmlWriter The xml writer
     */
    public function writeXml(\XMLWriter $xmlWriter): void
    {
        $xmlWriter->startElementNS(
            'atom',
            Resources::LINK,
            Resources::ATOM_NAMESPACE
        );
        $this->writeInnerXml($xmlWriter);
        $xmlWriter->endElement();
    }

    /**
     * Writes the inner XML representing the ATOM link item.
     *
     * @param \XMLWriter $xmlWriter The xml writer
     */
    public function writeInnerXml(\XMLWriter $xmlWriter): void
    {
        Validate::notNull($xmlWriter, 'xmlWriter');

        $this->writeOptionalAttribute($xmlWriter, 'href', $this->href);
        $this->writeOptionalAttribute($xmlWriter, 'rel', $this->rel);
        $this->writeOptionalAttribute($xmlWriter, 'type', $this->type);
        $this->writeOptionalAttribute($xmlWriter, 'hreflang', $this->hreflang);
        $this->writeOptionalAttribute($xmlWriter, 'title', $this->title);
        $this->writeOptionalAttribute($xmlWriter, 'length', $this->length);

        if (!empty($this->undefinedContent)) {
            $xmlWriter->writeRaw($this->undefinedContent);
        }
    }
}
