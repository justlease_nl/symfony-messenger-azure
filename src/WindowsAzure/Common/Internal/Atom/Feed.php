<?php

declare(strict_types=1);

/**
 * LICENSE: Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * PHP version 5
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @see      https://github.com/WindowsAzure/azure-sdk-for-php
 */

namespace WindowsAzure\Common\Internal\Atom;

use WindowsAzure\Common\Internal\Resources;
use WindowsAzure\Common\Internal\Validate;

/**
 * The feed class of ATOM library.
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @version   Release: 0.5.0_2016-11
 *
 * @see      https://github.com/WindowsAzure/azure-sdk-for-php
 */
class Feed extends AtomBase
{
    // @codingStandardsIgnoreStart

    /**
     * The entry of the feed.
     */
    protected array $entry;

    /**
     * the author of the feed.
     */
    protected ?array $author = null;

    /**
     * The category of the feed.
     */
    protected ?array $category = null;

    /**
     * The contributor of the feed.
     */
    protected ?array $contributor = null;

    /**
     * The generator of the feed.
     */
    protected ?Generator $generator = null;

    /**
     * The icon of the feed.
     */
    protected string $icon = '';

    /**
     * The ID of the feed.
     */
    protected string $id = '';

    /**
     * The link of the feed.
     */
    protected ?array $link = null;

    /**
     * The logo of the feed.
     */
    protected string $logo = '';

    /**
     * The rights of the feed.
     */
    protected string $rights = '';

    /**
     * The subtitle of the feed.
     */
    protected string $subtitle = '';

    /**
     * The title of the feed.
     */
    protected string $title = '';

    /**
     * The update of the feed.
     */
    protected ?\DateTime $updated = null;

    /**
     * The extension element of the feed.
     */
    protected string $extensionElement = '';

    /**
     * Creates a feed object with specified XML string.
     *
     * @param string $xmlString An XML string representing the feed object
     */
    public function parseXml(string $xmlString): void
    {
        $feedXml = simplexml_load_string($xmlString);
        $attributes = $feedXml->attributes();
        $feedArray = (array) $feedXml;
        if (!empty($attributes)) {
            $this->attributes = (array) $attributes;
        }

        if (\array_key_exists('author', $feedArray)) {
            $this->author = $this->processAuthorNode($feedArray);
        }

        if (\array_key_exists('entry', $feedArray)) {
            $this->entry = $this->processEntryNode($feedArray);
        }

        if (\array_key_exists('category', $feedArray)) {
            $this->category = $this->processCategoryNode($feedArray);
        }

        if (\array_key_exists('contributor', $feedArray)) {
            $this->contributor = $this->processContributorNode($feedArray);
        }

        if (\array_key_exists('generator', $feedArray)) {
            $generator = new Generator();
            $generatorValue = $feedArray['generator'];
            if (\is_string($generatorValue)) {
                $generator->setText($generatorValue);
            } else {
                $generator->parseXml($generatorValue->asXML());
            }

            $this->generator = $generator;
        }

        if (\array_key_exists('icon', $feedArray)) {
            $this->icon = (string) $feedArray['icon'];
        }

        if (\array_key_exists('id', $feedArray)) {
            $this->id = (string) $feedArray['id'];
        }

        if (\array_key_exists('link', $feedArray)) {
            $this->link = $this->processLinkNode($feedArray);
        }

        if (\array_key_exists('logo', $feedArray)) {
            $this->logo = (string) $feedArray['logo'];
        }

        if (\array_key_exists('rights', $feedArray)) {
            $this->rights = (string) $feedArray['rights'];
        }

        if (\array_key_exists('subtitle', $feedArray)) {
            $this->subtitle = (string) $feedArray['subtitle'];
        }

        if (\array_key_exists('title', $feedArray)) {
            $this->title = (string) $feedArray['title'];
        }

        if (\array_key_exists('updated', $feedArray)) {
            $this->updated = \DateTime::createFromFormat(
                \DateTime::ATOM,
                (string) $feedArray['updated']
            );
        }
    }

    /**
     * Adds an attribute to the feed object instance.
     *
     * @param string $attributeKey   The key of the attribute
     * @param mixed  $attributeValue The value of the attribute
     */
    public function addAttribute(string $attributeKey, $attributeValue): void
    {
        $this->attributes[$attributeKey] = $attributeValue;
    }

    /**
     * Gets the author of the feed.
     */
    public function getAuthor(): array
    {
        return $this->author;
    }

    /**
     * Sets the author of the feed.
     *
     * @param array $author The author of the feed
     */
    public function setAuthor(array $author): void
    {
        $person = new Person();
        foreach ($author as $authorInstance) {
            Validate::isInstanceOf($authorInstance, $person, 'author');
        }
        $this->author = $author;
    }

    /**
     * Gets the category of the feed.
     */
    public function getCategory(): array
    {
        return $this->category;
    }

    /**
     * Sets the category of the feed.
     *
     * @param array $category The category of the feed
     */
    public function setCategory(array $category): void
    {
        $categoryClassInstance = new Category();
        foreach ($category as $categoryInstance) {
            Validate::isInstanceOf(
                $categoryInstance,
                $categoryClassInstance,
                'category'
            );
        }
        $this->category = $category;
    }

    /**
     * Gets contributor.
     */
    public function getContributor(): array
    {
        return $this->contributor;
    }

    /**
     * Sets contributor.
     *
     * @param array $contributor The contributor of the feed
     */
    public function setContributor(array $contributor): void
    {
        $person = new Person();
        foreach ($contributor as $contributorInstance) {
            Validate::isInstanceOf($contributorInstance, $person, 'contributor');
        }
        $this->contributor = $contributor;
    }

    /**
     * Gets generator.
     */
    public function getGenerator(): Generator
    {
        return $this->generator;
    }

    /**
     * Sets the generator.
     *
     * @param Generator $generator Sets the generator of the feed
     */
    public function setGenerator(Generator $generator): void
    {
        $this->generator = $generator;
    }

    /**
     * Gets the icon of the feed.
     */
    public function getIcon(): string
    {
        return $this->icon;
    }

    /**
     * Sets the icon of the feed.
     *
     * @param string $icon The icon of the feed
     */
    public function setIcon(string $icon): void
    {
        $this->icon = $icon;
    }

    /**
     * Gets the ID of the feed.
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * Sets the ID of the feed.
     *
     * @param string $id The ID of the feed
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * Gets the link of the feed.
     */
    public function getLink(): array
    {
        return $this->link;
    }

    /**
     * Sets the link of the feed.
     *
     * @param array $link The link of the feed
     */
    public function setLink(array $link): void
    {
        $this->link = $link;
    }

    /**
     * Gets the logo of the feed.
     */
    public function getLogo(): string
    {
        return $this->logo;
    }

    /**
     * Sets the logo of the feed.
     *
     * @param string $logo The logo of the feed
     */
    public function setLogo(string $logo): void
    {
        $this->logo = $logo;
    }

    /**
     * Gets the rights of the feed.
     */
    public function getRights(): string
    {
        return $this->rights;
    }

    /**
     * Sets the rights of the feed.
     *
     * @param string $rights The rights of the feed
     */
    public function setRights(string $rights): void
    {
        $this->rights = $rights;
    }

    /**
     * Gets the sub title.
     */
    public function getSubtitle(): string
    {
        return $this->subtitle;
    }

    /**
     * Sets the sub title of the feed.
     *
     * @param string $subtitle Sets the sub title of the feed
     */
    public function setSubtitle(string $subtitle): void
    {
        $this->subtitle = $subtitle;
    }

    /**
     * Gets the title of the feed.
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * Sets the title of the feed.
     *
     * @param string $title The title of the feed
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * Gets the updated.
     */
    public function getUpdated(): \DateTime
    {
        return $this->updated;
    }

    /**
     * Sets the updated.
     *
     * @param \DateTime $updated updated
     */
    public function setUpdated(\DateTime $updated): void
    {
        Validate::isInstanceOf($updated, new \DateTime(), 'updated');
        $this->updated = $updated;
    }

    /**
     * Gets the extension element.
     */
    public function getExtensionElement(): string
    {
        return $this->extensionElement;
    }

    /**
     * Sets the extension element.
     *
     * @param string $extensionElement The extension element
     */
    public function setExtensionElement(string $extensionElement): void
    {
        $this->extensionElement = $extensionElement;
    }

    /**
     * Gets the entry of the feed.
     */
    public function getEntry(): array
    {
        return $this->entry;
    }

    /**
     * Sets the entry of the feed.
     *
     * @param array $entry The entry of the feed
     */
    public function setEntry(array $entry): void
    {
        $this->entry = $entry;
    }

    /**
     * Writes an XML representing the feed object.
     *
     * @param \XMLWriter $xmlWriter The XML writer
     */
    public function writeXml(\XMLWriter $xmlWriter): void
    {
        Validate::notNull($xmlWriter, 'xmlWriter');

        $xmlWriter->startElementNS('atom', 'feed', Resources::ATOM_NAMESPACE);
        $this->writeInnerXml($xmlWriter);
        $xmlWriter->endElement();
    }

    /**
     * Writes an XML representing the feed object.
     *
     * @param \XMLWriter $xmlWriter The XML writer
     */
    public function writeInnerXml(\XMLWriter $xmlWriter): void
    {
        Validate::notNull($xmlWriter, 'xmlWriter');

        if (null !== $this->attributes) {
            if (\is_array($this->attributes)) {
                foreach (
                    $this->attributes
                    as $attributeName => $attributeValue
                ) {
                    $xmlWriter->writeAttribute($attributeName, $attributeValue);
                }
            }
        }

        if (null !== $this->author) {
            $this->writeArrayItem(
                $xmlWriter,
                $this->author,
                Resources::AUTHOR
            );
        }

        if (null !== $this->category) {
            $this->writeArrayItem(
                $xmlWriter,
                $this->category,
                Resources::CATEGORY
            );
        }

        if (null !== $this->contributor) {
            $this->writeArrayItem(
                $xmlWriter,
                $this->contributor,
                Resources::CONTRIBUTOR
            );
        }

        if (null !== $this->generator) {
            $this->generator->writeXml($xmlWriter);
        }

        $this->writeOptionalElementNS(
            $xmlWriter,
            'atom',
            'icon',
            Resources::ATOM_NAMESPACE,
            $this->icon
        );

        $this->writeOptionalElementNS(
            $xmlWriter,
            'atom',
            'logo',
            Resources::ATOM_NAMESPACE,
            $this->logo
        );

        $this->writeOptionalElementNS(
            $xmlWriter,
            'atom',
            'id',
            Resources::ATOM_NAMESPACE,
            $this->id
        );

        if (null !== $this->link) {
            $this->writeArrayItem(
                $xmlWriter,
                $this->link,
                Resources::LINK
            );
        }

        $this->writeOptionalElementNS(
            $xmlWriter,
            'atom',
            'rights',
            Resources::ATOM_NAMESPACE,
            $this->rights
        );

        $this->writeOptionalElementNS(
            $xmlWriter,
            'atom',
            'subtitle',
            Resources::ATOM_NAMESPACE,
            $this->subtitle
        );

        $this->writeOptionalElementNS(
            $xmlWriter,
            'atom',
            'title',
            Resources::ATOM_NAMESPACE,
            $this->title
        );

        if (null !== $this->updated) {
            $xmlWriter->writeElementNS(
                'atom',
                'updated',
                Resources::ATOM_NAMESPACE,
                $this->updated->format(\DateTime::ATOM)
            );
        }
    }
}

// @codingStandardsIgnoreEnd
