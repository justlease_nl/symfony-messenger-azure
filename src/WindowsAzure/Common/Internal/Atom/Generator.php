<?php

declare(strict_types=1);

/**
 * LICENSE: Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * PHP version 5
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @see      https://github.com/WindowsAzure/azure-sdk-for-php
 */

namespace WindowsAzure\Common\Internal\Atom;

use WindowsAzure\Common\Internal\Resources;

/**
 * The generator class of ATOM library.
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @version   Release: 0.5.0_2016-11
 *
 * @see      https://github.com/WindowsAzure/azure-sdk-for-php
 */
class Generator extends AtomBase
{
    /**
     * The of the generator.
     */
    protected string $text = '';

    /**
     * The Uri of the generator.
     */
    protected string $uri = '';

    /**
     * The version of the generator.
     */
    protected string $version = '';

    /**
     * Creates a generator instance with specified XML string.
     *
     * @param string $xmlString A string representing a generator
     *                          instance
     */
    public function parseXml(string $xmlString): void
    {
        $generatorXml = new \SimpleXMLElement($xmlString);
        $attributes = $generatorXml->attributes();
        if (!empty($attributes['uri'])) {
            $this->uri = (string) $attributes['uri'];
        }

        if (!empty($attributes['version'])) {
            $this->version = (string) $attributes['version'];
        }

        $this->text = (string) $generatorXml;
    }

    /**
     * Creates an ATOM generator instance with specified name.
     *
     * @param string $text The text content of the generator
     */
    public function __construct(string $text = null)
    {
        if (!empty($text)) {
            $this->text = $text;
        }
    }

    /**
     * Gets the text of the generator.
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * Sets the text of the generator.
     *
     * @param string $text The text of the generator
     */
    public function setText(string $text): void
    {
        $this->text = $text;
    }

    /**
     * Gets the URI of the generator.
     */
    public function getUri(): string
    {
        return $this->uri;
    }

    /**
     * Sets the URI of the generator.
     *
     * @param string $uri The URI of the generator
     */
    public function setUri(string $uri): void
    {
        $this->uri = $uri;
    }

    /**
     * Gets the version of the generator.
     */
    public function getVersion(): string
    {
        return $this->version;
    }

    /**
     * Sets the version of the generator.
     *
     * @param string $version The version of the generator
     */
    public function setVersion(string $version): void
    {
        $this->version = $version;
    }

    /**
     * Writes an XML representing the generator.
     *
     * @param \XMLWriter $xmlWriter The XML writer
     */
    public function writeXml(\XMLWriter $xmlWriter): void
    {
        $xmlWriter->startElementNS(
            'atom',
            Resources::CATEGORY,
            Resources::ATOM_NAMESPACE
        );

        $this->writeOptionalAttribute(
            $xmlWriter,
            'uri',
            $this->uri
        );

        $this->writeOptionalAttribute(
            $xmlWriter,
            'version',
            $this->version
        );

        $xmlWriter->writeRaw($this->text);
        $xmlWriter->endElement();
    }
}
