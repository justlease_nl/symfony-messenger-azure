<?php

declare(strict_types=1);

/**
 * LICENSE: Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * PHP version 5
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @see      https://github.com/windowsazure/azure-sdk-for-php
 */

namespace WindowsAzure\Common\Internal;

/**
 * Project resources.
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @version   Release: 0.5.0_2016-11
 *
 * @see      https://github.com/windowsazure/azure-sdk-for-php
 */
class Resources
{
    // @codingStandardsIgnoreStart

    // Connection strings
    public const USE_DEVELOPMENT_STORAGE_NAME = 'UseDevelopmentStorage';
    public const DEVELOPMENT_STORAGE_PROXY_URI_NAME = 'DevelopmentStorageProxyUri';
    public const DEFAULT_ENDPOINTS_PROTOCOL_NAME = 'DefaultEndpointsProtocol';
    public const ACCOUNT_NAME_NAME = 'AccountName';
    public const ACCOUNT_KEY_NAME = 'AccountKey';
    public const BLOB_ENDPOINT_NAME = 'BlobEndpoint';
    public const QUEUE_ENDPOINT_NAME = 'QueueEndpoint';
    public const TABLE_ENDPOINT_NAME = 'TableEndpoint';
    public const SHARED_ACCESS_SIGNATURE_NAME = 'SharedAccessSignature';
    public const DEV_STORE_NAME = 'devstoreaccount1';
    public const DEV_STORE_KEY = 'Eby8vdM02xNOcqFlqUwJPLlmEtlCDXJ1OUzFT50uSRZ6IFsuFq2UVErCz4I6tq/K1SZFPTOtr/KBHBeksoGMGw==';
    public const BLOB_BASE_DNS_NAME = 'blob.core.windows.net';
    public const QUEUE_BASE_DNS_NAME = 'queue.core.windows.net';
    public const TABLE_BASE_DNS_NAME = 'table.core.windows.net';
    public const DEV_STORE_CONNECTION_STRING = 'BlobEndpoint=127.0.0.1:10000;QueueEndpoint=127.0.0.1:10001;TableEndpoint=127.0.0.1:10002;AccountName=devstoreaccount1;AccountKey=Eby8vdM02xNOcqFlqUwJPLlmEtlCDXJ1OUzFT50uSRZ6IFsuFq2UVErCz4I6tq/K1SZFPTOtr/KBHBeksoGMGw==';
    public const SUBSCRIPTION_ID_NAME = 'SubscriptionID';
    public const CERTIFICATE_PATH_NAME = 'CertificatePath';
    public const SERVICE_BUS_ENDPOINT_NAME = 'Endpoint';
    public const SHARED_SHARED_ACCESS_KEY_NAME = 'SharedAccessKeyName';
    public const SHARED_SHARED_ACCESS_KEY = 'SharedAccessKey';
    public const ENTITY_PATH = 'EntityPath';

    // Messages
    public const INVALID_TYPE_MSG = 'The provided variable should be of type: ';
    public const INVALID_META_MSG = 'Metadata cannot contain newline characters.';
    public const AZURE_ERROR_MSG = "Fail:\nCode: %s\nValue: %s\ndetails (if any): %s.";
    public const NOT_IMPLEMENTED_MSG = 'This method is not implemented.';
    public const NULL_OR_EMPTY_MSG = "'%s' can't be NULL or empty.";
    public const NULL_MSG = "'%s' can't be NULL.";
    public const INVALID_PATH_MSG = "Provided path does not exist: '%s'.";
    public const INVALID_URL_MSG = 'Provided URL is invalid.';
    public const INVALID_HT_MSG = 'The header type provided is invalid.';
    public const INVALID_EDM_MSG = 'The provided EDM type is invalid.';
    public const INVALID_PROP_MSG = 'One of the provided properties is not an instance of class Property';
    public const INVALID_ENTITY_MSG = 'The provided entity object is invalid.';
    public const INVALID_VERSION_MSG = 'Server does not support any known protocol versions.';
    public const INVALID_BO_TYPE_MSG = 'Batch operation name is not supported or invalid.';
    public const INVALID_BO_PN_MSG = 'Batch operation parameter is not supported.';
    public const INVALID_OC_COUNT_MSG = 'Operations and contexts must be of same size.';
    public const INVALID_EXC_OBJ_MSG = 'Exception object type should be ServiceException.';
    public const NULL_TABLE_KEY_MSG = 'Partition and row keys can\'t be NULL.';
    public const BATCH_ENTITY_DEL_MSG = 'The entity was deleted successfully.';
    public const INVALID_PROP_VAL_MSG = "'%s' property value must satisfy %s.";
    public const INVALID_PARAM_MSG = "The provided variable '%s' should be of type '%s'";
    public const INVALID_STRING_LENGTH = "The provided variable '%s' should be of %s characters long";
    public const INVALID_BTE_MSG = 'The blob block type must exist in %s';
    public const INVALID_BLOB_PAT_MSG = 'The provided access type is invalid.';
    public const INVALID_SVC_PROP_MSG = 'The provided service properties is invalid.';
    public const UNKNOWN_SRILZER_MSG = 'The provided serializer type is unknown';
    public const INVALID_CREATE_SERVICE_OPTIONS_MSG = 'Must provide valid location or affinity group.';
    public const INVALID_UPDATE_SERVICE_OPTIONS_MSG = 'Must provide either description or label.';
    public const INVALID_CONFIG_MSG = 'Config object must be of type Configuration';
    public const INVALID_ACH_MSG = 'The provided access condition header is invalid';
    public const INVALID_RECEIVE_MODE_MSG = 'The receive message option is in neither RECEIVE_AND_DELETE nor PEEK_LOCK mode.';
    public const INVALID_CONFIG_URI = "The provided URI '%s' is invalid. It has to pass the check 'filter_var(<user_uri>, FILTER_VALIDATE_URL)'.";
    public const INVALID_CONFIG_VALUE = "The provided config value '%s' does not belong to the valid values subset:\n%s";
    public const INVALID_ACCOUNT_KEY_FORMAT = "The provided account key '%s' is not a valid base64 string. It has to pass the check 'base64_decode(<user_account_key>, true)'.";
    public const MISSING_CONNECTION_STRING_SETTINGS = "The provided connection string '%s' does not have complete configuration settings.";
    public const INVALID_CONNECTION_STRING_SETTING_KEY = "The setting key '%s' is not found in the expected configuration setting keys:\n%s";
    public const INVALID_CERTIFICATE_PATH = "The provided certificate path '%s' is invalid.";
    public const INSTANCE_TYPE_VALIDATION_MSG = 'The type of %s is %s but is expected to be %s.';
    public const MISSING_CONNECTION_STRING_CHAR = 'Missing %s character';
    public const ERROR_PARSING_STRING = "'%s' at position %d.";
    public const INVALID_CONNECTION_STRING = "Argument '%s' is not a valid connection string: '%s'";
    public const ERROR_CONNECTION_STRING_MISSING_KEY = 'Missing key name';
    public const ERROR_CONNECTION_STRING_EMPTY_KEY = 'Empty key name';
    public const ERROR_CONNECTION_STRING_MISSING_CHARACTER = 'Missing %s character';
    public const ERROR_EMPTY_SETTINGS = 'No keys were found in the connection string';
    public const MISSING_LOCK_LOCATION_MSG = 'The lock location of the brokered message is missing.';
    public const INVALID_SLOT = "The provided deployment slot '%s' is not valid. Only 'staging' and 'production' are accepted.";
    public const INVALID_DEPLOYMENT_LOCATOR_MSG = 'A slot or deployment name must be provided.';
    public const INVALID_CHANGE_MODE_MSG = "The change mode must be 'Auto' or 'Manual'. Use Mode class constants for that purpose.";
    public const INVALID_DEPLOYMENT_STATUS_MSG = "The change mode must be 'Running' or 'Suspended'. Use DeploymentStatus class constants for that purpose.";
    public const ERROR_OAUTH_GET_ACCESS_TOKEN = 'Unable to get oauth access token for endpoint \'%s\', account name \'%s\'';
    public const ERROR_OAUTH_SERVICE_MISSING = 'OAuth service missing for account name \'%s\'';
    public const ERROR_METHOD_NOT_FOUND = 'Method \'%s\' not found in object class \'%s\'';
    public const ERROR_INVALID_DATE_STRING = 'Parameter \'%s\' is not a date formatted string \'%s\'';

    // HTTP Headers
    public const X_MS_HEADER_PREFIX = 'x-ms-';
    public const X_MS_META_HEADER_PREFIX = 'x-ms-meta-';
    public const X_MS_APPROXIMATE_MESSAGES_COUNT = 'x-ms-approximate-messages-count';
    public const X_MS_POPRECEIPT = 'x-ms-popreceipt';
    public const X_MS_TIME_NEXT_VISIBLE = 'x-ms-time-next-visible';
    public const X_MS_BLOB_PUBLIC_ACCESS = 'x-ms-blob-public-access';
    public const X_MS_VERSION = 'x-ms-version';
    public const X_MS_DATE = 'x-ms-date';
    public const X_MS_BLOB_SEQUENCE_NUMBER = 'x-ms-blob-sequence-number';
    public const X_MS_BLOB_SEQUENCE_NUMBER_ACTION = 'x-ms-sequence-number-action';
    public const X_MS_BLOB_TYPE = 'x-ms-blob-type';
    public const X_MS_BLOB_CONTENT_TYPE = 'x-ms-blob-content-type';
    public const X_MS_BLOB_CONTENT_ENCODING = 'x-ms-blob-content-encoding';
    public const X_MS_BLOB_CONTENT_LANGUAGE = 'x-ms-blob-content-language';
    public const X_MS_BLOB_CONTENT_MD5 = 'x-ms-blob-content-md5';
    public const X_MS_BLOB_CACHE_CONTROL = 'x-ms-blob-cache-control';
    public const X_MS_BLOB_CONTENT_LENGTH = 'x-ms-blob-content-length';
    public const X_MS_COPY_SOURCE = 'x-ms-copy-source';
    public const X_MS_RANGE = 'x-ms-range';
    public const X_MS_RANGE_GET_CONTENT_MD5 = 'x-ms-range-get-content-md5';
    public const X_MS_LEASE_DURATION = 'x-ms-lease-duration';
    public const X_MS_LEASE_ID = 'x-ms-lease-id';
    public const X_MS_LEASE_TIME = 'x-ms-lease-time';
    public const X_MS_LEASE_STATUS = 'x-ms-lease-status';
    public const X_MS_LEASE_ACTION = 'x-ms-lease-action';
    public const X_MS_DELETE_SNAPSHOTS = 'x-ms-delete-snapshots';
    public const X_MS_PAGE_WRITE = 'x-ms-page-write';
    public const X_MS_SNAPSHOT = 'x-ms-snapshot';
    public const X_MS_SOURCE_IF_MODIFIED_SINCE = 'x-ms-source-if-modified-since';
    public const X_MS_SOURCE_IF_UNMODIFIED_SINCE = 'x-ms-source-if-unmodified-since';
    public const X_MS_SOURCE_IF_MATCH = 'x-ms-source-if-match';
    public const X_MS_SOURCE_IF_NONE_MATCH = 'x-ms-source-if-none-match';
    public const X_MS_SOURCE_LEASE_ID = 'x-ms-source-lease-id';
    public const X_MS_CONTINUATION_NEXTTABLENAME = 'x-ms-continuation-nexttablename';
    public const X_MS_CONTINUATION_NEXTPARTITIONKEY = 'x-ms-continuation-nextpartitionkey';
    public const X_MS_CONTINUATION_NEXTROWKEY = 'x-ms-continuation-nextrowkey';
    public const X_MS_REQUEST_ID = 'x-ms-request-id';
    public const ETAG = 'etag';
    public const LAST_MODIFIED = 'last-modified';
    public const DATE = 'date';
    public const AUTHENTICATION = 'authorization';
    public const WRAP_AUTHORIZATION = 'WRAP access_token="%s"';
    public const SAS_AUTHORIZATION = 'SharedAccessSignature sig=%s&se=%s&skn=%s&sr=%s';
    public const CONTENT_ENCODING = 'content-encoding';
    public const CONTENT_LANGUAGE = 'content-language';
    public const CONTENT_LENGTH = 'content-length';
    public const CONTENT_LENGTH_NO_SPACE = 'contentlength';
    public const CONTENT_MD5 = 'content-md5';
    public const CONTENT_TYPE = 'content-type';
    public const CONTENT_ID = 'content-id';
    public const CONTENT_RANGE = 'content-range';
    public const CACHE_CONTROL = 'cache-control';
    public const IF_MODIFIED_SINCE = 'if-modified-since';
    public const IF_MATCH = 'if-match';
    public const IF_NONE_MATCH = 'if-none-match';
    public const IF_UNMODIFIED_SINCE = 'if-unmodified-since';
    public const RANGE = 'range';
    public const DATA_SERVICE_VERSION = 'dataserviceversion';
    public const MAX_DATA_SERVICE_VERSION = 'maxdataserviceversion';
    public const ACCEPT_HEADER = 'accept';
    public const ACCEPT_CHARSET = 'accept-charset';
    public const USER_AGENT = 'User-Agent';

    // Type
    public const QUEUE_TYPE_NAME = 'IQueue';
    public const BLOB_TYPE_NAME = 'IBlob';
    public const TABLE_TYPE_NAME = 'ITable';
    public const SERVICE_MANAGEMENT_TYPE_NAME = 'IServiceManagement';
    public const SERVICE_BUS_TYPE_NAME = 'IServiceBus';
    public const WRAP_TYPE_NAME = 'IWrap';

    // WRAP
    public const WRAP_ACCESS_TOKEN = 'wrap_access_token';
    public const WRAP_ACCESS_TOKEN_EXPIRES_IN = 'wrap_access_token_expires_in';
    public const WRAP_NAME = 'wrap_name';
    public const WRAP_PASSWORD = 'wrap_password';
    public const WRAP_SCOPE = 'wrap_scope';

    // OAuth
    public const OAUTH_CLIENT_ASSERTION_TYPE = 'client_assertion_type';
    public const OAUTH_CLIENT_ASSERTION = 'client_assertion';
    public const OAUTH_RESOURCE = 'resource';
    public const OAUTH_GRANT_TYPE = 'grant_type';
    public const OAUTH_CLIENT_ID = 'client_id';
    public const OAUTH_CLIENT_SECRET = 'client_secret';
    public const OAUTH_USERNAME = 'username';
    public const OAUTH_PASSWORD = 'password';
    public const OAUTH_SCOPE = 'scope';
    public const OAUTH_GT_CLIENT_CREDENTIALS = 'client_credentials';
    public const OAUTH_ACCESS_TOKEN = 'access_token';
    public const OAUTH_EXPIRES_IN = 'expires_in';
    public const OAUTH_ACCESS_TOKEN_PREFIX = 'Bearer ';
    public const OAUTH_V1_ENDPOINT = '/oauth2/token';

    // HTTP Methods
    public const HTTP_GET = 'GET';
    public const HTTP_PUT = 'PUT';
    public const HTTP_POST = 'POST';
    public const HTTP_HEAD = 'HEAD';
    public const HTTP_DELETE = 'DELETE';
    public const HTTP_MERGE = 'MERGE';

    // Misc
    public const EMPTY_STRING = '';
    public const SEPARATOR = ',';
    public const AZURE_DATE_FORMAT = 'D, d M Y H:i:s T';
    public const TIMESTAMP_FORMAT = 'Y-m-d H:i:s';
    public const EMULATED = 'EMULATED';
    public const EMULATOR_BLOB_URI = '127.0.0.1:10000';
    public const EMULATOR_QUEUE_URI = '127.0.0.1:10001';
    public const EMULATOR_TABLE_URI = '127.0.0.1:10002';
    public const ASTERISK = '*';
    public const SERVICE_MANAGEMENT_URL = 'https://management.core.windows.net';
    public const HTTP_SCHEME = 'http';
    public const HTTPS_SCHEME = 'https';
    public const SETTING_NAME = 'SettingName';
    public const SETTING_CONSTRAINT = 'SettingConstraint';
    public const DEV_STORE_URI = 'http://127.0.0.1';
    public const SERVICE_URI_FORMAT = '%s://%s.%s';
    public const WRAP_ENDPOINT_URI_FORMAT = 'https://%s-sb.accesscontrol.windows.net/WRAPv0.9';

    // Xml Namespaces
    public const WA_XML_NAMESPACE = 'http://schemas.microsoft.com/windowsazure';
    public const ATOM_XML_NAMESPACE = 'http://www.w3.org/2005/Atom';
    public const DS_XML_NAMESPACE = 'http://schemas.microsoft.com/ado/2007/08/dataservices';
    public const DSM_XML_NAMESPACE = 'http://schemas.microsoft.com/ado/2007/08/dataservices/metadata';
    public const XSI_XML_NAMESPACE = 'http://www.w3.org/2001/XMLSchema-instance';
    public const TRT_XML_NAMESPACE = 'http://schemas.microsoft.com/Azure/MediaServices/KeyDelivery/TokenRestrictionTemplate/v1';
    public const PRL_XML_NAMESPACE = 'http://schemas.microsoft.com/Azure/MediaServices/KeyDelivery/PlayReadyTemplate/v1';

    // Header values
    public const SDK_USER_AGENT = 'Azure-SDK-For-PHP/0.5.0';
    public const STORAGE_API_LATEST_VERSION = '2012-02-12';
    public const SM_API_LATEST_VERSION = '2011-10-01';
    public const DATA_SERVICE_VERSION_VALUE = '1.0;NetFx';
    public const MAX_DATA_SERVICE_VERSION_VALUE = '2.0;NetFx';
    public const ACCEPT_HEADER_VALUE = 'application/atom+xml,application/xml';
    public const ATOM_ENTRY_CONTENT_TYPE = 'application/atom+xml;type=entry;charset=utf-8';
    public const ATOM_FEED_CONTENT_TYPE = 'application/atom+xml;type=feed;charset=utf-8';
    public const ACCEPT_CHARSET_VALUE = 'utf-8';
    public const INT32_MAX = 2147483647;
    public const MEDIA_SERVICES_API_LATEST_VERSION = '2.17';
    public const MEDIA_SERVICES_DATA_SERVICE_VERSION_VALUE = '3.0;NetFx';
    public const MEDIA_SERVICES_MAX_DATA_SERVICE_VERSION_VALUE = '3.0;NetFx';

    // Query parameter names
    public const QP_PREFIX = 'Prefix';
    public const QP_MAX_RESULTS = 'MaxResults';
    public const QP_METADATA = 'Metadata';
    public const QP_MARKER = 'Marker';
    public const QP_NEXT_MARKER = 'NextMarker';
    public const QP_COMP = 'comp';
    public const QP_VISIBILITY_TIMEOUT = 'visibilitytimeout';
    public const QP_POPRECEIPT = 'popreceipt';
    public const QP_NUM_OF_MESSAGES = 'numofmessages';
    public const QP_PEEK_ONLY = 'peekonly';
    public const QP_MESSAGE_TTL = 'messagettl';
    public const QP_INCLUDE = 'include';
    public const QP_TIMEOUT = 'timeout';
    public const QP_DELIMITER = 'Delimiter';
    public const QP_REST_TYPE = 'restype';
    public const QP_SNAPSHOT = 'snapshot';
    public const QP_BLOCKID = 'blockid';
    public const QP_BLOCK_LIST_TYPE = 'blocklisttype';
    public const QP_SELECT = '$select';
    public const QP_TOP = '$top';
    public const QP_SKIP = '$skip';
    public const QP_FILTER = '$filter';
    public const QP_NEXT_TABLE_NAME = 'NextTableName';
    public const QP_NEXT_PK = 'NextPartitionKey';
    public const QP_NEXT_RK = 'NextRowKey';
    public const QP_ACTION = 'action';
    public const QP_EMBED_DETAIL = 'embed-detail';

    // Query parameter values
    public const QPV_REGENERATE = 'regenerate';
    public const QPV_CONFIG = 'config';
    public const QPV_STATUS = 'status';
    public const QPV_UPGRADE = 'upgrade';
    public const QPV_WALK_UPGRADE_DOMAIN = 'walkupgradedomain';
    public const QPV_REBOOT = 'reboot';
    public const QPV_REIMAGE = 'reimage';
    public const QPV_ROLLBACK = 'rollback';

    // Request body content types
    public const URL_ENCODED_CONTENT_TYPE = 'application/x-www-form-urlencoded';
    public const XML_CONTENT_TYPE = 'application/xml';
    public const JSON_CONTENT_TYPE = 'application/json';
    public const BINARY_FILE_TYPE = 'application/octet-stream';
    public const XML_ATOM_CONTENT_TYPE = 'application/atom+xml';
    public const HTTP_TYPE = 'application/http';
    public const MULTIPART_MIXED_TYPE = 'multipart/mixed';

    // Common used XML tags
    public const XTAG_ATTRIBUTES = '@attributes';
    public const XTAG_NAMESPACE = '@namespace';
    public const XTAG_LABEL = 'Label';
    public const XTAG_NAME = 'Name';
    public const XTAG_DESCRIPTION = 'Description';
    public const XTAG_LOCATION = 'Location';
    public const XTAG_AFFINITY_GROUP = 'AffinityGroup';
    public const XTAG_HOSTED_SERVICES = 'HostedServices';
    public const XTAG_STORAGE_SERVICES = 'StorageServices';
    public const XTAG_STORAGE_SERVICE = 'StorageService';
    public const XTAG_DISPLAY_NAME = 'DisplayName';
    public const XTAG_SERVICE_NAME = 'ServiceName';
    public const XTAG_URL = 'Url';
    public const XTAG_ID = 'ID';
    public const XTAG_STATUS = 'Status';
    public const XTAG_HTTP_STATUS_CODE = 'HttpStatusCode';
    public const XTAG_CODE = 'Code';
    public const XTAG_MESSAGE = 'Message';
    public const XTAG_STORAGE_SERVICE_PROPERTIES = 'StorageServiceProperties';
    public const XTAG_ENDPOINT = 'Endpoint';
    public const XTAG_ENDPOINTS = 'Endpoints';
    public const XTAG_PRIMARY = 'Primary';
    public const XTAG_SECONDARY = 'Secondary';
    public const XTAG_KEY_TYPE = 'KeyType';
    public const XTAG_STORAGE_SERVICE_KEYS = 'StorageServiceKeys';
    public const XTAG_ERROR = 'Error';
    public const XTAG_HOSTED_SERVICE = 'HostedService';
    public const XTAG_HOSTED_SERVICE_PROPERTIES = 'HostedServiceProperties';
    public const XTAG_CREATE_HOSTED_SERVICE = 'CreateHostedService';
    public const XTAG_CREATE_STORAGE_SERVICE_INPUT = 'CreateStorageServiceInput';
    public const XTAG_UPDATE_STORAGE_SERVICE_INPUT = 'UpdateStorageServiceInput';
    public const XTAG_CREATE_AFFINITY_GROUP = 'CreateAffinityGroup';
    public const XTAG_UPDATE_AFFINITY_GROUP = 'UpdateAffinityGroup';
    public const XTAG_UPDATE_HOSTED_SERVICE = 'UpdateHostedService';
    public const XTAG_PACKAGE_URL = 'PackageUrl';
    public const XTAG_CONFIGURATION = 'Configuration';
    public const XTAG_START_DEPLOYMENT = 'StartDeployment';
    public const XTAG_TREAT_WARNINGS_AS_ERROR = 'TreatWarningsAsError';
    public const XTAG_CREATE_DEPLOYMENT = 'CreateDeployment';
    public const XTAG_DEPLOYMENT_SLOT = 'DeploymentSlot';
    public const XTAG_PRIVATE_ID = 'PrivateID';
    public const XTAG_ROLE_INSTANCE_LIST = 'RoleInstanceList';
    public const XTAG_UPGRADE_DOMAIN_COUNT = 'UpgradeDomainCount';
    public const XTAG_ROLE_LIST = 'RoleList';
    public const XTAG_SDK_VERSION = 'SdkVersion';
    public const XTAG_INPUT_ENDPOINT_LIST = 'InputEndpointList';
    public const XTAG_LOCKED = 'Locked';
    public const XTAG_ROLLBACK_ALLOWED = 'RollbackAllowed';
    public const XTAG_UPGRADE_STATUS = 'UpgradeStatus';
    public const XTAG_UPGRADE_TYPE = 'UpgradeType';
    public const XTAG_CURRENT_UPGRADE_DOMAIN_STATE = 'CurrentUpgradeDomainState';
    public const XTAG_CURRENT_UPGRADE_DOMAIN = 'CurrentUpgradeDomain';
    public const XTAG_ROLE_NAME = 'RoleName';
    public const XTAG_INSTANCE_NAME = 'InstanceName';
    public const XTAG_INSTANCE_STATUS = 'InstanceStatus';
    public const XTAG_INSTANCE_UPGRADE_DOMAIN = 'InstanceUpgradeDomain';
    public const XTAG_INSTANCE_FAULT_DOMAIN = 'InstanceFaultDomain';
    public const XTAG_INSTANCE_SIZE = 'InstanceSize';
    public const XTAG_INSTANCE_STATE_DETAILS = 'InstanceStateDetails';
    public const XTAG_INSTANCE_ERROR_CODE = 'InstanceErrorCode';
    public const XTAG_OS_VERSION = 'OsVersion';
    public const XTAG_ROLE_INSTANCE = 'RoleInstance';
    public const XTAG_ROLE = 'Role';
    public const XTAG_INPUT_ENDPOINT = 'InputEndpoint';
    public const XTAG_VIP = 'Vip';
    public const XTAG_PORT = 'Port';
    public const XTAG_DEPLOYMENT = 'Deployment';
    public const XTAG_DEPLOYMENTS = 'Deployments';
    public const XTAG_REGENERATE_KEYS = 'RegenerateKeys';
    public const XTAG_SWAP = 'Swap';
    public const XTAG_PRODUCTION = 'Production';
    public const XTAG_SOURCE_DEPLOYMENT = 'SourceDeployment';
    public const XTAG_CHANGE_CONFIGURATION = 'ChangeConfiguration';
    public const XTAG_MODE = 'Mode';
    public const XTAG_UPDATE_DEPLOYMENT_STATUS = 'UpdateDeploymentStatus';
    public const XTAG_ROLE_TO_UPGRADE = 'RoleToUpgrade';
    public const XTAG_FORCE = 'Force';
    public const XTAG_UPGRADE_DEPLOYMENT = 'UpgradeDeployment';
    public const XTAG_UPGRADE_DOMAIN = 'UpgradeDomain';
    public const XTAG_WALK_UPGRADE_DOMAIN = 'WalkUpgradeDomain';
    public const XTAG_ROLLBACK_UPDATE_OR_UPGRADE = 'RollbackUpdateOrUpgrade';
    public const XTAG_CONTAINER_NAME = 'ContainerName';
    public const XTAG_ACCOUNT_NAME = 'AccountName';

    // Service Bus
    public const LIST_TOPICS_PATH = '$Resources/Topics';
    public const LIST_QUEUES_PATH = '$Resources/Queues';
    public const LIST_RULES_PATH = '%s/subscriptions/%s/rules';
    public const LIST_SUBSCRIPTIONS_PATH = '%s/subscriptions';
    public const RECEIVE_MESSAGE_PATH = '%s/messages/head';
    public const RECEIVE_SUBSCRIPTION_MESSAGE_PATH = '%s/subscriptions/%s/messages/head';
    public const SEND_MESSAGE_PATH = '%s/messages';
    public const RULE_PATH = '%s/subscriptions/%s/rules/%s';
    public const SUBSCRIPTION_PATH = '%s/subscriptions/%s';
    public const DEFAULT_RULE_NAME = '$Default';
    public const UNIQUE_ID_PREFIX = 'urn:uuid:';
    public const SERVICE_BUS_NAMESPACE = 'http://schemas.microsoft.com/netservices/2010/10/servicebus/connect';
    public const BROKER_PROPERTIES = 'BrokerProperties';
    public const XMLNS_ATOM = 'xmlns:atom';
    public const XMLNS = 'xmlns';
    public const ATOM_NAMESPACE = 'http://www.w3.org/2005/Atom';
    public const AUTHORITY_FORMAT = 'grant_type=client_credentials&client_id=%s&client_secret=%s';

    // ATOM string
    public const AUTHOR = 'author';
    public const CATEGORY = 'category';
    public const CONTRIBUTOR = 'contributor';
    public const ENTRY = 'entry';
    public const LINK = 'link';
    public const PROPERTIES = 'properties';
    public const ELEMENT = 'element';

    // PHP URL Keys
    public const PHP_URL_SCHEME = 'scheme';
    public const PHP_URL_HOST = 'host';
    public const PHP_URL_PORT = 'port';
    public const PHP_URL_USER = 'user';
    public const PHP_URL_PASS = 'pass';
    public const PHP_URL_PATH = 'path';
    public const PHP_URL_QUERY = 'query';
    public const PHP_URL_FRAGMENT = 'fragment';

    // Status Codes
    public const STATUS_OK = 200;
    public const STATUS_CREATED = 201;
    public const STATUS_ACCEPTED = 202;
    public const STATUS_NO_CONTENT = 204;
    public const STATUS_PARTIAL_CONTENT = 206;
    public const STATUS_MOVED_PERMANENTLY = 301;

    // HTTP_Request2 config parameter names
    public const USE_BRACKETS = 'use_brackets';
    public const SSL_VERIFY_PEER = 'ssl_verify_peer';
    public const SSL_VERIFY_HOST = 'ssl_verify_host';
    public const SSL_LOCAL_CERT = 'ssl_local_cert';
    public const SSL_CAFILE = 'ssl_cafile';
    public const CONNECT_TIMEOUT = 'connect_timeout';

    // Media services
    public const MEDIA_SERVICES_URL = 'https://media.windows.net/API/';
    public const MEDIA_SERVICES_OAUTH_URL = 'https://wamsprodglobal001acs.accesscontrol.windows.net/v2/OAuth2-13';
    public const MEDIA_SERVICES_OAUTH_SCOPE = 'urn:WindowsAzureMediaServices';
    public const MEDIA_SERVICES_INPUT_ASSETS_REL = 'http://schemas.microsoft.com/ado/2007/08/dataservices/related/InputMediaAssets';
    public const MEDIA_SERVICES_ASSET_REL = 'http://schemas.microsoft.com/ado/2007/08/dataservices/related/Asset';
    public const MEDIA_SERVICES_ENCRYPTION_VERSION = '1.0';

    // @codingStandardsIgnoreEnd
}
