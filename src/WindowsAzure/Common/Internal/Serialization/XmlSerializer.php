<?php

declare(strict_types=1);

/**
 * LICENSE: Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * PHP version 5
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @see      https://github.com/windowsazure/azure-sdk-for-php
 */

namespace WindowsAzure\Common\Internal\Serialization;

use WindowsAzure\Common\Internal\Validate;
use XMLWriter;

/**
 * Short description.
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @version   Release: 0.5.0_2016-11
 *
 * @see      https://github.com/windowsazure/azure-sdk-for-php
 */
class XmlSerializer implements ISerializer
{
    public const STANDALONE = 'standalone';
    public const ROOT_NAME = 'rootName';
    public const DEFAULT_TAG = 'defaultTag';

    /**
     * Gets the attributes of a specified object if get attributes
     * method is exposed.
     *
     * @param object $targetObject The target object
     * @param array  $methodArray  The array of method of the target object
     *
     * @return mixed
     */
    private static function _getInstanceAttributes(object $targetObject, array $methodArray)
    {
        foreach ($methodArray as $method) {
            if ('getAttributes' == $method->name) {
                $classProperty = $method->invoke($targetObject);

                return $classProperty;
            }
        }

        return null;
    }

    /**
     * Serialize an object with specified root element name.
     *
     * @param object $targetObject The target object
     * @param string $rootName     The name of the root element
     */
    public static function objectSerialize(object $targetObject, string $rootName): string
    {
        Validate::notNull($targetObject, 'targetObject');
        Validate::isString($rootName, 'rootName');
        $xmlWriter = new XMLWriter();
        $xmlWriter->openMemory();
        $xmlWriter->setIndent(true);
        $reflectionClass = new \ReflectionClass($targetObject);
        $methodArray = $reflectionClass->getMethods();
        $attributes = self::_getInstanceAttributes(
            $targetObject,
            $methodArray
        );

        $xmlWriter->startElement($rootName);
        if (null !== $attributes) {
            foreach (array_keys($attributes) as $attributeKey) {
                $xmlWriter->writeAttribute(
                    (string) $attributeKey,
                    $attributes[$attributeKey]
                );
            }
        }

        foreach ($methodArray as $method) {
            if ((0 === strpos($method->name, 'get'))
                && $method->isPublic()
                && ('getAttributes' != $method->name)
            ) {
                $variableName = substr($method->name, 3);
                $variableValue = $method->invoke($targetObject);
                if (!empty($variableValue)) {
                    if ('object' === \gettype($variableValue)) {
                        $xmlWriter->writeRaw(
                            self::objectSerialize(
                                $variableValue,
                                $variableName
                            )
                        );
                    } else {
                        $xmlWriter->writeElement($variableName, (string) $variableValue);
                    }
                }
            }
        }
        $xmlWriter->endElement();

        return $xmlWriter->outputMemory(true);
    }
}
