<?php

declare(strict_types=1);

/**
 * LICENSE: Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * PHP version 5
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @see      https://github.com/windowsazure/azure-sdk-for-php
 */

namespace WindowsAzure\Common\Internal;

use Psr\Http\Message\ResponseInterface;
use WindowsAzure\Common\Internal\Http\HttpCallContext;

/**
 * Base class for all services rest proxies.
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @version   Release: 0.5.0_2016-11
 *
 * @see      https://github.com/windowsazure/azure-sdk-for-php
 */
class ServiceRestProxy extends RestProxy
{
    /**
     * Sends HTTP request with the specified HTTP call context.
     *
     * @param HttpCallContext $context The HTTP call context
     */
    protected function sendHttpContext(HttpCallContext $context): ResponseInterface
    {
        $context->setUri($this->getUri());

        return parent::sendHttpContext($context);
    }

    /**
     * Groups set of values into one value separated with Resources::SEPARATOR.
     *
     * @param array $values array of values to be grouped
     */
    public function groupQueryValues(array $values): string
    {
        $joined = Resources::EMPTY_STRING;

        foreach ($values as $value) {
            if (null !== $value && !empty($value)) {
                $joined .= $value.Resources::SEPARATOR;
            }
        }

        return trim($joined, Resources::SEPARATOR);
    }

    /**
     * Adds metadata elements to headers array.
     *
     * @param array $headers  HTTP request headers
     * @param array $metadata user specified metadata
     */
    protected function addMetadataHeaders(array $headers, array $metadata): array
    {
        $this->validateMetadata($metadata);

        $metadata = $this->generateMetadataHeaders($metadata);
        $headers = array_merge($headers, $metadata);

        return $headers;
    }

    /**
     * Generates metadata headers by prefixing each element with 'x-ms-meta'.
     *
     * @param array $metadata user defined metadata
     */
    public function generateMetadataHeaders(array $metadata): array
    {
        $metadataHeaders = [];

        foreach ($metadata as $key => $value) {
            $headerName = Resources::X_MS_META_HEADER_PREFIX;
            if (false !== strpos($value, "\r")
                || false !== strpos($value, "\n")
            ) {
                throw new \InvalidArgumentException(Resources::INVALID_META_MSG);
            }

            $headerName .= strtolower($key);
            $metadataHeaders[$headerName] = $value;
        }

        return $metadataHeaders;
    }

    /**
     * Gets metadata array by parsing them from given headers.
     *
     * @param array $headers HTTP headers containing metadata elements
     */
    public function getMetadataArray(array $headers): array
    {
        $metadata = [];
        foreach ($headers as $key => $value) {
            $isMetadataHeader = Utilities::startsWith(
                strtolower($key),
                Resources::X_MS_META_HEADER_PREFIX
            );

            if ($isMetadataHeader) {
                $MetadataName = str_replace(
                    Resources::X_MS_META_HEADER_PREFIX,
                    Resources::EMPTY_STRING,
                    strtolower($key)
                );

                $metadata[$MetadataName] = $value;
            }
        }

        return $metadata;
    }

    /**
     * Validates the provided metadata array.
     *
     * @param array|null $metadata The metadata array
     */
    public function validateMetadata(array $metadata = null): void
    {
        if (null !== $metadata) {
            Validate::isArray($metadata, 'metadata');
        } else {
            $metadata = [];
        }

        foreach ($metadata as $key => $value) {
            Validate::isString($key, 'metadata key');
            Validate::isString($value, 'metadata value');
        }
    }
}
