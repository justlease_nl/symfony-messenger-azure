<?php

declare(strict_types=1);

/**
 * LICENSE: Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * PHP version 5
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @see      https://github.com/windowsazure/azure-sdk-for-php
 */

namespace WindowsAzure\Common\Internal;

/**
 * Validates against a condition and throws an exception in case of failure.
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @version   Release: 0.5.0_2016-11
 *
 * @see      https://github.com/windowsazure/azure-sdk-for-php
 */
class Validate
{
    /**
     * Throws exception if the provided variable type is not array.
     *
     * @param mixed  $var  The variable to check
     * @param string $name The parameter name
     *
     * @throws InvalidArgumentTypeException
     */
    public static function isArray($var, string $name): void
    {
        if (!\is_array($var)) {
            throw new InvalidArgumentTypeException(\gettype([]), $name);
        }
    }

    /**
     * Throws exception if the provided variable type is not string.
     *
     * @param mixed  $var  The variable to check
     * @param string $name The parameter name
     *
     * @throws InvalidArgumentTypeException
     */
    public static function isString($var, string $name): void
    {
        if (!\is_string($var)) {
            throw new InvalidArgumentTypeException(\gettype(''), $name);
        }
    }

    /**
     * Throws exception if the provided variable is set to null.
     *
     * @param mixed  $var  The variable to check
     * @param string $name The parameter name
     *
     * @throws \InvalidArgumentException
     */
    public static function notNullOrEmpty($var, string $name): void
    {
        if (null === $var || empty($var)) {
            throw new \InvalidArgumentException(sprintf(Resources::NULL_OR_EMPTY_MSG, $name));
        }
    }

    /**
     * Throws exception if the provided condition is not satisfied.
     *
     * @param bool   $isSatisfied    condition result
     * @param string $failureMessage the exception message
     *
     * @throws \Exception
     */
    public static function isTrue(bool $isSatisfied, string $failureMessage): void
    {
        if (!$isSatisfied) {
            throw new \InvalidArgumentException($failureMessage);
        }
    }

    /**
     * Throws exception if the provided variable is set to null.
     *
     * @param mixed  $var  The variable to check
     * @param string $name The parameter name
     *
     * @throws \InvalidArgumentException
     */
    public static function notNull($var, string $name): void
    {
        if (null === $var) {
            throw new \InvalidArgumentException(sprintf(Resources::NULL_MSG, $name));
        }
    }

    /**
     * Throws exception if the object is not of the specified class type.
     *
     * @param mixed  $objectInstance An object that requires class type validation
     * @param mixed  $classInstance  The instance of the class the the
     *                               object instance should be
     * @param string $name           The name of the object
     *
     * @throws \InvalidArgumentException
     */
    public static function isInstanceOf($objectInstance, $classInstance, string $name): bool
    {
        self::notNull($classInstance, 'classInstance');
        if (null === $objectInstance) {
            return true;
        }

        $objectType = \gettype($objectInstance);
        $classType = \gettype($classInstance);

        if ($objectType === $classType) {
            return true;
        } else {
            throw new \InvalidArgumentException(sprintf(Resources::INSTANCE_TYPE_VALIDATION_MSG, $name, $objectType, $classType));
        }
    }

    /**
     * Creates a anonymous function that check if the given uri is valid or not.
     */
    public static function getIsValidUri(): callable
    {
        return fn ($uri) => self::isValidUri($uri, 'uri');
    }

    public static function isValidUri(?string $uri, string $name): bool
    {
        $isValid = filter_var($uri, \FILTER_VALIDATE_URL);

        if ($isValid) {
            return true;
        } else {
            throw new \RuntimeException(sprintf(Resources::INVALID_CONFIG_URI, $uri));
        }
    }
}
