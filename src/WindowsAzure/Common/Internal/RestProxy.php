<?php

declare(strict_types=1);

/**
 * LICENSE: Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * PHP version 5
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @see      https://github.com/windowsazure/azure-sdk-for-php
 */

namespace WindowsAzure\Common\Internal;

use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\UriFactoryInterface;
use WindowsAzure\Common\Internal\Http\HttpCallContext;
use WindowsAzure\Common\ServiceException;

/**
 * Base class for all REST proxies.
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @version   Release: 0.5.0_2016-11
 *
 * @see      https://github.com/windowsazure/azure-sdk-for-php
 */
class RestProxy
{
    private array $_filters;

    private string $_uri;
    private RequestFactoryInterface $requestFactory;
    private StreamFactoryInterface $streamFactory;
    private ClientInterface $client;
    private UriFactoryInterface $uriFactory;

    public function __construct(
        string $uri,
        RequestFactoryInterface $requestFactory,
        UriFactoryInterface $uriFactory,
        StreamFactoryInterface $streamFactory,
        ClientInterface $client
    ) {
        $this->_filters = [];
        $this->_uri = $uri;
        $this->requestFactory = $requestFactory;
        $this->uriFactory = $uriFactory;
        $this->streamFactory = $streamFactory;
        $this->client = $client;
    }

    /**
     * Gets HTTP filters that will process each request.
     */
    public function getFilters(): array
    {
        return $this->_filters;
    }

    /**
     * Gets the Uri of the service.
     */
    public function getUri(): string
    {
        return $this->_uri;
    }

    /**
     * Sets the Uri of the service.
     *
     * @param string $uri The URI of the request
     */
    public function setUri(string $uri): void
    {
        $this->_uri = $uri;
    }

    /**
     * Sends HTTP request with the specified HTTP call context.
     *
     * @param HttpCallContext $context The HTTP call context
     */
    protected function sendHttpContext(HttpCallContext $context): ResponseInterface
    {
        $contextUrl = $context->getUri();
        $uri = $this->uriFactory->createUri(empty($contextUrl) ? $this->_uri : $contextUrl);

        $query = http_build_query($context->getQueryParameters());
        $uri = $uri->withQuery($query);
        $uri = $uri->withPath($uri->getPath().$context->getPath());

        $request = $this->requestFactory->createRequest($context->getMethod(), $uri);
        if (\is_string($context->getBody())) {
            $request = $request->withBody($this->streamFactory->createStream($context->getBody()));
        }
        foreach ($context->getHeaders() as $header => $value) {
            $request = $request->withHeader($header, $value);
        }

        foreach ($this->_filters as $filter) {
            $request = $filter->handleRequest($request);
        }

        $response = $this->client->sendRequest($request);

        $start = \count($this->_filters) - 1;
        for ($index = $start; $index >= 0; --$index) {
            $response = $this->_filters[$index]->handleResponse($request, $response);
        }

        $statusCode = (string) $response->getStatusCode();
        if (!\in_array($statusCode, $context->getStatusCodes())) {
            throw new ServiceException($statusCode, $response->getReasonPhrase(), (string) $response->getBody());
        }

        return $response;
    }

    /**
     * Adds new filter to new service rest proxy object and returns that object back.
     *
     * @param IServiceFilter $filter Filter to add for the pipeline
     *
     * @return static
     */
    public function withFilter(IServiceFilter $filter): self
    {
        $serviceProxyWithFilter = clone $this;
        $serviceProxyWithFilter->_filters[] = $filter;

        return $serviceProxyWithFilter;
    }
}
