<?php

declare(strict_types=1);

/**
 * LICENSE: Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * PHP version 5
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @see      https://github.com/windowsazure/azure-sdk-for-php
 */

namespace WindowsAzure\Common\Internal\Http;

use WindowsAzure\Common\Internal\Resources;
use WindowsAzure\Common\Internal\Utilities;
use WindowsAzure\Common\Internal\Validate;

/**
 * Holds basic elements for making HTTP call.
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @version   Release: 0.5.0_2016-11
 *
 * @see      https://github.com/windowsazure/azure-sdk-for-php
 */
class HttpCallContext
{
    /**
     * The HTTP method used to make this call.
     */
    private string $_method = 'GET';

    /**
     * HTTP request headers.
     */
    private array $_headers = [];

    /**
     * The URI query parameters.
     */
    private array $_queryParams = [];

    private ?string $_uri = null;

    /**
     * The URI path.
     */
    private ?string $_path = null;

    /**
     * The expected status codes.
     */
    private array $_statusCodes = [];

    /**
     * The HTTP request body.
     */
    private ?string $_body = null;

    /**
     * Gets method.
     */
    public function getMethod(): string
    {
        return $this->_method;
    }

    /**
     * Sets method.
     *
     * @param string $method The method value
     */
    public function setMethod(string $method): void
    {
        Validate::isString($method, 'method');

        $this->_method = $method;
    }

    /**
     * Gets headers.
     */
    public function getHeaders(): array
    {
        return $this->_headers;
    }

    /**
     * Sets headers.
     *
     * Ignores the header if its value is empty.
     *
     * @param array $headers The headers value
     */
    public function setHeaders(array $headers): void
    {
        $this->_headers = [];
        foreach ($headers as $key => $value) {
            $this->addHeader($key, $value);
        }
    }

    /**
     * Gets queryParams.
     */
    public function getQueryParameters(): array
    {
        return $this->_queryParams;
    }

    /**
     * Sets queryParams.
     *
     * Ignores the query variable if its value is empty.
     *
     * @param array $queryParams The queryParams value
     */
    public function setQueryParameters(array $queryParams): void
    {
        $this->_queryParams = [];
        foreach ($queryParams as $key => $value) {
            $this->addQueryParameter($key, $value);
        }
    }

    /**
     * Gets uri.
     */
    public function getUri(): ?string
    {
        return $this->_uri;
    }

    /**
     * Sets uri.
     *
     * @param string $uri The uri value
     */
    public function setUri(string $uri): void
    {
        Validate::isString($uri, 'uri');

        $this->_uri = $uri;
    }

    /**
     * Gets path.
     */
    public function getPath(): ?string
    {
        return $this->_path;
    }

    /**
     * Sets path.
     *
     * @param string $path The path value
     */
    public function setPath(string $path): void
    {
        Validate::isString($path, 'path');

        $this->_path = $path;
    }

    /**
     * Gets statusCodes.
     */
    public function getStatusCodes(): array
    {
        return $this->_statusCodes;
    }

    /**
     * Sets statusCodes.
     *
     * @param array $statusCodes The statusCodes value
     */
    public function setStatusCodes(array $statusCodes): void
    {
        $this->_statusCodes = [];
        foreach ($statusCodes as $value) {
            $this->addStatusCode($value);
        }
    }

    /**
     * Gets body.
     */
    public function getBody(): ?string
    {
        return $this->_body;
    }

    public function setBody(string $body): void
    {
        $this->_body = $body;
    }

    public function addHeader(string $name, string $value): void
    {
        $this->_headers[$name] = $value;
    }

    /**
     * Adds or sets header pair.
     *
     * Ignores header if it's value satisfies empty().
     *
     * @param string $name  The HTTP header name
     * @param string $value The HTTP header value
     */
    public function addOptionalHeader(string $name, string $value): void
    {
        Validate::isString($name, 'name');
        Validate::isString($value, 'value');

        if (!empty($value)) {
            $this->_headers[$name] = $value;
        }
    }

    /**
     * Removes header from the HTTP request headers.
     *
     * @param string $name The HTTP header name
     */
    public function removeHeader(string $name): void
    {
        Validate::isString($name, 'name');
        Validate::notNullOrEmpty($name, 'name');

        unset($this->_headers[$name]);
    }

    /**
     * Adds or sets query parameter pair.
     *
     * @param string $name  The URI query parameter name
     * @param string $value The URI query parameter value
     */
    public function addQueryParameter(string $name, string $value): void
    {
        Validate::isString($name, 'name');
        Validate::isString($value, 'value');

        $this->_queryParams[$name] = $value;
    }

    /**
     * Adds status code to the expected status codes.
     *
     * @param int $statusCode The expected status code
     */
    public function addStatusCode(int $statusCode): void
    {
        $this->_statusCodes[] = $statusCode;
    }

    /**
     * Gets header value.
     *
     * @param string $name The header name
     *
     * @return mixed
     */
    public function getHeader(string $name)
    {
        return Utilities::tryGetValue($this->_headers, $name);
    }

    /**
     * Converts the context object to string.
     */
    public function __toString(): string
    {
        $headers = Resources::EMPTY_STRING;

        foreach ($this->_headers as $key => $value) {
            $headers .= "$key: $value\n";
        }

        $str = "$this->_method $this->_uri/$this->_path HTTP/1.1\n$headers\n";
        $str .= $this->_body;

        return $str;
    }
}
