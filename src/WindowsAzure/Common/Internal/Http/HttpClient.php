<?php

declare(strict_types=1);

/**
 * LICENSE: Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * PHP version 5
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @see      https://github.com/windowsazure/azure-sdk-for-php
 */

namespace WindowsAzure\Common\Internal\Http;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\ResponseInterface;
use WindowsAzure\Common\Internal\Resources;

/**
 * HTTP client which sends and receives HTTP requests and responses.
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @version   Release: 0.5.0_2016-11
 *
 * @see      https://github.com/windowsazure/azure-sdk-for-php
 */
class HttpClient
{
    /**
     * Initializes new HttpClient object.
     *
     * @param string $certificatePath          The certificate path
     * @param string $certificateAuthorityPath The path of the certificate authority
     */
    public static function createClient(
        string $certificatePath = Resources::EMPTY_STRING,
        string $certificateAuthorityPath = Resources::EMPTY_STRING
    ): ClientInterface {
        $config = [
            // Don't allow redirect.
            RequestOptions::ALLOW_REDIRECTS => false,
            Resources::USE_BRACKETS => true,
            Resources::SSL_VERIFY_PEER => false,
            Resources::SSL_VERIFY_HOST => false,
            RequestOptions::HEADERS => [
                Resources::USER_AGENT => Resources::SDK_USER_AGENT,
                'expect' => '',
            ],
        ];

        if (!empty($certificatePath)) {
            $config[Resources::SSL_LOCAL_CERT] = $certificatePath;
            $config[Resources::SSL_VERIFY_HOST] = true;
            $config[RequestOptions::CERT] = $certificatePath;
        }

        if (!empty($certificateAuthorityPath)) {
            $config[Resources::SSL_CAFILE] = $certificateAuthorityPath;
            $config[Resources::SSL_VERIFY_PEER] = true;
        }

        return new Client($config);
    }

    /**
     * @return string[]
     */
    public static function getResponseHeaders(ResponseInterface $response): array
    {
        $responseHeaderArray = $response->getHeaders();

        /** @var string[] $responseHeaders */
        $responseHeaders = [];

        foreach ($responseHeaderArray as $key => $value) {
            $responseHeaders[strtolower($key)] = implode(',', $value);
        }

        return $responseHeaders;
    }
}
