<?php

declare(strict_types=1);

/**
 * LICENSE: Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * PHP version 5
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @see      https://github.com/windowsazure/azure-sdk-for-php
 */

namespace WindowsAzure\Common;

use GuzzleHttp\Psr7\HttpFactory;
use Psr\Http\Client\ClientInterface;
use WindowsAzure\Common\Internal\Http\HttpClient;
use WindowsAzure\ServiceBus\Internal\IServiceBus;
use WindowsAzure\ServiceBus\Internal\IServiceBusManagement;
use WindowsAzure\ServiceBus\ServiceBusManagementRestProxy;
use WindowsAzure\ServiceBus\ServiceBusRestProxy;
use WindowsAzure\ServiceBus\ServiceBusSettings;

/**
 * Builds azure service objects.
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @version   Release: 0.5.0_2016-11
 *
 * @see      https://github.com/windowsazure/azure-sdk-for-php
 */
class ServicesBuilder
{
    private static ?ServicesBuilder $_instance = null;

    /**
     * Gets the HTTP client used in the REST services construction.
     */
    protected function httpClient(): ClientInterface
    {
        return HttpClient::createClient();
    }

    /**
     * Builds a Service Bus object.
     *
     * @param string $connectionString The configuration connection string
     */
    public function createServiceBusService(string $connectionString): IServiceBus
    {
        $settings = ServiceBusSettings::createFromConnectionString(
            $connectionString
        );

        $httpClient = $this->httpClient();
        $httpFactory = new HttpFactory();
        $serviceBusWrapper = new ServiceBusRestProxy(
            $settings->getServiceBusEndpointUri(),
            $httpFactory,
            $httpFactory,
            $httpFactory,
            $httpClient
        );

        $filter = $settings->getFilter();

        return $serviceBusWrapper->withFilter($filter);
    }

    /**
     * Builds a Service Bus object.
     *
     * @param string $connectionString The configuration connection string
     */
    public function createServiceBusManagementService(string $connectionString): IServiceBusManagement
    {
        $settings = ServiceBusSettings::createFromConnectionString(
            $connectionString
        );

        $httpClient = $this->httpClient();
        $httpFactory = new HttpFactory();
        $serviceBusWrapper = new ServiceBusManagementRestProxy(
            $settings->getServiceBusEndpointUri(),
            $httpFactory,
            $httpFactory,
            $httpFactory,
            $httpClient
        );

        $filter = $settings->getFilter();

        return $serviceBusWrapper->withFilter($filter);
    }

    /**
     * Gets the static instance of this class.
     */
    public static function getInstance(): self
    {
        if (!isset(self::$_instance)) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }
}
