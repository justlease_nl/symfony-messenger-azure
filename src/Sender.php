<?php

declare(strict_types=1);

namespace Justlease\AzureMessengerAdapter;

use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Stamp\DelayStamp;
use Symfony\Component\Messenger\Transport\Sender\SenderInterface;
use Symfony\Component\Messenger\Transport\Serialization\SerializerInterface;
use WindowsAzure\ServiceBus\Models\BrokeredMessage;

final class Sender implements SenderInterface
{
    private Connection $connection;
    private SerializerInterface $serializer;

    public function __construct(Connection $connection, SerializerInterface $serializer)
    {
        $this->connection = $connection;
        $this->serializer = $serializer;
    }

    public function send(Envelope $envelope): Envelope
    {
        $message = $this->serializer->encode($envelope->withoutStampsOfType(AzureMessageStamp::class));
        $body = $message['body'];
        $headers = $message['headers'] ?? [];
        $brokeredMessage = new BrokeredMessage($body);
        $brokeredMessage->setContentType($headers['Content-Type'] ?? 'application/octet-stream');
        unset($headers['Content-Type']);
        foreach ($headers as $propertyName => $propertyValue) {
            $brokeredMessage->setProperty(urlencode($propertyName), $propertyValue);
        }

        /** @var DelayStamp|null $delayStamp */
        $delayStamp = $envelope->last(DelayStamp::class);
        if (null !== $delayStamp) {
            $delayInMs = $delayStamp->getDelay();
            $brokeredMessage->setScheduledEnqueueTimeUtc(new \DateTime($delayInMs.'ms'));
        }

        $this->connection->publish($brokeredMessage);

        return $envelope;
    }
}
