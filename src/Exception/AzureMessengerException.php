<?php

declare(strict_types=1);

namespace Justlease\AzureMessengerAdapter\Exception;

use Symfony\Component\Messenger\Exception\TransportException;
use WindowsAzure\Common\ServiceException;
use WindowsAzure\ServiceBus\Models\BrokeredMessage;

final class AzureMessengerException extends TransportException
{
    public static function whenReceivingMessages(\Throwable $e): self
    {
        return new self('An error occurred when receiving messages from the servicebus', 0, $e);
    }

    public static function whenListingTopics(ServiceException $e): self
    {
        return new self('An error occurred when listing topics from the servicebus', 0, $e);
    }

    public static function whenCreatingTopic(string $topicName, ServiceException $e): self
    {
        return new self(sprintf('An error occurred when creating topic "%s" at the servicebus', $topicName), 0, $e);
    }

    public static function whenSendingTopicMessage(string $topicName, ServiceException $e = null): self
    {
        return new self(
            sprintf(
                'An error occurred when sending topic message "%s" to the servicebus',
                $topicName
            ),
            0,
            $e
        );
    }

    public static function whenSendingQueueMessage(string $queueName, ServiceException $e = null): self
    {
        return new self(
            sprintf(
                'An error occurred when sending message to the servicebus queue "%s" ',
                $queueName
            ),
            0,
            $e
        );
    }

    public static function whenListingSubscriptions(ServiceException $e): self
    {
        return new self('An error occurred when listing subscriptions from the servicebus', 0, $e);
    }

    public static function whenCreatingSubscription(
        string $topicName,
        string $subscriptionName,
        ServiceException $e
    ): self {
        return new self(
            sprintf(
                'An error occurred when creating subscription "%s" for topic "%s" at the servicebus',
                $subscriptionName,
                $topicName
            ),
            0,
            $e
        );
    }

    public static function whenDeletingMessage(BrokeredMessage $message, ServiceException $e): self
    {
        return new self(
            sprintf(
                'An error occurred when deleting message "%s" from the servicebus',
                $message->getMessageId()
            ),
            0,
            $e
        );
    }

    public static function whenUnlockingMessage(BrokeredMessage $message, ServiceException $e): self
    {
        return new self(
            sprintf(
                'An error occurred when unlocking message "%s" from the servicebus',
                $message->getMessageId()
            ),
            0,
            $e
        );
    }
}
