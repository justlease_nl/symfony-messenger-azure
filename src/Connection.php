<?php

declare(strict_types=1);

namespace Justlease\AzureMessengerAdapter;

use Justlease\AzureMessengerAdapter\Exception\AzureMessengerException;
use Psr\Http\Client\NetworkExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TimeoutExceptionInterface;
use WindowsAzure\Common\ServiceException;
use WindowsAzure\ServiceBus\Internal\IServiceBus;
use WindowsAzure\ServiceBus\Models\BrokeredMessage;
use WindowsAzure\ServiceBus\Models\ReceiveMessageOptions;

class Connection
{
    public const DSN_PREFIX = 'azureservicebus://';
    private const DEFAULT_OPTIONS = [
        'queue_name' => 'default',
        'auto_setup' => true,
    ];

    /**
     * Configuration of the connection.
     *
     * Available options:
     *
     * * queue_name: name of the queue
     * * auto_setup: Whether the table should be created automatically during send / get. Default: true
     *
     * @var string[]
     */
    private array $configuration;
    private IServiceBus $serviceBus;
    private bool $autoSetup;

    /**
     * @param string[] $configuration
     */
    public function __construct(array $configuration, IServiceBus $serviceBus)
    {
        $this->configuration = array_replace_recursive(static::DEFAULT_OPTIONS, $configuration);
        $this->serviceBus = $serviceBus;
        $this->autoSetup = filter_var($this->configuration['auto_setup'], \FILTER_VALIDATE_BOOLEAN);
    }

    public function publish(BrokeredMessage $message): void
    {
        if ($this->autoSetup) {
            $this->setup();
        }

        $queueName = $this->configuration['queue_name'];
        if (isset($this->configuration['subscription'])) {
            try {
                $this->serviceBus->sendTopicMessage($queueName, $message);
            } catch (ServiceException $e) {
                throw AzureMessengerException::whenSendingTopicMessage($queueName, $e);
            }
        } else {
            try {
                $this->serviceBus->sendQueueMessage($queueName, $message);
            } catch (ServiceException $e) {
                throw AzureMessengerException::whenSendingQueueMessage($queueName, $e);
            }
        }
    }

    public function get(): ?BrokeredMessage
    {
        if ($this->autoSetup) {
            $this->setup();
        }

        $queueName = $this->configuration['queue_name'];
        $options = new ReceiveMessageOptions();
        $options->setPeekLock();

        try {
            if (isset($this->configuration['subscription'])) {
                return $this->serviceBus->receiveSubscriptionMessage(
                    $queueName,
                    $this->configuration['subscription'],
                    $options
                );
            }

            return $this->serviceBus->receiveQueueMessage($queueName, $options);
        } catch (NetworkExceptionInterface $e) {
            if ($e->getPrevious() instanceof TimeoutExceptionInterface) {
                return null;
            }
            throw AzureMessengerException::whenReceivingMessages($e);
        } catch (ServiceException $e) {
            throw AzureMessengerException::whenReceivingMessages($e);
        }
    }

    public function deleteMessage(BrokeredMessage $message): void
    {
        try {
            $this->serviceBus->deleteMessage($message);
        } catch (ServiceException $e) {
            throw AzureMessengerException::whenDeletingMessage($message, $e);
        }
    }

    public function unlockMessage(BrokeredMessage $message): void
    {
        try {
            $this->serviceBus->unlockMessage($message);
        } catch (ServiceException $e) {
            throw AzureMessengerException::whenUnlockingMessage($message, $e);
        }
    }

    public function setup(): void
    {
    }
}
