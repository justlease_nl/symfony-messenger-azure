<?php

declare(strict_types=1);

namespace Justlease\AzureMessengerAdapter;

use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Transport\Serialization\SerializerInterface;
//use Symfony\Component\Messenger\Transport\SetupableTransportInterface;
use Symfony\Component\Messenger\Transport\TransportInterface;

final class AzureTransport implements TransportInterface /*, SetupableTransportInterface*/
{
    private Connection $connection;
    private SerializerInterface $serializer;
    private Receiver $receiver;
    private Sender $sender;

    public function __construct(
        Connection $connection,
        SerializerInterface $serializer
    ) {
        $this->connection = $connection;
        $this->serializer = $serializer;
    }

    public function get(): iterable
    {
        return ($this->receiver ?? $this->getReceiver())->get();
    }

    public function ack(Envelope $envelope): void
    {
        ($this->receiver ?? $this->getReceiver())->ack($envelope);
    }

    public function reject(Envelope $envelope): void
    {
        ($this->receiver ?? $this->getReceiver())->reject($envelope);
    }

    public function send(Envelope $envelope): Envelope
    {
        return ($this->sender ?? $this->getSender())->send($envelope);
    }

    public function setup(): void
    {
        $this->connection->setup();
    }

    private function getReceiver(): Receiver
    {
        return $this->receiver = new Receiver($this->connection, $this->serializer);
    }

    private function getSender(): Sender
    {
        return $this->sender = new Sender($this->connection, $this->serializer);
    }

    public function receive(callable $handler): void
    {
        ($this->receiver ?? $this->getReceiver())->receive($handler);
    }

    public function stop(): void
    {
        ($this->receiver ?? $this->getReceiver())->stop();
    }
}
