<?php

declare(strict_types=1);

namespace Justlease\AzureMessengerAdapter\Bundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

final class AzureMessengerAdapterBundle extends Bundle
{
}
