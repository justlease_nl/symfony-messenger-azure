<?php

declare(strict_types=1);

namespace Justlease\AzureMessengerAdapter;

use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Exception\MessageDecodingFailedException;
use Symfony\Component\Messenger\Transport\Receiver\ReceiverInterface;
use Symfony\Component\Messenger\Transport\Serialization\SerializerInterface;

final class Receiver implements ReceiverInterface
{
    private Connection $connection;
    private SerializerInterface $serializer;
    private bool $shouldStop = false;

    public function __construct(Connection $connection, SerializerInterface $serializer)
    {
        $this->connection = $connection;
        $this->serializer = $serializer;
    }

    public function get(): iterable
    {
        while ($message = $this->connection->get()) {
            $jsonMessage = $message->getBody();
            $headers = [];
            foreach ($message->getProperties() as $name => $value) {
                $headers[urldecode($name)] = $value;
            }
            $headers['Content-Type'] = $message->getContentType();
            try {
                $envelope = $this->serializer->decode(['body' => $jsonMessage, 'headers' => $headers]);
            } catch (MessageDecodingFailedException $exception) {
                $this->connection->deleteMessage($message);

                throw $exception;
            }
            yield $envelope
                ->with(new AzureMessageStamp($message));
        }
    }

    public function ack(Envelope $envelope): void
    {
        /** @var ?AzureMessageStamp $azureStamp */
        $azureStamp = $envelope->last(AzureMessageStamp::class);
        if (null === $azureStamp) {
            return;
        }

        $this->connection->deleteMessage($azureStamp->message);
    }

    public function reject(Envelope $envelope): void
    {
        /** @var ?AzureMessageStamp $azureStamp */
        $azureStamp = $envelope->last(AzureMessageStamp::class);
        if (null === $azureStamp) {
            return;
        }

        $this->connection->deleteMessage($azureStamp->message);
    }

    public function receive(callable $handler): void
    {
        while (!$this->shouldStop) {
            foreach ($this->get() as $envelope) {
                try {
                    $handler($envelope);

                    $this->ack($envelope);
                } catch (\Throwable $e) {
                    $this->reject($envelope);

                    throw $e;
                } finally {
                    if (\function_exists('pcntl_signal_dispatch')) {
                        pcntl_signal_dispatch();
                    }
                }
            }

            $handler(null);

            usleep(200000);
            if (\function_exists('pcntl_signal_dispatch')) {
                pcntl_signal_dispatch();
            }
        }
    }

    public function stop(): void
    {
        $this->shouldStop = true;
    }
}
