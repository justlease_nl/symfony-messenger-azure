<?php

declare(strict_types=1);

namespace Justlease\AzureMessengerAdapter;

use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\UriFactoryInterface;
use Symfony\Component\Messenger\Exception\InvalidArgumentException;
use Symfony\Component\Messenger\Transport\Serialization\Serializer;
use Symfony\Component\Messenger\Transport\Serialization\SerializerInterface;
use Symfony\Component\Messenger\Transport\TransportFactoryInterface;
use Symfony\Component\Messenger\Transport\TransportInterface;
use WindowsAzure\ServiceBus\ServiceBusRestProxy;
use WindowsAzure\ServiceBus\ServiceBusSettings;

final class AzureTransportFactory implements TransportFactoryInterface
{
    private RequestFactoryInterface $requestFactory;
    private UriFactoryInterface $uriFactory;
    private StreamFactoryInterface $streamFactory;
    private ClientInterface $client;

    public function __construct(
        RequestFactoryInterface $requestFactory,
        UriFactoryInterface $uriFactory,
        StreamFactoryInterface $streamFactory,
        ClientInterface $azureTransportClient
    ) {
        $this->requestFactory = $requestFactory;
        $this->uriFactory = $uriFactory;
        $this->streamFactory = $streamFactory;
        $this->client = $azureTransportClient;
    }

    /**
     * @param string[] $options
     */
    public function createTransport(
        string $dsn,
        array $options,
        SerializerInterface $serializer = null
    ): TransportInterface {
        unset($options['transport_name']);

        $connectionString = substr($dsn, \strlen(Connection::DSN_PREFIX));
        if (false === $params = parse_url($connectionString)) {
            throw new InvalidArgumentException('The given Azure ServiceBus Messenger DSN is invalid.');
        }
        $query = [];
        if (isset($params['query'])) {
            parse_str($params['query'], $query);
        }
        $connection = $this->createConnectionFromConnectionString($params['path'], $query + $options);

        return new AzureTransport($connection, $serializer ?? Serializer::create());
    }

    /**
     * @param string[] $options
     */
    public function supports(string $dsn, array $options): bool
    {
        return str_starts_with($dsn, Connection::DSN_PREFIX);
    }

    private function createConnectionFromConnectionString(string $connectionString, array $options): Connection
    {
        $settings = ServiceBusSettings::createFromConnectionString(
            $connectionString
        );
        $serviceBus = new ServiceBusRestProxy(
            $settings->getServiceBusEndpointUri(),
            $this->requestFactory,
            $this->uriFactory,
            $this->streamFactory,
            $this->client
        );
        $serviceBus = $serviceBus->withFilter($settings->getFilter());

        return new Connection($options, $serviceBus);
    }
}
