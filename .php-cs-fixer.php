<?php
$finder = PhpCsFixer\Finder::create()
    ->exclude('vendor')
    ->name('*.php')
    ->in(__DIR__)
;
return (new PhpCsFixer\Config())
    ->setRules(
        [
            '@PSR12' => true,
            '@PSR12:risky' => true,
            '@Symfony' => true,
            '@Symfony:risky' => true,
            '@PHP71Migration' => true,
            '@PHP71Migration:risky' => true,
            '@PHP73Migration' => true,
            '@PHP74Migration' => true,
            '@PHP74Migration:risky' => true,
            'single_blank_line_before_namespace' => true,
            'ordered_imports' => true,
            'concat_space' => ['spacing' => 'none'],
            'no_mixed_echo_print' => ['use' => 'echo'],
            'binary_operator_spaces' => ['operators' => ['=>' => 'single_space', '=' => 'single_space']],
            'fully_qualified_strict_types' => true,
        ]
    )
    ->setFinder($finder)
    ->setRiskyAllowed(true)
    ->setUsingCache(true)
    ;
