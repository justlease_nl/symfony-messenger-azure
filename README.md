Azure PubSub adapter for symfony/messenger
===========================

This is a Receiver/Sender on Azure for the symfony/messenger component for servicebus queue and/or topic and subscribers.

## Quick start

First of all: This uses topics / subscriptions like described [here](https://docs.microsoft.com/en-us/azure/service-bus-messaging/service-bus-php-how-to-use-topics-subscriptions). Make sure you have a connection-string ready.

1. Install the transport
```console
composer require symfony/messenger justlease/symfony-messenger-azure
```

2. Add the bundle
```new Justlease\AzureMessengerAdapter\Bundle\AzureMessengerAdapterBundle()```
   or
```
    Justlease\AzureMessengerAdapter\Bundle\AzureMessengerAdapterBundle::class => ['all' => true],
```

3. Add the following configuration:

```yaml
framework.messenger.transports:
            azure_queues:
              dsn: '%env(MESSENGER_TRANSPORT_DSN)%'
              serializer: messenger.transport.symfony_serializer
              options:
                queue_name: <topic or queue>
                subscription: <subscription name when using topic/subscription>
```

4. environment config
```
###
MESSENGER_TRANSPORT_DSN=azureservicebus://Endpoint=https://tblgservicebus-test.servicebus.windows.net/;SharedAccessKeyName=name;SharedAccessKey=<key>
###
```

## Links
 - [How to use Service Bus topics with PHP](https://docs.microsoft.com/en-us/azure/service-bus-messaging/service-bus-php-how-to-use-topics-subscriptions)
