<?php

declare(strict_types=1);

namespace Justlease\AzureMessengerAdapter\Tests\Bundle\DependencyInjection;

use Justlease\AzureMessengerAdapter\AzureTransportFactory;
use Justlease\AzureMessengerAdapter\Bundle\DependencyInjection\AzureMessengerAdapterExtension;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\ConfigurationExtensionInterface;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\Messenger\Tests\Fixtures\DummyMessage;

class AzureMessengerAdapterExtensionTest extends TestCase
{
    use ProphecyTrait;

    private AzureMessengerAdapterExtension $extension;

    protected function setUp(): void
    {
        $this->extension = new AzureMessengerAdapterExtension();
    }

    public function testConstruct(): void
    {
        $this->extension = new AzureMessengerAdapterExtension();
        $this->assertInstanceOf(ExtensionInterface::class, $this->extension);
        $this->assertInstanceOf(ConfigurationExtensionInterface::class, $this->extension);
    }

    public function testLoad(): void
    {
        $config = ['azure_messenger' => [
            'messages' => [DummyMessage::class => 'topic_name'],
            'azure' => [
                'connectionString' => 'Endpoint=test-endpoint-url',
            ],
        ]];

        $containerBuilderProphecy = $this->prophesize(ContainerBuilder::class);

        $containerBuilderProphecy->fileExists(Argument::any())->willReturn(true);
        $containerBuilderProphecy->removeBindings(AzureTransportFactory::class)->shouldBeCalled();
        $containerBuilderProphecy->setDefinition(AzureTransportFactory::class, Argument::which('getTags', ['messenger.transport_factory' => [[]]]))->willReturnArgument(1);
        $this->extension->load($config, $containerBuilderProphecy->reveal());
    }
}
