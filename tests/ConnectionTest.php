<?php

declare(strict_types=1);

namespace Justlease\AzureMessengerAdapter\Tests;

use Justlease\AzureMessengerAdapter\Connection;
use Justlease\AzureMessengerAdapter\Exception\AzureMessengerException;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Messenger\Tests\Fixtures\DummyMessage;
use WindowsAzure\Common\ServiceException;
use WindowsAzure\ServiceBus\Internal\IServiceBus;
use WindowsAzure\ServiceBus\Internal\IServiceBusManagement;
use WindowsAzure\ServiceBus\Models\BrokeredMessage;
use WindowsAzure\ServiceBus\Models\ListSubscriptionsResult;
use WindowsAzure\ServiceBus\Models\ListTopicsResult;
use WindowsAzure\ServiceBus\Models\ReceiveMessageOptions;
use WindowsAzure\ServiceBus\Models\SubscriptionInfo;
use WindowsAzure\ServiceBus\Models\TopicInfo;

final class ConnectionTest extends TestCase
{
    /**
     * @var MockObject|IServiceBus
     */
    private MockObject $serviceBus;
    /**
     * @var MockObject|IServiceBusManagement
     */
    private MockObject $serviceBusManagement;

    protected function setUp(): void
    {
        $this->serviceBus = $this->createMock(IServiceBus::class);
        $this->serviceBusManagement = $this->createMock(IServiceBusManagement::class);
    }

    /**
     * @doesNotPerformAssertions
     */
    public function testItSendsTopicMessages(): void
    {
        $this->expectNotToPerformAssertions();
        $listTopicsResult = new ListTopicsResult();
        $topicName = 'test';
        $listTopicsResult->setTopicInfos([
            new TopicInfo($topicName),
        ]);
        $message = ['body' => '...', 'headers' => ['type' => DummyMessage::class]];
        $this->serviceBusManagement->method('listTopics')->with()->willReturn($listTopicsResult);
        $brokeredMessage = new BrokeredMessage(json_encode($message));
        $this->serviceBus->method('sendTopicMessage')->with($topicName, $brokeredMessage);

        $connection = new Connection(
            ['queue_name' => $topicName, 'subscription' => 'subscriptionName'],
            $this->serviceBus
        );
        $connection->publish($brokeredMessage);
    }

    /**
     * @doesNotPerformAssertions
     */
    public function testCreatesTopicFirstWhenNotExistsWhenPublishing(): void
    {
        $this->expectNotToPerformAssertions();
        $listTopicsResult = new ListTopicsResult();
        $listTopicsResult->setTopicInfos([
            new TopicInfo('test-not-exists'),
        ]);
        $this->serviceBusManagement->method('listTopics')->willReturn($listTopicsResult);
        $this->serviceBusManagement->method('createTopic')->with(new TopicInfo('test'));
        $this->serviceBus->method('sendTopicMessage')->with('test');

        $connection = new Connection(
            ['queue_name' => 'test', 'subscription' => 'subscriptionName'],
            $this->serviceBus
        );
        $encoded = ['body' => '...', 'headers' => ['type' => DummyMessage::class]];
        $brokeredMessage = new BrokeredMessage(json_encode($encoded));
        $connection->publish($brokeredMessage);
    }

    /**
     * @doesNotPerformAssertions
     */
    public function testItReceivesSubscriptionMessage(): void
    {
        $this->expectNotToPerformAssertions();
        $listSubscriptionsResult = new ListSubscriptionsResult();
        $topicName = 'test';
        $listSubscriptionsResult->setSubscriptionInfos([
            new SubscriptionInfo($topicName),
        ]);
        $message = ['body' => '...', 'headers' => ['type' => DummyMessage::class]];
        $this->serviceBusManagement->method('listSubscriptions')->with($topicName)->willReturn($listSubscriptionsResult);
        $options = new ReceiveMessageOptions();
        $options->setPeekLock();
        $this->serviceBus->method('receiveSubscriptionMessage')->with($topicName, 'subscriptionName', $options)->willReturn(new BrokeredMessage(
            json_encode($message)
        ));

        $connection = new Connection(
            ['queue_name' => $topicName, 'subscription' => 'subscriptionName'],
            $this->serviceBus
        );
        $connection->get();
    }

    public function testThrowsExceptionWhenListSubscriptionFailedWhenPublishing(): void
    {
        $topicName = 'test';
        $listSubscriptionsResult = new ListSubscriptionsResult();
        $listSubscriptionsResult->setSubscriptionInfos([]);
        $this->expectException(AzureMessengerException::class);
        $this->serviceBusManagement->method('listSubscriptions')->with($topicName)->willReturn($listSubscriptionsResult);
        $options = new ReceiveMessageOptions();
        $options->setPeekLock();
        $this->serviceBus->method('receiveSubscriptionMessage')->with($topicName, 'subscriptionName', $options)->willThrowException(new ServiceException('test', '', ''));

        $connection = new Connection(
            ['queue_name' => $topicName, 'subscription' => 'subscriptionName'],
            $this->serviceBus
        );
        $connection->get();
    }

    /**
     * @doesNotPerformAssertions
     */
    public function testCreatesSubscriptionFirstWhenNotExistsWhenReceiving(): void
    {
        $this->expectNotToPerformAssertions();
        $message = ['body' => '...', 'headers' => ['type' => DummyMessage::class]];
        $listSubscriptionsResult = new ListSubscriptionsResult();
        $topicName = 'test';
        $listSubscriptionsResult->setSubscriptionInfos([]);
        $this->serviceBusManagement->method('createSubscription')->with($topicName, new SubscriptionInfo('subscriptionName'));
        $this->serviceBusManagement->method('listSubscriptions')->with($topicName)->willReturn($listSubscriptionsResult);
        $options = new ReceiveMessageOptions();
        $options->setPeekLock();
        $this->serviceBus->method('receiveSubscriptionMessage')->with($topicName, 'subscriptionName', $options)->willReturn(new BrokeredMessage(
            json_encode($message)
        ));

        $connection = new Connection(
            ['queue_name' => $topicName, 'subscription' => 'subscriptionName'],
            $this->serviceBus
        );
        $connection->get();
    }
}
