<?php

declare(strict_types=1);

namespace Justlease\AzureMessengerAdapter\Tests;

use Justlease\AzureMessengerAdapter\AzureMessageStamp;
use Justlease\AzureMessengerAdapter\Connection;
use Justlease\AzureMessengerAdapter\Receiver;
use Justlease\AzureMessengerAdapter\Tests\Fixtures\DummyMessage;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Exception\MessageDecodingFailedException;
use Symfony\Component\Messenger\Transport\Serialization\Serializer;
use Symfony\Component\Serializer as SerializerComponent;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use WindowsAzure\ServiceBus\Models\BrokeredMessage;

class ReceiverTest extends TestCase
{
    use ProphecyTrait;

    public function testItSendTheDecodedMessageToTheHandler(): void
    {
        $serializer = new Serializer(
            new SerializerComponent\Serializer([new ObjectNormalizer()], ['json' => new JsonEncoder()])
        );

        $connection = $this->createMock(Connection::class);

        $brokerMessage = new BrokeredMessage('{"message": "Hi"}');
        $brokerMessage->setProperty('type', DummyMessage::class);

        $connection
            ->method('get')
            ->willReturnOnConsecutiveCalls($brokerMessage, null);

        $receiver = new Receiver($connection, $serializer);
        /** @var \Generator $envelopes */
        $envelopes = $receiver->get();

        /** @var Envelope $envelope */
        $envelope = $envelopes->current();

        $this->assertEquals(new DummyMessage('Hi'), $envelope->getMessage());
        $this->assertInstanceOf(AzureMessageStamp::class, $envelope->last(AzureMessageStamp::class));
        $envelopes->next();
        $this->assertNull($envelopes->current());
    }

    public function testDecodingExceptionRemovesMessage(): void
    {
        $serializer = $this->createMock(Serializer::class);
        $connection = $this->createMock(Connection::class);

        $brokerMessage = new BrokeredMessage('{"message": "Hi"}');
        $brokerMessage->setProperty('type', DummyMessage::class);

        $connection
            ->method('get')
            ->willReturnOnConsecutiveCalls($brokerMessage, null);

        $serializer
            ->method('decode')
            ->willThrowException(new MessageDecodingFailedException('test'));
        $connection
            ->expects($this->once())
            ->method('deleteMessage')
            ->with($brokerMessage);

        $receiver = new Receiver($connection, $serializer);
        /** @var \Generator $envelopes */
        $envelopes = $receiver->get();

        $this->expectException(MessageDecodingFailedException::class);
        $envelopes->current();
    }
}
