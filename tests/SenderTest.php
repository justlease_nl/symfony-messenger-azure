<?php

declare(strict_types=1);

namespace Justlease\AzureMessengerAdapter\Tests;

use Justlease\AzureMessengerAdapter\Connection;
use Justlease\AzureMessengerAdapter\Sender;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Stamp\DelayStamp;
use Symfony\Component\Messenger\Transport\Serialization\SerializerInterface;
use WindowsAzure\ServiceBus\Models\BrokeredMessage;

class SenderTest extends TestCase
{
    use ProphecyTrait;

    public function testItSendsTheEncodedMessage(): void
    {
        $envelope = new Envelope(new DummyMessage());
        $encoded = ['body' => '...', 'headers' => ['type' => DummyMessage::class, 'Content-Type' => 'json']];
        $serializer = $this->getMockBuilder(SerializerInterface::class)->getMock();
        $serializer->method('encode')->with($envelope)->willReturnOnConsecutiveCalls($encoded);
        $connection = $this->prophesize(Connection::class);
        $messageChecker = Argument::that(function (BrokeredMessage $message) {
            self::assertSame('...', $message->getBody());
            self::assertSame('json', $message->getContentType());
            self::assertSame(['type' => DummyMessage::class], $message->getProperties());
            self::assertNull($message->getBrokerProperties()->getScheduledEnqueueTimeUtc());

            return true;
        });
        $connection->publish($messageChecker);

        $sender = new Sender($connection->reveal(), $serializer);
        $sender->send($envelope);
    }

    public function testItSendsTheEncodedMessageWithDelay(): void
    {
        $envelope = new Envelope(new DummyMessage(), [new DelayStamp(12000)]);
        $encoded = ['body' => '...', 'headers' => ['type' => DummyMessage::class, 'Content-Type' => 'json']];
        $serializer = $this->createMock(SerializerInterface::class);
        $serializer->method('encode')->with($envelope)->willReturnOnConsecutiveCalls($encoded);
        $connection = $this->prophesize(Connection::class);
        $messageChecker = Argument::that(function (BrokeredMessage $message) {
            self::assertSame('...', $message->getBody());
            self::assertSame('json', $message->getContentType());
            self::assertSame(['type' => DummyMessage::class], $message->getProperties());
            self::assertEqualsWithDelta(new \DateTime('12sec'), $message->getBrokerProperties()->getScheduledEnqueueTimeUtc(), 1);

            return true;
        });
        $connection->publish($messageChecker);

        $sender = new Sender($connection->reveal(), $serializer);
        $sender->send($envelope);
    }
}
class DummyMessage
{
}
