<?php

declare(strict_types=1);

/**
 * LICENSE: Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * PHP version 5
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @see      https://github.com/windowsazure/azure-sdk-for-php
 */

namespace Tests\framework;

use WindowsAzure\Common\Internal\Resources;

/**
 * Resources for testing framework.
 *
 * @author     Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright  2012 Microsoft Corporation
 * @license    http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @version   Release: 0.5.0_2016-11
 *
 * @see       https://github.com/windowsazure/azure-sdk-for-php
 */
class TestResources
{
    public const QUEUE1_NAME = 'Queue1';
    public const QUEUE2_NAME = 'Queue2';
    public const QUEUE3_NAME = 'Queue3';
    public const KEY1 = 'key1';
    public const KEY2 = 'key2';
    public const KEY3 = 'key3';
    public const KEY4 = 'AhlzsbLRkjfwObuqff3xrhB2yWJNh1EMptmcmxFJ6fvPTVX3PZXwrG2YtYWf5DPMVgNsteKStM5iBLlknYFVoA==';
    public const VALUE1 = 'value1';
    public const VALUE2 = 'value2';
    public const VALUE3 = 'value3';
    public const ACCOUNT_NAME = 'myaccount';
    public const QUEUE_URI = '.queue.core.windows.net';
    public const URI1 = 'http://myaccount.queue.core.windows.net/myqueue';
    public const URI2 = 'http://myaccount.queue.core.windows.net/?comp=list';
    public const DATE1 = 'Sat, 18 Feb 2012 16:25:21 GMT';
    public const DATE2 = 'Mon, 20 Feb 2012 17:12:31 GMT';
    public const VALID_URL = 'http://www.example.com';
    public const HEADER1 = 'testheader1';
    public const HEADER2 = 'testheader2';
    public const HEADER1_VALUE = 'HeaderValue1';
    public const HEADER2_VALUE = 'HeaderValue2';

    // Media services
    public const MEDIA_SERVICES_ASSET_NAME = 'TestAsset';
    public const MEDIA_SERVICES_OUTPUT_ASSET_NAME = 'TestOutputAsset';
    public const MEDIA_SERVICES_ACCESS_POLICY_NAME = 'TestAccessPolicy';
    public const MEDIA_SERVICES_LOCATOR_NAME = 'TestLocator';
    public const MEDIA_SERVICES_JOB_NAME = 'TestJob';
    public const MEDIA_SERVICES_JOB_ID_PREFIX = 'nb:jid:UUID:';
    public const MEDIA_SERVICES_JOB_TEMPLATE_NAME = 'TestJobTemplate';
    public const MEDIA_SERVICES_JOB_TEMPLATE_ID_PREFIX = 'nb:jtid:UUID:';
    public const MEDIA_SERVICES_TASK_CONFIGURATION = 'H.264 HD 720p VBR';
    public const MEDIA_SERVICES_PROCESSOR_NAME = 'Media Encoder Standard';
    public const MEDIA_SERVICES_DECODE_PROCESSOR_NAME = 'Storage Decryption';
    public const MEDIA_SERVICES_PROCESSOR_ID_PREFIX = 'nb:mpid:UUID:';
    public const MEDIA_SERVICES_DUMMY_FILE_NAME = 'simple.avi';
    public const MEDIA_SERVICES_DUMMY_FILE_CONTENT = 'test file content';
    public const MEDIA_SERVICES_DUMMY_FILE_NAME_1 = 'other.avi';
    public const MEDIA_SERVICES_DUMMY_FILE_CONTENT_1 = 'other file content';
    public const MEDIA_SERVICES_ISM_FILE_NAME = 'small.ism';
    public const MEDIA_SERVICES_ISMC_FILE_NAME = 'small.ismc';
    public const MEDIA_SERVICES_STREAM_APPEND = 'Manifest';
    public const MEDIA_SERVICES_INGEST_MANIFEST = 'TestIngestManifest';
    public const MEDIA_SERVICES_INGEST_MANIFEST_ASSET = 'TestIngestManifestAsset';
    public const MEDIA_SERVICES_CONTENT_KEY_AUTHORIZATION_POLICY_NAME = 'TestContentKeyAuthorizationPolicy';
    public const MEDIA_SERVICES_CONTENT_KEY_AUTHORIZATION_OPTIONS_NAME = 'TestContentKeyAuthorizationPolicyOption';
    public const MEDIA_SERVICES_CONTENT_KEY_AUTHORIZATION_POLICY_RESTRICTION_NAME = 'TestContentKeyAuthorizationPolicyRestriction';
    public const MEDIA_SERVICES_ASSET_DELIVERY_POLICY_NAME = 'AssetDeliveryPolicyName';
    public const MEDIA_SERVICES_CHANNEL_NAME = 'PHPSDK-CHANNEL-UNITTEST';

    // See https://tools.ietf.org/html/rfc2616
    public const STATUS_NOT_MODIFIED = 304;
    public const STATUS_BAD_REQUEST = 400;
    public const STATUS_UNAUTHORIZED = 401;
    public const STATUS_FORBIDDEN = 403;
    public const STATUS_NOT_FOUND = 404;
    public const STATUS_CONFLICT = 409;
    public const STATUS_PRECONDITION_FAILED = 412;
    public const STATUS_INTERNAL_SERVER_ERROR = 500;

    public static function getServiceBusConnectionString()
    {
        $connectionString = getenv('AZURE_SERVICE_BUS_CONNECTION_STRING');

        if (empty($connectionString)) {
            throw new \Exception('AZURE_SERVICE_BUS_CONNECTION_STRING environment variable is missing.');
        }

        return $connectionString;
    }
}
