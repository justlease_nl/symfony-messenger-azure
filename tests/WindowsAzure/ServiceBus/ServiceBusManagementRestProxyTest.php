<?php

declare(strict_types=1);

/**
 * LICENSE: Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * PHP version 5
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @see      https://github.com/WindowsAzure/azure-sdk-for-php
 */

namespace Tests\unit\WindowsAzure\ServiceBus;

use Tests\framework\ServiceBusRestProxyTestBase;
use WindowsAzure\Common\Internal\Resources;
use WindowsAzure\Common\ServiceException;
use WindowsAzure\ServiceBus\Models\ListQueuesOptions;
use WindowsAzure\ServiceBus\Models\ListRulesOptions;
use WindowsAzure\ServiceBus\Models\ListSubscriptionsOptions;
use WindowsAzure\ServiceBus\Models\ListTopicsOptions;
use WindowsAzure\ServiceBus\Models\QueueInfo;
use WindowsAzure\ServiceBus\Models\RuleInfo;
use WindowsAzure\ServiceBus\Models\SqlFilter;
use WindowsAzure\ServiceBus\Models\SubscriptionInfo;
use WindowsAzure\ServiceBus\Models\TopicInfo;

/**
 * Unit tests for ServiceBusManagementRestProxy class.
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @version   Release: 0.5.0_2016-11
 *
 * @see      https://github.com/WindowsAzure/azure-sdk-for-php
 */
class ServiceBusManagementRestProxyTest extends ServiceBusRestProxyTestBase
{
    /**
     * @covers \WindowsAzure\ServiceBus\ServiceBusManagementRestProxy::createQueue
     */
    public function testCreateQueueWorks(): void
    {
        // Setup
        $queueName = 'createQueueWorks';
        $queueInfo = new QueueInfo($queueName);
        $this->safeDeleteQueue($queueName);

        // Test
        $queueInfo = $this->createQueue($queueInfo);

        // Assert
        $this->assertNotNull($queueInfo);
    }

    /**
     * @covers \WindowsAzure\ServiceBus\ServiceBusManagementRestProxy::deleteQueue
     */
    public function testDeleteQueueWorks(): void
    {
        // Setup
        $queueName = 'testDeleteQueueWorks';
        $queueInfo = new QueueInfo($queueName);
        $this->safeDeleteQueue($queueName);
        $this->serviceBusManagementWrapper->createQueue($queueInfo);

        // Test
        $this->serviceBusManagementWrapper->deleteQueue($queueName);

        // Assert
        $this->assertNotNull($queueInfo);
    }

    /**
     * @covers \WindowsAzure\ServiceBus\ServiceBusManagementRestProxy::deleteQueue
     */
    public function testDeleteQueueNonExistQueueFail(): void
    {
        // Setup
        $queueName = 'IDoNotExist';
        $this->expectException(ServiceException::class);
        // Test
        $this->serviceBusManagementWrapper->deleteQueue($queueName);

        // Assert
    }

    /**
     * @covers \WindowsAzure\ServiceBus\ServiceBusManagementRestProxy::deleteQueue
     */
    public function testDeleteQueueSuccess(): void
    {
        // Setup
        $queueName = 'testDeleteQueueSuccess';
        $createQueueInfo = new QueueInfo($queueName);
        $listQueuesOptions = new ListQueuesOptions();
        $listQueuesResult = $this->serviceBusManagementWrapper->listQueues($listQueuesOptions);

        foreach ($listQueuesResult->getQueueInfos() as $queueInfo) {
            $this->serviceBusManagementWrapper->deleteQueue($queueInfo->getTitle());
        }

        $this->serviceBusManagementWrapper->createQueue($createQueueInfo);

        // Test
        $this->serviceBusManagementWrapper->deleteQueue($queueName);
        $listQueuesResult = $this->serviceBusManagementWrapper->listQueues($listQueuesOptions);

        // Assert

        $this->assertEquals(
            0,
            \count($listQueuesResult->getQueueInfos())
        );
    }

    /**
     * @covers \WindowsAzure\ServiceBus\ServiceBusManagementRestProxy::listQueues
     * @covers \WindowsAzure\ServiceBus\Models\QueueDescription::create
     */
    public function testListQueueSuccess(): void
    {
        // Setup
        $queueName = 'testListQueueSuccess';
        $createQueueInfo = new QueueInfo($queueName);
        $listQueuesOptions = new ListQueuesOptions();
        $listQueuesResult = $this->serviceBusManagementWrapper->listQueues($listQueuesOptions);

        foreach ($listQueuesResult->getQueueInfos() as $queueInfo) {
            $this->serviceBusManagementWrapper->deleteQueue($queueInfo->getTitle());
        }
        $this->serviceBusManagementWrapper->createQueue($createQueueInfo);

        // Test
        $listQueuesResult = $this->serviceBusManagementWrapper->listQueues($listQueuesOptions);
        $this->serviceBusManagementWrapper->deleteQueue($queueName);

        // Assert
        $this->assertEquals(
            1,
            \count($listQueuesResult->getQueueInfos())
        );
    }

    /**
     * @covers \WindowsAzure\ServiceBus\ServiceBusManagementRestProxy::createTopic
     * @covers \WindowsAzure\ServiceBus\ServiceBusManagementRestProxy::deleteTopic
     * @covers \WindowsAzure\ServiceBus\ServiceBusManagementRestProxy::listTopics
     * @covers \WindowsAzure\ServiceBus\ServiceBusManagementRestProxy::getTopic
     */
    public function testCreateListFetchAndDeleteTopicSuccess(): void
    {
        // Setup
        $topicName = 'createTopicSuccess';
        $topicInfo = new TopicInfo($topicName);
        $listTopicsOptions = new ListTopicsOptions();
        $listTopicsResult = $this->serviceBusManagementWrapper->listTopics($listTopicsOptions);

        foreach ($listTopicsResult->getTopicInfos() as $topicInfo) {
            $this->serviceBusManagementWrapper->deleteTopic($topicInfo->getTitle());
        }

        // Test
        $createTopicResult = $this->createTopic($topicInfo);
        $listTopicsResult = $this->serviceBusManagementWrapper->listTopics($listTopicsOptions);
        $getTopicResult = $this->serviceBusManagementWrapper->getTopic($topicName);
        $this->safeDeleteTopic($topicName);
        $listTopicsResult2 = $this->serviceBusManagementWrapper->listTopics($listTopicsOptions);

        // Assert
        $this->assertNotNull($createTopicResult);
        $this->assertNotNull($listTopicsResult);
        $this->assertNotNull($getTopicResult);
        $this->assertNotNull($listTopicsResult2);

        $this->assertEquals(
            1,
            \count($listTopicsResult->getTopicInfos())
        );
    }

    /**
     * @covers \WindowsAzure\ServiceBus\ServiceBusManagementRestProxy::createSubscription
     */
    public function testSubscriptionCanBeCreatedOnTopics(): void
    {
        // Setup
        $topicName = 'testCreateSubscriptionWorksTopic';
        $subscriptionName = 'testCreateSubscriptionWorksSubscription';
        $topicInfo = new TopicInfo($topicName);
        $subscriptionInfo = new SubscriptionInfo($subscriptionName);
        $this->safeDeleteSubscription($topicName, $subscriptionName);
        $this->safeDeleteTopic($topicName);
        $this->createTopic($topicInfo);

        // Test
        $subscriptionInfo = $this->createSubscription(
            $topicName,
            $subscriptionInfo
        );

        // Assert
        $this->assertNotNull($subscriptionInfo);
        $this->assertEquals(
            $subscriptionName,
            $subscriptionInfo->getTitle()
        );
    }

    /**
     * @covers \WindowsAzure\ServiceBus\ServiceBusManagementRestProxy::listSubscriptions
     */
    public function testSubscriptionsCanBeListed(): void
    {
        // Setup
        $topicName = 'testSubscriptionCanBeListed';
        $subscriptionName = 'sub';
        $topicInfo = new TopicInfo($topicName);
        $subscriptionInfo = new SubscriptionInfo($subscriptionName);
        $listSubscriptionOptions = new ListSubscriptionsOptions();
        $this->safeDeleteSubscription($topicName, $subscriptionName);
        $this->safeDeleteTopic($topicName);
        $this->createTopic($topicInfo);
        $this->createSubscription($topicName, $subscriptionInfo);

        // Test
        $listSubscriptionsResult = $this->serviceBusManagementWrapper->listSubscriptions(
            $topicName,
            $listSubscriptionOptions
        );

        // Assert
        $this->assertNotNull($listSubscriptionsResult);
        $this->assertEquals(
            1,
            \count($listSubscriptionsResult->getSubscriptionInfos())
        );
    }

    /**
     * @covers \WindowsAzure\ServiceBus\ServiceBusManagementRestProxy::getSubscription
     */
    public function testSubscriptionsDetailsMayBeFetched(): void
    {
        // Setup
        $topicName = 'testSubscriptionsDetailsMayBeFetched';
        $subscriptionName = 'sub';
        $topicInfo = new TopicInfo($topicName);
        $subscriptionInfo = new SubscriptionInfo($subscriptionName);
        $this->safeDeleteSubscription($topicName, $subscriptionName);
        $this->safeDeleteTopic($topicName);
        $this->createTopic($topicInfo);
        $this->createSubscription($topicName, $subscriptionInfo);

        // Test
        $subscriptionInfo = $this->serviceBusManagementWrapper->getSubscription(
            $topicName,
            $subscriptionName
        );

        // Assert
        $this->assertNotNull($subscriptionInfo);
        $this->assertEquals(
            $subscriptionName,
            $subscriptionInfo->getTitle()
        );
    }

    /**
     * @covers \WindowsAzure\ServiceBus\ServiceBusManagementRestProxy::deleteSubscription
     */
    public function testSubscriptionMayBeDeleted(): void
    {
        // Setup
        $topicName = 'testSubscriptionMayBeDeleted';
        $topicInfo = new TopicInfo($topicName);
        $subscriptionName = 'MySubscription';
        $subscriptionInfo = new SubscriptionInfo($subscriptionName);
        $listSubscriptionsOptions = new ListSubscriptionsOptions();
        $this->safeDeleteSubscription($topicName, $subscriptionName);
        $this->safeDeleteTopic($topicName);
        $this->createTopic($topicInfo);
        $this->createSubscription($topicName, $subscriptionInfo);

        // Test
        $this->serviceBusManagementWrapper->deleteSubscription($topicName, $subscriptionName);
        $listSubscriptionsResult = $this->serviceBusManagementWrapper->listSubscriptions(
            $topicName,
            $listSubscriptionsOptions
        );
        $subscriptionInfo = $listSubscriptionsResult->getSubscriptionInfos();

        // Assert
        $this->assertNotNull($listSubscriptionsResult);
        $this->assertEquals(
            0,
            \count($subscriptionInfo)
        );
    }

    /**
     * @covers \WindowsAzure\ServiceBus\ServiceBusManagementRestProxy::listSubscriptions
     */
    public function testSubscriptionCanBeListed(): void
    {
        // Setup
        $topicName = 'testSubscriptionMayBeDeleted';
        $topicInfo = new TopicInfo($topicName);
        $subscriptionName = 'MySubscription';
        $secondSubscriptionName = 'MySecondSubscription';
        $this->safeDeleteSubscription($topicName, $subscriptionName);
        $this->safeDeleteSubscription($topicName, $secondSubscriptionName);
        $this->safeDeleteTopic($topicName);
        $subscriptionInfo = new SubscriptionInfo($subscriptionName);
        $secondSubscriptionInfo =
            new SubscriptionInfo($secondSubscriptionName);
        $listSubscriptionOptions = new ListSubscriptionsOptions();

        $this->createTopic($topicInfo);
        $this->createSubscription($topicName, $subscriptionInfo);
        $this->createSubscription($topicName, $secondSubscriptionInfo);

        // Test
        $listSubscriptionsResult = $this->serviceBusManagementWrapper->listSubscriptions(
            $topicName,
            $listSubscriptionOptions
        );

        $subscriptionInfo = $listSubscriptionsResult->getSubscriptionInfos();

        $this->serviceBusManagementWrapper->deleteSubscription($topicName, $secondSubscriptionName);
        $this->serviceBusManagementWrapper->deleteSubscription($topicName, $subscriptionName);

        $emptyListSubscriptionsResult = $this->serviceBusManagementWrapper->listSubscriptions(
            $topicName,
            $listSubscriptionOptions
        );

        $emptySubscriptionInfo = $emptyListSubscriptionsResult->getSubscriptionInfos();

        // Assert
        $this->assertNotNull($listSubscriptionsResult);
        $this->assertNotNull($emptyListSubscriptionsResult);
        $this->assertEquals(
            2,
            \count($subscriptionInfo)
        );

        $this->assertEquals(
            0,
            \count($emptySubscriptionInfo)
        );
    }

    /**
     * @covers \WindowsAzure\ServiceBus\ServiceBusManagementRestProxy::createRule
     * @covers \WindowsAzure\ServiceBus\Models\RuleInfo::parseXml
     * @covers \WindowsAzure\ServiceBus\Models\RuleDescription::create
     */
    public function testRulesCanBeCreatedOnSubscription(): void
    {
        // Setup
        $topicName = 'testRulesCanBeCreatedOnSubscription';
        $subscriptionName = 'sub';
        $topicInfo = new TopicInfo($topicName);
        $subscriptionInfo = new SubscriptionInfo($subscriptionName);
        $ruleName = 'MyRule';
        $ruleInfo = new RuleInfo($ruleName);
        $this->safeDeleteSubscription($topicName, $subscriptionName);
        $this->safeDeleteTopic($topicName);
        $this->createTopic($topicInfo);
        $this->createSubscription($topicName, $subscriptionInfo);

        // Test
        $createRuleResult = $this->createRule($topicName, $subscriptionName, $ruleInfo);
        $ruleInfo = $this->serviceBusManagementWrapper->getRule($topicName, $subscriptionName, $ruleName);

        // Assert
        $this->assertNotNull($createRuleResult);
        $this->assertEquals(
            $ruleName,
            $ruleInfo->getTitle()
        );
    }

    /**
     * @covers \WindowsAzure\ServiceBus\ServiceBusManagementRestProxy::createRule
     * @covers \WindowsAzure\ServiceBus\ServiceBusManagementRestProxy::listRules
     */
    public function testRulesCanBeListedAndDefaultRuleIsPrecreated(): void
    {
        // Setup
        $topicName = 'testRulesCanBeListedAndDefaultRuleIsPrecreated';
        $subscriptionName = 'sub';
        $ruleName = 'MyRule';
        $secondRuleName = 'MyRule2';
        $topicInfo = new TopicInfo($topicName);
        $subscriptionInfo = new SubscriptionInfo($subscriptionName);
        $ruleInfo = new RuleInfo($ruleName);
        $secondRuleInfo = new RuleInfo($secondRuleName);
        $listRulesOptions = new ListRulesOptions();

        $this->safeDeleteSubscription($topicName, $subscriptionName);
        $this->safeDeleteTopic($topicName);

        $this->createTopic($topicInfo);
        $this->createSubscription($topicName, $subscriptionInfo);

        // Test
        $this->createRule($topicName, $subscriptionName, $ruleInfo);
        $this->createRule($topicName, $subscriptionName, $secondRuleInfo);
        $listRulesResult = $this->serviceBusManagementWrapper->listRules($topicName, $subscriptionName, $listRulesOptions);

        // Assert
        $this->assertNotNull($listRulesResult);
        $this->assertEquals(3, \count($listRulesResult->getRuleInfos()));
    }

    /**
     * @covers \WindowsAzure\ServiceBus\ServiceBusManagementRestProxy::getRule
     */
    public function testRuleDetailsMayBeFetched(): void
    {
        // Setup
        $topicName = 'testRuleDetailsMayBeFetched';
        $subscriptionName = 'sub';
        $topicInfo = new TopicInfo($topicName);
        $subscriptionInfo = new SubscriptionInfo($subscriptionName);
        $this->safeDeleteSubscription($topicName, $subscriptionName);
        $this->safeDeleteTopic($topicName);
        $this->createTopic($topicInfo);
        $this->createSubscription($topicName, $subscriptionInfo);

        // Test
        $getRuleResult = $this->serviceBusManagementWrapper->getRule(
            $topicName,
            $subscriptionName,
            Resources::DEFAULT_RULE_NAME
        );

        // Assert
        $this->assertNotNull($getRuleResult);
    }

    /**
     * @covers \WindowsAzure\ServiceBus\ServiceBusManagementRestProxy::getRule
     * @covers \WindowsAzure\ServiceBus\ServiceBusManagementRestProxy::deleteRule
     */
    public function testRuleMayBeDeleted(): void
    {
        // Setup
        $topicName = 'testRuleMayBeDeleted';
        $subscriptionName = 'sub';
        $firstRuleName = 'RuleNumberOne';
        $secondRuleName = 'RuleNumberTwo';

        $topicInfo = new TopicInfo($topicName);
        $subscriptionInfo = new SubscriptionInfo($subscriptionName);
        $firstRuleInfo = new RuleInfo($firstRuleName);
        $secondRuleInfo = new RuleInfo($secondRuleName);
        $listRulesOptions = new ListRulesOptions();

        $this->safeDeleteSubscription($topicName, $subscriptionName);
        $this->safeDeleteTopic($topicName);

        $this->createTopic($topicInfo);
        $this->createSubscription($topicName, $subscriptionInfo);
        $this->createRule($topicName, $subscriptionName, $firstRuleInfo);
        $this->createRule($topicName, $subscriptionName, $secondRuleInfo);

        // Test
        $this->serviceBusManagementWrapper->deleteRule($topicName, $subscriptionName, $secondRuleName);
        $this->serviceBusManagementWrapper->deleteRule($topicName, $subscriptionName, $firstRuleName);
        $this->serviceBusManagementWrapper->deleteRule($topicName, $subscriptionName, Resources::DEFAULT_RULE_NAME);

        $listRulesResult = $this->serviceBusManagementWrapper->listRules($topicName, $subscriptionName, $listRulesOptions);
        $ruleInfo = $listRulesResult->getRuleInfos();

        // Assert
        $this->assertNotNull($ruleInfo);
        $this->assertEquals(0, \count($ruleInfo));
    }

    /**
     * @covers \WindowsAzure\ServiceBus\ServiceBusManagementRestProxy::listRules
     * @covers \WindowsAzure\ServiceBus\Models\TopicInfo::parseXml
     * @covers \WindowsAzure\ServiceBus\Models\TopicDescription::create
     * @covers \WindowsAzure\ServiceBus\Models\SubscriptionInfo::parseXml
     * @covers \WindowsAzure\ServiceBus\Models\SubscriptionDescription::create
     * @covers \WindowsAzure\ServiceBus\Models\RuleInfo::parseXml
     * @covers \WindowsAzure\ServiceBus\Models\RuleDescription::create
     */
    public function testListRulesDeserializePropertiesOfSqlFilter(): void
    {
        // Setup
        $topicName = 'testListRulesDeserializePropertiesOfSqlFilter';
        $subscriptionName = 'sub';
        $expected = 'OrderID=123';

        $topicInfo = new TopicInfo($topicName);
        $subscriptionInfo = new SubscriptionInfo($subscriptionName);
        $this->safeDeleteSubscription($topicName, $subscriptionName);
        $this->safeDeleteTopic($topicName);

        $createTopicResult = $this->createTopic($topicInfo);
        $subscriptionInfo = $this->createSubscription(
            $topicName,
            $subscriptionInfo
        );
        $rule = new RuleInfo('one');
        $rule->withSqlFilter('OrderID=123');
        $this->serviceBusManagementWrapper->createRule($topicName, $subscriptionName, $rule);

        // Test
        $listRulesResult = $this->serviceBusManagementWrapper->listRules($topicName, $subscriptionName);
        $ruleInfo = $listRulesResult->getRuleInfos();
        $ruleInfoInstance = $ruleInfo[1];
        /** @var SqlFilter $actualFilter */
        $actualFilter = $ruleInfoInstance->getFilter();
        $actual = $actualFilter->getSqlExpression();

        // Assert
        $this->assertEquals(
            $expected,
            $actual
        );
    }

    /**
     * @covers \WindowsAzure\ServiceBus\ServiceBusManagementRestProxy::getRule
     */
    public function testRulesMayHaveActionAndFilter(): void
    {
        // Setup
        $topicName = 'testRulesMayHaveActionAndFilter';
        $subscriptionName = 'sub';

        $topicInfo = new TopicInfo($topicName);
        $subscriptionInfo = new SubscriptionInfo($subscriptionName);
        $this->safeDeleteSubscription($topicName, $subscriptionName);
        $this->safeDeleteTopic($topicName);

        $createTopicResult = $this->createTopic($topicInfo);
        $subscriptionInfo = $this->createSubscription(
            $topicName,
            $subscriptionInfo
        );
        $expectedRuleOne = new RuleInfo('one');
        $expectedRuleOne->withCorrelationFilter('my-id');

        $expectedRuleTwo = new RuleInfo('two');
        $expectedRuleTwo->withTrueFilter();

        $expectedRuleThree = new RuleInfo('three');
        $expectedRuleThree->withFalseFilter();

        $expectedRuleFour = new RuleInfo('four');
        $expectedRuleFour->withEmptyRuleAction();

        $expectedRuleFive = new RuleInfo('five');
        $expectedRuleFive->withSqlRuleAction('SET x = 5');

        $expectedRuleSix = new RuleInfo('six');
        $expectedRuleSix->withSqlFilter('x != 5');

        // Test
        $actualRuleOne = $this->serviceBusManagementWrapper->createRule(
            $topicName,
            $subscriptionName,
            $expectedRuleOne
        );

        $actualRuleTwo = $this->serviceBusManagementWrapper->createRule(
            $topicName,
            $subscriptionName,
            $expectedRuleTwo
        );

        $actualRuleThree = $this->serviceBusManagementWrapper->createRule(
            $topicName,
            $subscriptionName,
            $expectedRuleThree
        );

        $actualRuleFour = $this->serviceBusManagementWrapper->createRule(
            $topicName,
            $subscriptionName,
            $expectedRuleFour
        );

        $actualRuleFive = $this->serviceBusManagementWrapper->createRule(
            $topicName,
            $subscriptionName,
            $expectedRuleFive
        );

        $actualRuleSix = $this->serviceBusManagementWrapper->createRule(
            $topicName,
            $subscriptionName,
            $expectedRuleSix
        );

        // Assert
        $this->assertNotNull($createTopicResult);
        $this->assertNotNull($subscriptionInfo);

        $this->assertInstanceOf(
            'WindowsAzure\ServiceBus\Models\CorrelationFilter',
            $actualRuleOne->getFilter()
        );

        $this->assertInstanceOf(
            'WindowsAzure\ServiceBus\Models\TrueFilter',
            $actualRuleTwo->getFilter()
        );

        $this->assertInstanceOf(
            'WindowsAzure\ServiceBus\Models\FalseFilter',
            $actualRuleThree->getFilter()
        );

        $this->assertInstanceOf(
            'WindowsAzure\ServiceBus\Models\EmptyRuleAction',
            $actualRuleFour->getAction()
        );

        $this->assertInstanceOf(
            'WindowsAzure\ServiceBus\Models\SqlRuleAction',
            $actualRuleFive->getAction()
        );

        $this->assertInstanceOf(
            'WindowsAzure\ServiceBus\Models\SqlFilter',
            $actualRuleSix->getFilter()
        );
    }
}
