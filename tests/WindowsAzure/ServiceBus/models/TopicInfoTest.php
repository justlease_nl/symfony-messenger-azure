<?php

declare(strict_types=1);

/**
 * LICENSE: Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * PHP version 5
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @see      https://github.com/WindowsAzure/azure-sdk-for-php
 */

namespace Tests\unit\WindowsAzure\ServiceBus\models;

use WindowsAzure\ServiceBus\Models\TopicDescription;
use WindowsAzure\ServiceBus\Models\TopicInfo;

/**
 * Unit tests for class WrapAccessTokenResult.
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @version   Release: 0.5.0_2016-11
 *
 * @see      https://github.com/WindowsAzure/azure-sdk-for-php
 */
class TopicInfoTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @covers \WindowsAzure\ServiceBus\Models\TopicInfo::__construct
     */
    public function testTopicInfoConstructor(): void
    {
        // Setup

        // Test
        $topicInfo = new TopicInfo('testNameOfTopic');

        // Assert
        $this->assertNotNull($topicInfo);
    }

    /**
     * @covers \WindowsAzure\ServiceBus\Models\TopicInfo::getTopicDescription
     * @covers \WindowsAzure\ServiceBus\Models\TopicInfo::setTopicDescription
     */
    public function testGetSetTopicDescription(): void
    {
        // Setup
        $expected = new TopicDescription();
        $topicInfo = new TopicInfo();

        // Test
        $topicInfo->setTopicDescription($expected);
        $actual = $topicInfo->getTopicDescription();

        // Assert
        $this->assertEquals(
            $expected,
            $actual
        );
    }

    /**
     * @covers \WindowsAzure\ServiceBus\Models\TopicInfo::getDefaultMessageTimeToLive
     * @covers \WindowsAzure\ServiceBus\Models\TopicInfo::setDefaultMessageTimeToLive
     */
    public function testGetSetDefaultMessageTimeToLive(): void
    {
        // Setup
        $expected = 'testDefaultMessageTimeToLive';
        $topicInfo = new TopicInfo();

        // Test
        $topicInfo->setDefaultMessageTimeToLive($expected);
        $actual = $topicInfo->getDefaultMessageTimeToLive();

        // Assert
        $this->assertEquals(
            $expected,
            $actual
        );
    }

    /**
     * @covers \WindowsAzure\ServiceBus\Models\TopicInfo::getMaxSizeInMegabytes
     * @covers \WindowsAzure\ServiceBus\Models\TopicInfo::setMaxSizeInMegabytes
     */
    public function testGetSetMaxSizeInMegabytes(): void
    {
        // Setup
        $expected = mt_rand();
        $topicInfo = new TopicInfo();

        // Test
        $topicInfo->setMaxSizeInMegabytes($expected);
        $actual = $topicInfo->getMaxSizeInMegabytes();

        // Assert
        $this->assertEquals(
            $expected,
            $actual
        );
    }

    /**
     * @covers \WindowsAzure\ServiceBus\Models\TopicInfo::getRequiresDuplicateDetection
     * @covers \WindowsAzure\ServiceBus\Models\TopicInfo::setRequiresDuplicateDetection
     * @dataProvider trueFalseProvider
     */
    public function testGetSetRequiresDuplicateDetection(bool $expected): void
    {
        // Setup
        $topicInfo = new TopicInfo();

        // Test
        $topicInfo->setRequiresDuplicateDetection($expected);
        $actual = $topicInfo->getRequiresDuplicateDetection();

        // Assert
        $this->assertEquals(
            $expected,
            $actual
        );
    }

    /**
     * @covers \WindowsAzure\ServiceBus\Models\TopicInfo::getDuplicateDetectionHistoryTimeWindow
     * @covers \WindowsAzure\ServiceBus\Models\TopicInfo::setDuplicateDetectionHistoryTimeWindow
     */
    public function testGetSetDuplicateDetectionHistoryTimeWindow(): void
    {
        // Setup
        $expected = 'testDuplicateDetectionHistoryTimeWindow';
        $topicInfo = new TopicInfo();

        // Test
        $topicInfo->setDuplicateDetectionHistoryTimeWindow($expected);
        $actual = $topicInfo->getDuplicateDetectionHistoryTimeWindow();

        // Assert
        $this->assertEquals(
            $expected,
            $actual
        );
    }

    /**
     * @covers \WindowsAzure\ServiceBus\Models\TopicInfo::getEnableBatchedOperations
     * @covers \WindowsAzure\ServiceBus\Models\TopicInfo::setEnableBatchedOperations
     * @dataProvider trueFalseProvider
     */
    public function testGetSetEnableBatchedOperations(bool $expected): void
    {
        // Setup
        $topicInfo = new TopicInfo();

        // Test
        $topicInfo->setEnableBatchedOperations($expected);
        $actual = $topicInfo->getEnableBatchedOperations();

        // Assert
        $this->assertEquals(
            $expected,
            $actual
        );
    }

    /**
     * @covers \WindowsAzure\ServiceBus\Models\TopicInfo::getSizeInBytes
     * @covers \WindowsAzure\ServiceBus\Models\TopicInfo::setSizeInBytes
     */
    public function testGetSetSizeInBytes(): void
    {
        // Setup
        $expected = mt_rand();
        $topicInfo = new TopicInfo();

        // Test
        $topicInfo->setSizeInBytes($expected);
        $actual = $topicInfo->getSizeInBytes();

        // Assert
        $this->assertEquals(
            $expected,
            $actual
        );
    }

    public function trueFalseProvider()
    {
        return [
            [true],
            [false],
        ];
    }
}
