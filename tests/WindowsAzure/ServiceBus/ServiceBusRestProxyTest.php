<?php

declare(strict_types=1);

/**
 * LICENSE: Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * PHP version 5
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @see      https://github.com/WindowsAzure/azure-sdk-for-php
 */

namespace Tests\unit\WindowsAzure\ServiceBus;

use Tests\framework\ServiceBusRestProxyTestBase;
use WindowsAzure\Common\Internal\Resources;
use WindowsAzure\ServiceBus\Models\BrokeredMessage;
use WindowsAzure\ServiceBus\Models\QueueDescription;
use WindowsAzure\ServiceBus\Models\QueueInfo;
use WindowsAzure\ServiceBus\Models\ReceiveMessageOptions;
use WindowsAzure\ServiceBus\Models\SubscriptionInfo;
use WindowsAzure\ServiceBus\Models\TopicInfo;

/**
 * Unit tests for ServiceBusRestProxy class.
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @version   Release: 0.5.0_2016-11
 *
 * @see      https://github.com/WindowsAzure/azure-sdk-for-php
 */
class ServiceBusRestProxyTest extends ServiceBusRestProxyTestBase
{
    /**
     * @covers \WindowsAzure\ServiceBus\ServiceBusRestProxy::sendQueueMessage
     */
    public function testSendQueueMessageWorks(): void
    {
        // Setup
        $queueName = 'sendQueueMessageWorksQueue';
        $queueInfo = new QueueInfo($queueName);
        $this->safeDeleteQueue($queueName);
        $this->createQueue($queueInfo);
        $brokeredMessage = new BrokeredMessage();
        $brokeredMessage->setBody('sendQueueMessageWorksMessage');

        // Test
        $this->serviceBusWrapper->sendQueueMessage(
            'sendQueueMessageWorksQueue',
            $brokeredMessage
        );

        // Assert
        $this->assertNotNull($brokeredMessage);
    }

    /**
     * @covers \WindowsAzure\ServiceBus\ServiceBusRestProxy::receiveMessage
     */
    public function testReceiveMessageWorks(): void
    {
        // Setup
        $queueDescription = new QueueDescription();
        $queueName = 'testReceiveMessageWorksQueue';
        $queueInfo = new QueueInfo($queueName, $queueDescription);
        $this->safeDeleteQueue($queueName);
        $this->createQueue($queueInfo);
        $brokeredMessage = new BrokeredMessage();
        $expectedMessageText = 'testReceiveMessageWorks';
        $brokeredMessage->setBody($expectedMessageText);
        $this->serviceBusWrapper->sendQueueMessage(
            $queueName,
            $brokeredMessage
        );
        $receiveMessageOptions = new ReceiveMessageOptions();
        $receiveMessageOptions->setTimeout(5);
        $receiveMessageOptions->setReceiveAndDelete();

        // Test
        $receivedMessage = $this->serviceBusWrapper->receiveMessage(
            $queueName.'/messages/head',
            $receiveMessageOptions
        );

        // Assert
        $this->assertNotNull($receivedMessage);
        $this->assertEquals(
            $expectedMessageText,
            $receivedMessage->getBody()
        );
    }

    /**
     * @covers \WindowsAzure\ServiceBus\ServiceBusRestProxy::receiveQueueMessage
     */
    public function testPeekLockMessageWorks(): void
    {
        // Setup
        $queueDescription = new QueueDescription();
        $queueName = 'testPeekLockMessageWorks';
        $expectedMessage = 'testPeekLockMessageWorksMessage';
        $queueInfo = new QueueInfo($queueName, $queueDescription);
        $this->safeDeleteQueue($queueName);
        $this->createQueue($queueInfo);
        $brokeredMessage = new BrokeredMessage();
        $brokeredMessage->setBody($expectedMessage);
        $this->serviceBusWrapper->sendQueueMessage($queueName, $brokeredMessage);
        $receiveMessageOptions = new ReceiveMessageOptions();
        $receiveMessageOptions->setTimeout(5);
        $receiveMessageOptions->setPeekLock();

        // Test
        $receivedMessage = $this->serviceBusWrapper->receiveQueueMessage(
            $queueName,
            $receiveMessageOptions
        );

        // Assert
        $actualMessage = $receivedMessage->getBody();
        $this->assertEquals(
            $expectedMessage,
            $actualMessage
        );
    }

    /**
     * @covers \WindowsAzure\ServiceBus\ServiceBusRestProxy::deleteMessage
     */
    public function testDeleteMessageInvalidMessage(): void
    {
        // Setup
        $queueDescription = new QueueDescription();
        $queueName = 'testDeleteMessageInvalidMessage';
        $queueInfo = new QueueInfo($queueName, $queueDescription);
        $this->safeDeleteQueue($queueName);
        $this->createQueue($queueInfo);
        $brokeredMessage = new BrokeredMessage();
        $brokeredMessage->setLockLocation('');
        $this->expectException(\get_class(new \InvalidArgumentException()));

        // Test
        $this->serviceBusWrapper->deleteMessage($brokeredMessage);

        // Assert
        $this->assertTrue(false);
    }

    /**
     * @covers \WindowsAzure\ServiceBus\ServiceBusRestProxy::deleteMessage
     */
    public function testDeleteMessageSuccess(): void
    {
        // Setup
        $queueDescription = new QueueDescription();
        $queueName = 'testDeleteMessageSuccess';
        $expectedMessage = 'testDeleteMessageSuccess';
        $queueInfo = new QueueInfo($queueName, $queueDescription);
        $this->safeDeleteQueue($queueName);
        $this->createQueue($queueInfo);
        $brokeredMessage = new BrokeredMessage();
        $brokeredMessage->setBody($expectedMessage);

        // Test
        $this->serviceBusWrapper->sendQueueMessage($queueName, $brokeredMessage);
        $receiveMessageOptions = new ReceiveMessageOptions();
        $receiveMessageOptions->setTimeout(5);
        $receiveMessageOptions->setPeekLock();
        $receivedMessage = $this->serviceBusWrapper->receiveQueueMessage(
            $queueName,
            $receiveMessageOptions
        );
        $this->serviceBusWrapper->deleteMessage($receivedMessage);

        // Assert
        $this->assertTrue(true);
    }

    /**
     * @covers \WindowsAzure\ServiceBus\ServiceBusRestProxy::receiveQueueMessage
     */
    public function testPeekLockedMessageCanBeCompleted(): void
    {
        // Setup
        $queueDescription = new QueueDescription();
        $queueName = 'testPeekLockMessageCanBeCompleted';
        $expectedMessage = 'testPeekLockMessageCanBeCompletedMessage';
        $queueInfo = new QueueInfo($queueName, $queueDescription);
        $this->safeDeleteQueue($queueName);
        $this->createQueue($queueInfo);
        $brokeredMessage = new BrokeredMessage();
        $brokeredMessage->setBody($expectedMessage);

        $this->serviceBusWrapper->sendQueueMessage(
            $queueName,
            $brokeredMessage
        );

        $receiveMessageOptions = new ReceiveMessageOptions();
        $receiveMessageOptions->setTimeout(5);
        $receiveMessageOptions->setPeekLock();

        // Test
        $receivedMessage = $this->serviceBusWrapper->receiveQueueMessage(
            $queueName,
            $receiveMessageOptions
        );

        // Assert
        $lockToken = $receivedMessage->getLockToken();
        $lockedUntil = $receivedMessage->getLockedUntilUtc();
        $this->assertNotNull($lockToken);
        $this->assertNotNull($lockedUntil);
    }

    /**
     * @covers \WindowsAzure\ServiceBus\ServiceBusRestProxy::unlockMessage
     */
    public function testPeekLockedMessageCanBeUnlocked(): void
    {
        // Setup
        $queueDescription = new QueueDescription();
        $queueName = 'testPeekLockMessageCanBeUnlocked';
        $expectedMessage = 'testPeekLockMessageCanBeUnlocked';
        $queueInfo = new QueueInfo($queueName, $queueDescription);
        $this->safeDeleteQueue($queueName);
        $this->createQueue($queueInfo);
        $brokeredMessage = new BrokeredMessage();
        $brokeredMessage->setBody($expectedMessage);
        $this->serviceBusWrapper->sendQueueMessage($queueName, $brokeredMessage);
        $receiveMessageOptions = new ReceiveMessageOptions();
        $receiveMessageOptions->setTimeout(5);
        $receiveMessageOptions->setPeekLock();
        $peekedMessage = $this->serviceBusWrapper->receiveQueueMessage(
            $queueName,
            $receiveMessageOptions
        );
        $lockToken = $peekedMessage->getLockToken();
        $lockedUntilUtc = $peekedMessage->getLockedUntilUtc();

        // Test
        $this->serviceBusWrapper->unlockMessage($peekedMessage);
        $receiveMessageOptions->setReceiveAndDelete();
        $unlockedMessage = $this->serviceBusWrapper->receiveQueueMessage(
            $queueName,
            $receiveMessageOptions
        );

        // Assert
        $this->assertNotNull($lockToken);
        $this->assertNotNull($lockedUntilUtc);
        $this->assertNull($unlockedMessage->getLockToken());
        $this->assertNull($unlockedMessage->getLockedUntilUtc());
    }

    /**
     * @covers \WindowsAzure\ServiceBus\ServiceBusRestProxy::sendMessage
     * @covers \WindowsAzure\ServiceBus\ServiceBusRestProxy::sendQueueMessage
     * @covers \WindowsAzure\ServiceBus\ServiceBusRestProxy::receiveMessage
     */
    public function testContentTypePassesThrough(): void
    {
        // Setup
        $queueName = 'testContentTypePassesThrough';
        $queueDescription = new QueueDescription();
        $queueInfo = new QueueInfo($queueName, $queueDescription);
        $this->safeDeleteQueue($queueName);
        $this->createQueue($queueInfo);
        $expectedMessage = new BrokeredMessage();
        $expectedMessage->setBody('<data>testContentTypePassesThrough</data>');
        $expectedMessage->setContentType(Resources::XML_CONTENT_TYPE);
        $receiveMessageOptions = new ReceiveMessageOptions();
        $receiveMessageOptions->setTimeout(5);
        $receiveMessageOptions->setReceiveAndDelete();

        // Test
        $this->serviceBusWrapper->sendQueueMessage($queueName, $expectedMessage);
        $actualMessage = $this->serviceBusWrapper->receiveQueueMessage(
            $queueName,
            $receiveMessageOptions
        );

        // Assert
        $this->assertEquals(
            $expectedMessage->getBody(),
            $actualMessage->getBody()
        );

        $this->assertEquals(
            $expectedMessage->getContentType(),
            $actualMessage->getContentType()
        );
    }

    /**
     * @covers \WindowsAzure\ServiceBus\ServiceBusRestProxy::sendTopicMessage
     * @covers \WindowsAzure\ServiceBus\ServiceBusRestProxy::receiveMessage
     * @covers \WindowsAzure\ServiceBus\ServiceBusRestProxy::receiveSubscriptionMessage
     */
    public function testSubscriptionWillReceiveMessage(): void
    {
        // Setup
        $topicName = 'testSubscriptionWillReceiveMessage';
        $subscriptionName = 'sub';
        $messageBody = '<p>testSubscriptionWillReceiveMessage</p>';
        $topicInfo = new TopicInfo($topicName);
        $subscriptionInfo = new SubscriptionInfo($subscriptionName);
        $brokeredMessage = new BrokeredMessage();
        $brokeredMessage->setBody($messageBody);
        $brokeredMessage->setContentType('text/html');
        $createTopicResult = $this->createTopic($topicInfo);
        $createSubscriptionResult = $this->createSubscription($topicName, $subscriptionInfo);
        $receiveMessageOptions = new ReceiveMessageOptions();
        $receiveMessageOptions->setTimeout(5);
        $receiveMessageOptions->setReceiveAndDelete();

        // Test
        $this->serviceBusWrapper->sendTopicMessage($topicName, $brokeredMessage);
        $receivedMessage = $this->serviceBusWrapper->receiveSubscriptionMessage($topicName, $subscriptionName, $receiveMessageOptions);

        // Assert
        $this->assertNotNull($createTopicResult);
        $this->assertNotNull($createSubscriptionResult);
        $this->assertNotNull($receivedMessage);
        $this->assertEquals(
            $messageBody,
            $receivedMessage->getBody()
        );
    }

    /**
     * @covers \WindowsAzure\ServiceBus\ServiceBusRestProxy::receiveQueueMessage
     */
    public function testMessageMayHaveCustomProperties(): void
    {
        // Setup
        $queueName = 'testMessageMayHaveCustomProperties';
        $queueDescription = new QueueDescription();
        $queueInfo = new QueueInfo($queueName, $queueDescription);
        $expectedTestStringValue = 'testStringValue';
        $expectedTestIntValue = 38;
        $expectedTestDoubleValue = 3.14159;
        $expectedTestBooleanValue = true;
        $expectedTestBooleanFalseValue = false;
        $expectedTestArrayValue = [2, 3, 5, 7];

        $this->safeDeleteQueue($queueName);
        $this->createQueue($queueInfo);
        $brokeredMessage = new BrokeredMessage();

        $brokeredMessage->setProperty('testString', $expectedTestStringValue);
        $brokeredMessage->setProperty('testInt', $expectedTestIntValue);
        $brokeredMessage->setProperty('testDouble', $expectedTestDoubleValue);
        $brokeredMessage->setProperty('testBoolean', $expectedTestBooleanValue);
        $brokeredMessage->setProperty('testBooleanFalse', $expectedTestBooleanFalseValue);

        $this->serviceBusWrapper->sendQueueMessage($queueName, $brokeredMessage);
        $receiveMessageOptions = new ReceiveMessageOptions();
        $receiveMessageOptions->setTimeout(5);
        $receiveMessageOptions->setReceiveAndDelete();

        // Test
        $receivedMessage = $this->serviceBusWrapper->receiveQueueMessage(
            $queueName,
            $receiveMessageOptions
        );

        // Assert
        $this->assertNotNull($receivedMessage);
        $this->assertEquals(
            $expectedTestStringValue,
            $receivedMessage->getProperty('testString')
        );

        $this->assertEquals(
            $expectedTestIntValue,
            $receivedMessage->getProperty('testInt')
        );

        $this->assertEquals(
            $expectedTestDoubleValue,
            $receivedMessage->getProperty('testDouble')
        );

        $this->assertEquals(
            $expectedTestBooleanValue,
            $receivedMessage->getProperty('testBoolean')
        );

        $this->assertEquals(
            $expectedTestBooleanFalseValue,
            $receivedMessage->getProperty('testBooleanFalse')
        );
    }
}
