<?php

declare(strict_types=1);

/**
 * LICENSE: Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * PHP version 5
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @see      https://github.com/windowsazure/azure-sdk-for-php
 */

namespace Tests\unit\WindowsAzure\Common\Internal\Serialization;

use WindowsAzure\Common\Internal\Serialization\XmlSerializer;

/**
 * Unit tests for class XmlSerializer.
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @version   Release: 0.5.0_2016-11
 *
 * @see      https://github.com/windowsazure/azure-sdk-for-php
 */
class XmlSerializerTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @covers \WindowsAzure\Common\Internal\Serialization\XmlSerializer::objectSerialize
     */
    public function testObjectSerializeSuccess(): void
    {
        // Setup
        $expected = "<DummyClass/>\n";
        $target = new DummyClass();

        // Test
        $actual = XmlSerializer::objectSerialize($target, 'DummyClass');

        // Assert
        $this->assertEquals(
            $expected,
            $actual
        );
    }

    /**
     * @covers \WindowsAzure\Common\Internal\Serialization\XmlSerializer::objectSerialize
     */
    public function testObjectSerializeSuccessWithAttributes(): void
    {
        // Setup
        $expected = "<DummyClass testAttribute=\"testAttributeValue\"/>\n";
        $target = new DummyClass();
        $target->addAttribute('testAttribute', 'testAttributeValue');

        // Test
        $actual = XmlSerializer::objectSerialize($target, 'DummyClass');

        // Assert
        $this->assertEquals(
            $expected,
            $actual
        );
    }
}
