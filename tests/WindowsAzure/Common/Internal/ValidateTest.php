<?php

declare(strict_types=1);

/**
 * LICENSE: Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * PHP version 5
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @see      https://github.com/windowsazure/azure-sdk-for-php
 */

namespace Tests\unit\WindowsAzure\Common\Internal;

use WindowsAzure\Common\Internal\InvalidArgumentTypeException;
use WindowsAzure\Common\Internal\Resources;
use WindowsAzure\Common\Internal\Validate;

/**
 * Unit tests for class ValidateTest.
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @version   Release: 0.5.0_2016-11
 *
 * @see      https://github.com/windowsazure/azure-sdk-for-php
 */
class ValidateTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @covers \WindowsAzure\Common\Internal\Validate::isArray
     */
    public function testIsArrayWithArray(): void
    {
        Validate::isArray([], 'array');

        $this->assertTrue(true);
    }

    /**
     * @covers \WindowsAzure\Common\Internal\Validate::isArray
     */
    public function testIsArrayWithNonArray(): void
    {
        $this->expectException(\get_class(new InvalidArgumentTypeException('')));
        Validate::isArray(123, 'array');
    }

    /**
     * @covers \WindowsAzure\Common\Internal\Validate::isString
     */
    public function testIsStringWithString(): void
    {
        Validate::isString('I\'m a string', 'string');

        $this->assertTrue(true);
    }

    /**
     * @covers \WindowsAzure\Common\Internal\Validate::isString
     */
    public function testIsStringWithNonString(): void
    {
        $this->expectException(InvalidArgumentTypeException::class);
        Validate::isString(new \stdClass(), 'string');
    }

    /**
     * @covers \WindowsAzure\Common\Internal\Validate::isTrue
     */
    public function testIsTrueWithTrue(): void
    {
        Validate::isTrue(true, Resources::EMPTY_STRING);

        $this->assertTrue(true);
    }

    /**
     * @covers \WindowsAzure\Common\Internal\Validate::isTrue
     */
    public function testIsTrueWithFalse(): void
    {
        $this->expectException('\InvalidArgumentException');
        Validate::isTrue(false, Resources::EMPTY_STRING);
    }

    /**
     * @covers \WindowsAzure\Common\Internal\Validate::notNullOrEmpty
     */
    public function testNotNullOrEmptyWithNonEmpty(): void
    {
        Validate::notNullOrEmpty(1234, 'not null');

        $this->assertTrue(true);
    }

    /**
     * @covers \WindowsAzure\Common\Internal\Validate::notNullOrEmpty
     */
    public function testNotNullOrEmptyWithEmpty(): void
    {
        $this->expectException('\InvalidArgumentException');
        Validate::notNullOrEmpty(Resources::EMPTY_STRING, 'variable');
    }

    /**
     * @covers \WindowsAzure\Common\Internal\Validate::notNull
     */
    public function testNotNullWithNull(): void
    {
        $this->expectException('\InvalidArgumentException');
        Validate::notNullOrEmpty(null, 'variable');
    }

    /**
     * @covers \WindowsAzure\Common\Internal\Validate::isInstanceOf
     */
    public function testIsInstanceOfStringPasses(): void
    {
        // Setup
        $value = 'testString';
        $stringObject = 'stringObject';

        // Test
        $result = Validate::isInstanceOf($value, $stringObject, 'value');

        // Assert
        $this->assertTrue($result);
    }

    /**
     * @covers \WindowsAzure\Common\Internal\Validate::isInstanceOf
     */
    public function testIsInstanceOfStringFail(): void
    {
        // Setup
        $this->expectException('\InvalidArgumentException');
        $value = 'testString';
        $arrayObject = [];

        // Test
        $result = Validate::isInstanceOf($value, $arrayObject, 'value');

        // Assert
    }

    /**
     * @covers \WindowsAzure\Common\Internal\Validate::isInstanceOf
     */
    public function testIsInstanceOfArrayPasses(): void
    {
        // Setup
        $value = [];
        $arrayObject = [];

        // Test
        $result = Validate::isInstanceOf($value, $arrayObject, 'value');

        // Assert
        $this->assertTrue($result);
    }

    /**
     * @covers \WindowsAzure\Common\Internal\Validate::isInstanceOf
     */
    public function testIsInstanceOfArrayFail(): void
    {
        // Setup
        $this->expectException('\InvalidArgumentException');
        $value = [];
        $stringObject = 'testString';

        // Test
        $result = Validate::isInstanceOf($value, $stringObject, 'value');

        // Assert
    }

    /**
     * @covers \WindowsAzure\Common\Internal\Validate::isInstanceOf
     */
    public function testIsInstanceOfIntPasses(): void
    {
        // Setup
        $value = 38;
        $intObject = 83;

        // Test
        $result = Validate::isInstanceOf($value, $intObject, 'value');

        // Assert
        $this->assertTrue($result);
    }

    /**
     * @covers \WindowsAzure\Common\Internal\Validate::isInstanceOf
     */
    public function testIsInstanceOfIntFail(): void
    {
        // Setup
        $this->expectException('\InvalidArgumentException');
        $value = 38;
        $stringObject = 'testString';

        // Test
        $result = Validate::isInstanceOf($value, $stringObject, 'value');

        // Assert
    }

    /**
     * @covers \WindowsAzure\Common\Internal\Validate::isInstanceOf
     */
    public function testIsInstanceOfNullValue(): void
    {
        // Setup
        $value = null;
        $arrayObject = [];

        // Test
        $result = Validate::isInstanceOf($value, $arrayObject, 'value');

        // Assert
        $this->assertTrue($result);
    }

    /**
     * @covers \WindowsAzure\Common\Internal\Validate::getIsValidUri
     */
    public function testGetValidateUri(): void
    {
        // Test
        $function = Validate::getIsValidUri();

        // Assert
        $this->assertIsObject($function);
    }

    /**
     * @covers \WindowsAzure\Common\Internal\Validate::isValidUri
     */
    public function testIsValidUriPass(): void
    {
        // Setup
        $value = 'http://test.com';

        // Test
        $result = Validate::isValidUri($value, 'uri');

        // Assert
        $this->assertTrue($result);
    }

    /**
     * @covers \WindowsAzure\Common\Internal\Validate::isValidUri
     */
    public function testIsValidUriNull(): void
    {
        // Setup
        $this->expectException(\get_class(new \RuntimeException('')));
        $value = null;

        // Test
        $result = Validate::isValidUri($value, 'uri');

        // Assert
    }

    /**
     * @covers \WindowsAzure\Common\Internal\Validate::isValidUri
     */
    public function testIsValidUriNotUri(): void
    {
        // Setup
        $this->expectException(\get_class(new \RuntimeException('')));
        $value = 'test string';

        // Test
        $result = Validate::isValidUri($value, 'uri');

        // Assert
    }
}
