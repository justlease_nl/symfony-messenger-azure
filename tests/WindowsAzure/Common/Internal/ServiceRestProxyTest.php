<?php

declare(strict_types=1);

/**
 * LICENSE: Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * PHP version 5
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @see      https://github.com/windowsazure/azure-sdk-for-php
 */

namespace Tests\unit\WindowsAzure\Common\Internal;

use GuzzleHttp\Psr7\HttpFactory;
use Tests\mock\WindowsAzure\Common\Internal\Filters\SimpleFilterMock;
use WindowsAzure\Common\Internal\Http\HttpClient;
use WindowsAzure\Common\Internal\Resources;
use WindowsAzure\Common\Internal\RestProxy;
use WindowsAzure\Common\Internal\ServiceRestProxy;

/**
 * Unit tests for class ServiceRestProxy.
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @version   Release: 0.5.0_2016-11
 *
 * @see      https://github.com/windowsazure/azure-sdk-for-php
 */
class ServiceRestProxyTest extends \PHPUnit\Framework\TestCase
{
    public function testConstruct()
    {
        // Setup
        $uri = 'http://www.microsoft.com';
        $httpFactory = new HttpFactory();
        $httpClient = HttpClient::createClient();

        // Test
        $proxy = new ServiceRestProxy(
            $uri,
            $httpFactory,
            $httpFactory,
            $httpFactory,
            $httpClient
        );

        // Assert
        $this->assertNotNull($proxy);
        $this->assertEquals($uri, $proxy->getUri());

        return $proxy;
    }

    /**
     * @covers  \WindowsAzure\Common\Internal\ServiceRestProxy::withFilter
     * @depends testConstruct
     */
    public function testWithFilter(RestProxy $restRestProxy): void
    {
        // Setup
        $filter = new SimpleFilterMock('name', 'value');

        // Test
        $actual = $restRestProxy->withFilter($filter);

        // Assert
        $this->assertCount(1, $actual->getFilters());
        $this->assertCount(0, $restRestProxy->getFilters());
    }

    /**
     * @covers  \WindowsAzure\Common\Internal\ServiceRestProxy::getFilters
     * @depends testConstruct
     */
    public function testGetFilters(RestProxy $restRestProxy): void
    {
        // Setup
        $filter = new SimpleFilterMock('name', 'value');
        $withFilter = $restRestProxy->withFilter($filter);

        // Test
        $actual1 = $withFilter->getFilters();
        $actual2 = $restRestProxy->getFilters();

        // Assert
        $this->assertCount(1, $actual1);
        $this->assertCount(0, $actual2);
    }

    /**
     * @covers  \WindowsAzure\Common\Internal\ServiceRestProxy::groupQueryValues
     * @depends testConstruct
     */
    public function testGroupQueryValues(ServiceRestProxy $restRestProxy): void
    {
        // Setup
        $values = ['A', 'B', 'C'];
        $expected = 'A,B,C';

        // Test
        $actual = $restRestProxy->groupQueryValues($values);

        // Assert
        $this->assertEquals($expected, $actual);
    }

    /**
     * @covers  \WindowsAzure\Common\Internal\ServiceRestProxy::groupQueryValues
     * @depends testConstruct
     */
    public function testGroupQueryValuesWithNulls(ServiceRestProxy $restRestProxy): void
    {
        // Setup
        $values = [null, '', null];

        // Test
        $actual = $restRestProxy->groupQueryValues($values);

        // Assert
        $this->assertTrue(empty($actual));
    }

    /**
     * @covers  \WindowsAzure\Common\Internal\ServiceRestProxy::groupQueryValues
     * @depends testConstruct
     */
    public function testGroupQueryValuesWithMix(ServiceRestProxy $restRestProxy): void
    {
        // Setup
        $values = [null, 'B', 'C', ''];
        $expected = 'B,C';

        // Test
        $actual = $restRestProxy->groupQueryValues($values);

        // Assert
        $this->assertEquals($expected, $actual);
    }

    /**
     * @covers \WindowsAzure\Common\Internal\ServiceRestProxy::generateMetadataHeaders
     * @depends testConstruct
     */
    public function testGenerateMetadataHeader(ServiceRestProxy $proxy): void
    {
        // Setup
        $metadata = ['key1' => 'value1', 'MyName' => 'WindowsAzure', 'MyCompany' => 'Microsoft_'];
        $expected = [];
        foreach ($metadata as $key => $value) {
            $expected[Resources::X_MS_META_HEADER_PREFIX.strtolower($key)] = $value;
        }

        // Test
        $actual = $proxy->generateMetadataHeaders($metadata);

        // Assert
        $this->assertEquals($expected, $actual);
    }

    /**
     * @covers \WindowsAzure\Common\Internal\ServiceRestProxy::generateMetadataHeaders
     * @depends testConstruct
     */
    public function testGenerateMetadataHeaderInvalidNameFail(ServiceRestProxy $proxy): void
    {
        // Setup
        $metadata = ['key1' => "value1\n", 'MyName' => "\rAzurr", 'MyCompany' => "Micr\r\nosoft_"];
        $this->expectException(\get_class(new \InvalidArgumentException(Resources::INVALID_META_MSG)));

        // Test
        $proxy->generateMetadataHeaders($metadata);
    }

    /**
     * @covers \WindowsAzure\Common\Internal\ServiceRestProxy::getMetadataArray
     * @depends testConstruct
     */
    public function testGetMetadataArray(ServiceRestProxy $proxy): void
    {
        // Setup
        $expected = ['key1' => 'value1', 'myname' => 'azure', 'mycompany' => 'microsoft_'];
        $metadataHeaders = [];
        foreach ($expected as $key => $value) {
            $metadataHeaders[Resources::X_MS_META_HEADER_PREFIX.strtolower($key)] = $value;
        }

        // Test
        $actual = $proxy->getMetadataArray($metadataHeaders);

        // Assert
        $this->assertEquals($expected, $actual);
    }

    /**
     * @covers \WindowsAzure\Common\Internal\ServiceRestProxy::getMetadataArray
     * @depends testConstruct
     */
    public function testGetMetadataArrayWithMsHeaders(ServiceRestProxy $proxy): void
    {
        // Setup
        $key = 'name';
        $validMetadataKey = Resources::X_MS_META_HEADER_PREFIX.$key;
        $value = 'correct';
        $metadataHeaders = ['x-ms-key1' => 'value1', 'myname' => 'x-ms-date',
                          $validMetadataKey => $value, 'mycompany' => 'microsoft_', ];

        // Test
        $actual = $proxy->getMetadataArray($metadataHeaders);

        // Assert
        $this->assertCount(1, $actual);
        $this->assertEquals($value, $actual[$key]);
    }
}
