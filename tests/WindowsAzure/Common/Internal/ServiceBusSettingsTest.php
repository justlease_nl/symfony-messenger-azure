<?php

declare(strict_types=1);

/**
 * LICENSE: Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * PHP version 5
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @see      https://github.com/windowsazure/azure-sdk-for-php
 */

namespace Tests\unit\WindowsAzure\Common\Internal;

use WindowsAzure\Common\Internal\IServiceFilter;
use WindowsAzure\Common\Internal\Resources;
use WindowsAzure\ServiceBus\ServiceBusSettings;

/**
 * Unit tests for class ServiceBusSettings.
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @version   Release: 0.5.0_2016-11
 *
 * @see      https://github.com/windowsazure/azure-sdk-for-php
 */
class ServiceBusSettingsTest extends \PHPUnit\Framework\TestCase
{
    protected function setUp(): void
    {
        $property = new \ReflectionProperty('WindowsAzure\ServiceBus\ServiceBusSettings', 'isInitialized');
        $property->setAccessible(true);
        $property->setValue(false);
    }

    /**
     * @covers \WindowsAzure\ServiceBus\ServiceBusSettings::createFromConnectionString
     * @covers \WindowsAzure\ServiceBus\ServiceBusSettings::init
     * @covers \WindowsAzure\ServiceBus\ServiceBusSettings::__construct
     * @covers \WindowsAzure\Common\Internal\ServiceSettings::getValidator
     * @covers \WindowsAzure\Common\Internal\ServiceSettings::optional
     * @covers \WindowsAzure\Common\Internal\ServiceSettings::allRequired
     * @covers \WindowsAzure\Common\Internal\ServiceSettings::setting
     * @covers \WindowsAzure\Common\Internal\ServiceSettings::settingWithFunc
     * @covers \WindowsAzure\Common\Internal\ServiceSettings::matchedSpecification
     * @covers \WindowsAzure\Common\Internal\ServiceSettings::parseAndValidateKeys
     * @covers \WindowsAzure\Common\Internal\ServiceSettings::noMatch
     */
    public function testCreateFromConnectionStringWithServiceBusAutomaticCase(): void
    {
        // Setup
        $namespace = 'mynamespace';
        $expectedServiceBusEndpoint = "https://$namespace.servicebus.windows.net";
        $expectedKeyName = 'myname';
        $expectedKey = 'mypassword';
        $connectionString = "Endpoint=$expectedServiceBusEndpoint;SharedAccessKeyName=$expectedKeyName;SharedAccessKey=$expectedKey";

        // Test
        $actual = ServiceBusSettings::createFromConnectionString($connectionString);

        // Assert
        $this->assertInstanceOf('WindowsAzure\Common\Internal\IServiceFilter', $actual->getFilter());
        $this->assertEquals($expectedServiceBusEndpoint, $actual->getServiceBusEndpointUri());
    }

    /**
     * @covers \WindowsAzure\ServiceBus\ServiceBusSettings::createFromConnectionString
     * @covers \WindowsAzure\ServiceBus\ServiceBusSettings::init
     * @covers \WindowsAzure\ServiceBus\ServiceBusSettings::__construct
     * @covers \WindowsAzure\Common\Internal\ServiceSettings::getValidator
     * @covers \WindowsAzure\Common\Internal\ServiceSettings::optional
     * @covers \WindowsAzure\Common\Internal\ServiceSettings::allRequired
     * @covers \WindowsAzure\Common\Internal\ServiceSettings::setting
     * @covers \WindowsAzure\Common\Internal\ServiceSettings::settingWithFunc
     * @covers \WindowsAzure\Common\Internal\ServiceSettings::matchedSpecification
     * @covers \WindowsAzure\Common\Internal\ServiceSettings::parseAndValidateKeys
     * @covers \WindowsAzure\Common\Internal\ServiceSettings::noMatch
     */
    public function testCreateFromConnectionStringWithMissingServiceBusEndpointFail(): void
    {
        // Setup
        $connectionString = 'SharedAccessKeyName=name;SharedAccessKey=password';
        $expectedMsg = sprintf(Resources::MISSING_CONNECTION_STRING_SETTINGS, $connectionString);

        $this->expectException('\RuntimeException');
        $this->expectExceptionMessage($expectedMsg);

        // Test
        ServiceBusSettings::createFromConnectionString($connectionString);
    }

    /**
     * @covers \WindowsAzure\ServiceBus\ServiceBusSettings::createFromConnectionString
     * @covers \WindowsAzure\ServiceBus\ServiceBusSettings::init
     * @covers \WindowsAzure\ServiceBus\ServiceBusSettings::__construct
     * @covers \WindowsAzure\Common\Internal\ServiceSettings::getValidator
     * @covers \WindowsAzure\Common\Internal\ServiceSettings::optional
     * @covers \WindowsAzure\Common\Internal\ServiceSettings::allRequired
     * @covers \WindowsAzure\Common\Internal\ServiceSettings::setting
     * @covers \WindowsAzure\Common\Internal\ServiceSettings::settingWithFunc
     * @covers \WindowsAzure\Common\Internal\ServiceSettings::matchedSpecification
     * @covers \WindowsAzure\Common\Internal\ServiceSettings::parseAndValidateKeys
     * @covers \WindowsAzure\Common\Internal\ServiceSettings::noMatch
     */
    public function testCreateFromConnectionStringWithInvalidServiceBusKeyFail(): void
    {
        // Setup
        $invalidKey = 'InvalidKey';
        $connectionString = "$invalidKey=value;SharedSecretIssuer=name;SharedSecretValue=password";
        $expectedMsg = sprintf(
            Resources::INVALID_CONNECTION_STRING_SETTING_KEY,
            $invalidKey,
            implode("\n", ['Endpoint', 'SharedAccessKeyName', 'SharedAccessKey'])
        );
        $this->expectException('\RuntimeException');
        $this->expectExceptionMessage($expectedMsg);

        // Test
        \WindowsAzure\ServiceBus\ServiceBusSettings::createFromConnectionString($connectionString);
    }

    /**
     * @covers \WindowsAzure\ServiceBus\ServiceBusSettings::getServiceBusEndpointUri
     */
    public function testGetServiceBusEndpointUri(): void
    {
        // Setup
        $expected = 'serviceBusEndpointUri';
        $setting = new ServiceBusSettings($expected, $this->createMock(IServiceFilter::class));

        // Test
        $actual = $setting->getServiceBusEndpointUri();

        // Assert
        $this->assertEquals($expected, $actual);
    }

    /**
     * @covers \WindowsAzure\ServiceBus\ServiceBusSettings::getFilter
     */
    public function testGetFilter(): void
    {
        // Setup
        $expected = $this->createMock(IServiceFilter::class);
        $setting = new ServiceBusSettings('', $expected);

        // Test
        $actual = $setting->getFilter();

        // Assert
        $this->assertEquals($expected, $actual);
    }

    /**
     * @covers \WindowsAzure\ServiceBus\ServiceBusSettings::createFromConnectionString
     * @covers \WindowsAzure\ServiceBus\ServiceBusSettings::init
     * @covers \WindowsAzure\ServiceBus\ServiceBusSettings::__construct
     * @covers \WindowsAzure\Common\Internal\ServiceSettings::getValidator
     * @covers \WindowsAzure\Common\Internal\ServiceSettings::optional
     * @covers \WindowsAzure\Common\Internal\ServiceSettings::allRequired
     * @covers \WindowsAzure\Common\Internal\ServiceSettings::setting
     * @covers \WindowsAzure\Common\Internal\ServiceSettings::settingWithFunc
     * @covers \WindowsAzure\Common\Internal\ServiceSettings::matchedSpecification
     * @covers \WindowsAzure\Common\Internal\ServiceSettings::parseAndValidateKeys
     * @covers \WindowsAzure\Common\Internal\ServiceSettings::noMatch
     */
    public function testCreateFromConnectionStringWithCaseInvesitive(): void
    {
        // Setup
        $namepspace = 'mynamespace';
        $expectedServiceBusEndpoint = "https://$namepspace.servicebus.windows.net";
        $expectedKeyName = 'myname';
        $expectedKey = 'mypassword';
        $connectionString = "eNdPoinT=$expectedServiceBusEndpoint;shARedaCCESskeYNAme=$expectedKeyName;shAReDacCESSkeY=$expectedKey";

        // Test
        $actual = ServiceBusSettings::createFromConnectionString($connectionString);

        // Assert
        $this->assertInstanceOf('WindowsAzure\Common\Internal\IServiceFilter', $actual->getFilter());
        $this->assertEquals($expectedServiceBusEndpoint, $actual->getServiceBusEndpointUri());
    }
}
