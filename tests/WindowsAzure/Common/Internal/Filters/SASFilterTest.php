<?php

declare(strict_types=1);

namespace Tests\unit\WindowsAzure\Common\Internal\Filters;

use GuzzleHttp\Psr7\Request;
use PHPUnit\Framework\TestCase;
use WindowsAzure\Common\Internal\Filters\SASFilter;

class SASFilterTest extends TestCase
{
    public function testHandleRequest(): void
    {
        $sharedAccessKey = 'secret-access-key';
        $sharedAccessKeyName = 'key-name';
        $filter = new SASFilter($sharedAccessKeyName, $sharedAccessKey);
        $request = new Request('GET', 'https://sb-name.servicebus.windows.net/');
        $result = $filter->handleRequest($request);
        self::assertStringStartsWith('SharedAccessSignature sig=', $result->getHeader('authorization')[0]);
        self::assertStringEndsWith('&skn=key-name&sr=https%3a%2f%2fsb-name.servicebus.windows.net%2f', $result->getHeader('authorization')[0]);
    }
}
