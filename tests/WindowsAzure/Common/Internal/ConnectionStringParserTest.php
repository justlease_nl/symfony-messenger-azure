<?php

declare(strict_types=1);

/**
 * LICENSE: Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * PHP version 5
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @see      https://github.com/windowsazure/azure-sdk-for-php
 */

namespace Tests\unit\WindowsAzure\Common\Internal;

use WindowsAzure\Common\Internal\ConnectionStringParser;

/**
 * Unit tests for class ConnectionStringParser.
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @version   Release: 0.5.0_2016-11
 *
 * @see      https://github.com/windowsazure/azure-sdk-for-php
 */
class ConnectionStringParserTest extends \PHPUnit\Framework\TestCase
{
    private function _parseTest($connectionString): void
    {
        // Setup
        $arguments = \func_get_args();
        $count = \func_num_args();
        $expected = [];
        for ($i = 1; $i < $count; $i += 2) {
            $expected[$arguments[$i]] = $arguments[$i + 1];
        }

        // Test
        $actual = ConnectionStringParser::parseConnectionString('connectionString', $connectionString);

        // Assert
        $this->assertEquals($expected, $actual);
    }

    /**
     * @covers \WindowsAzure\Common\Internal\ConnectionStringParser::parseConnectionString
     * @covers \WindowsAzure\Common\Internal\ConnectionStringParser::__construct
     * @covers \WindowsAzure\Common\Internal\ConnectionStringParser::_parse
     * @covers \WindowsAzure\Common\Internal\ConnectionStringParser::_createException
     * @covers \WindowsAzure\Common\Internal\ConnectionStringParser::_skipWhiteSpaces
     * @covers \WindowsAzure\Common\Internal\ConnectionStringParser::_extractKey
     * @covers \WindowsAzure\Common\Internal\ConnectionStringParser::_extractString
     * @covers \WindowsAzure\Common\Internal\ConnectionStringParser::_skipOperator
     * @covers \WindowsAzure\Common\Internal\ConnectionStringParser::_extractValue
     */
    public function testKeyNames(): void
    {
        $this->_parseTest('a=b', 'a', 'b');
        $this->_parseTest(' a =b; c = d', 'a', 'b', 'c', 'd');
        $this->_parseTest('a b=c', 'a b', 'c');
        $this->_parseTest("'a b'=c", 'a b', 'c');
        $this->_parseTest('"a b"=c', 'a b', 'c');
        $this->_parseTest('"a=b"=c', 'a=b', 'c');
        $this->_parseTest('a=b=c', 'a', 'b=c');
        $this->_parseTest("'a='=b", 'a=', 'b');
        $this->_parseTest('"a="=b', 'a=', 'b');
        $this->_parseTest("\"a'b\"=c", "a'b", 'c');
        $this->_parseTest("'a\"b'=c", 'a"b', 'c');
        $this->_parseTest("a'b=c", "a'b", 'c');
        $this->_parseTest('a"b=c', 'a"b', 'c');
        $this->_parseTest("a'=b", "a'", 'b');
        $this->_parseTest('a"=b', 'a"', 'b');
    }

    /**
     * @covers \WindowsAzure\Common\Internal\ConnectionStringParser::parseConnectionString
     * @covers \WindowsAzure\Common\Internal\ConnectionStringParser::__construct
     * @covers \WindowsAzure\Common\Internal\ConnectionStringParser::_parse
     * @covers \WindowsAzure\Common\Internal\ConnectionStringParser::_createException
     * @covers \WindowsAzure\Common\Internal\ConnectionStringParser::_skipWhiteSpaces
     * @covers \WindowsAzure\Common\Internal\ConnectionStringParser::_extractKey
     * @covers \WindowsAzure\Common\Internal\ConnectionStringParser::_extractString
     * @covers \WindowsAzure\Common\Internal\ConnectionStringParser::_skipOperator
     * @covers \WindowsAzure\Common\Internal\ConnectionStringParser::_extractValue
     */
    public function testAssignments(): void
    {
        $this->_parseTest('a=b', 'a', 'b');
        $this->_parseTest('a = b', 'a', 'b');
        $this->_parseTest('a==b', 'a', '=b');
    }

    /**
     * @covers \WindowsAzure\Common\Internal\ConnectionStringParser::parseConnectionString
     * @covers \WindowsAzure\Common\Internal\ConnectionStringParser::__construct
     * @covers \WindowsAzure\Common\Internal\ConnectionStringParser::_parse
     * @covers \WindowsAzure\Common\Internal\ConnectionStringParser::_createException
     * @covers \WindowsAzure\Common\Internal\ConnectionStringParser::_skipWhiteSpaces
     * @covers \WindowsAzure\Common\Internal\ConnectionStringParser::_extractKey
     * @covers \WindowsAzure\Common\Internal\ConnectionStringParser::_extractString
     * @covers \WindowsAzure\Common\Internal\ConnectionStringParser::_skipOperator
     * @covers \WindowsAzure\Common\Internal\ConnectionStringParser::_extractValue
     */
    public function testValues(): void
    {
        $this->_parseTest('a=b', 'a', 'b');
        $this->_parseTest('a= b ', 'a', 'b');
        $this->_parseTest('a= b ;c= d;', 'a', 'b', 'c', 'd');
        $this->_parseTest('a=', 'a', '');
        $this->_parseTest('a=;', 'a', '');
        $this->_parseTest('a=;b=', 'a', '', 'b', '');
        $this->_parseTest('a==b', 'a', '=b');
        $this->_parseTest('a=b=;c==d=', 'a', 'b=', 'c', '=d=');
        $this->_parseTest("a='b c'", 'a', 'b c');
        $this->_parseTest('a="b c"', 'a', 'b c');
        $this->_parseTest("a=\"b'c\"", 'a', "b'c");
        $this->_parseTest("a='b\"c'", 'a', 'b"c');
        $this->_parseTest("a='b=c'", 'a', 'b=c');
        $this->_parseTest('a="b=c"', 'a', 'b=c');
        $this->_parseTest("a='b;c=d'", 'a', 'b;c=d');
        $this->_parseTest('a="b;c=d"', 'a', 'b;c=d');
        $this->_parseTest("a='b c' ", 'a', 'b c');
        $this->_parseTest('a="b c" ', 'a', 'b c');
        $this->_parseTest("a=b'c", 'a', "b'c");
        $this->_parseTest('a=b"c', 'a', 'b"c');
        $this->_parseTest("a=b'", 'a', "b'");
        $this->_parseTest('a=b"', 'a', 'b"');
    }

    /**
     * @covers \WindowsAzure\Common\Internal\ConnectionStringParser::parseConnectionString
     * @covers \WindowsAzure\Common\Internal\ConnectionStringParser::__construct
     * @covers \WindowsAzure\Common\Internal\ConnectionStringParser::_parse
     * @covers \WindowsAzure\Common\Internal\ConnectionStringParser::_createException
     * @covers \WindowsAzure\Common\Internal\ConnectionStringParser::_skipWhiteSpaces
     * @covers \WindowsAzure\Common\Internal\ConnectionStringParser::_extractKey
     * @covers \WindowsAzure\Common\Internal\ConnectionStringParser::_extractString
     * @covers \WindowsAzure\Common\Internal\ConnectionStringParser::_skipOperator
     * @covers \WindowsAzure\Common\Internal\ConnectionStringParser::_extractValue
     */
    public function testSeparators(): void
    {
        $this->_parseTest('a=b;', 'a', 'b');
        $this->_parseTest('a=b', 'a', 'b');
        $this->_parseTest('a=b;c=d', 'a', 'b', 'c', 'd');
        $this->_parseTest('a=b;c=d;', 'a', 'b', 'c', 'd');
        $this->_parseTest('a=b ; c=d', 'a', 'b', 'c', 'd');
    }

    /**
     * @covers \WindowsAzure\Common\Internal\ConnectionStringParser::parseConnectionString
     * @covers \WindowsAzure\Common\Internal\ConnectionStringParser::__construct
     * @covers \WindowsAzure\Common\Internal\ConnectionStringParser::_parse
     * @covers \WindowsAzure\Common\Internal\ConnectionStringParser::_createException
     * @covers \WindowsAzure\Common\Internal\ConnectionStringParser::_skipWhiteSpaces
     * @covers \WindowsAzure\Common\Internal\ConnectionStringParser::_extractKey
     * @covers \WindowsAzure\Common\Internal\ConnectionStringParser::_extractString
     * @covers \WindowsAzure\Common\Internal\ConnectionStringParser::_skipOperator
     * @covers \WindowsAzure\Common\Internal\ConnectionStringParser::_extractValue
     * @dataProvider invalidInputFailProvider
     */
    public function testInvalidInputFail($value, string $expectedMessage): void
    {
        // Setup
        $this->expectException('\RuntimeException');
        $this->expectExceptionMessage($expectedMessage);

        // Test
        ConnectionStringParser::parseConnectionString('connectionString', $value);
    }

    public function invalidInputFailProvider()
    {
        return [
            'Separator without an assignment' => [';', 'Missing key name'],
            'Missing key name' => ['=b', 'Missing key name'],
            'Empty key name single-quoted string' => ["''=b", 'Empty key name'],
            'Empty key name double-quoted string' => ['""=b', 'Empty key name'],
            'Missing assignment' => ['test', 'Missing = character'],
            'Separator without key=value' => [';a=b', 'Missing key name'],
            'Two separators at the end' => ['a=b;;', 'Missing key name'],
            'Two separators in the middle' => ['a=b;;c=d', 'Missing key name'],
            'Runaway single-quoted string at the beginning of the key name' => ["'a=b", 'Missing \' character'],
            'Runaway double-quoted string at the beginning of the key name' => ['"a=b', 'Missing " character'],
            'Runaway single-quoted string in key name' => ["'=b", 'Missing \' character'],
            'Runaway double-quoted string in key name' => ['"=b', 'Missing " character'],
            'Runaway single-quoted string in value' => ["a='b", 'Missing \' character'],
            'Runaway double-quoted string in value' => ['a="b', 'Missing " character'],
            'Extra character after single-quoted value' => ["a='b'c", 'Missing ; character'],
            'Extra character after double-quoted value' => ['a="b"c', 'Missing ; character'],
            'Extra character after single-quoted key' => ["'a'b=c", 'Missing = character'],
            'Extra character after double-quoted key' => ['"a"b=c', 'Missing = character'],
        ];
    }
}
