<?php

declare(strict_types=1);

/**
 * LICENSE: Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * PHP version 5
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @see      https://github.com/windowsazure/azure-sdk-for-php
 */

namespace Tests\unit\WindowsAzure\Common\Internal;

use WindowsAzure\Common\Internal\Utilities;

/**
 * Unit tests for class Utilities.
 *
 * @category  Microsoft
 *
 * @author    Azure PHP SDK <azurephpsdk@microsoft.com>
 * @copyright 2012 Microsoft Corporation
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 *
 * @version   Release: 0.5.0_2016-11
 *
 * @see      https://github.com/windowsazure/azure-sdk-for-php
 */
class UtilitiesTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @covers \WindowsAzure\Common\Internal\Utilities::tryGetValue
     */
    public function testTryGetValue(): void
    {
        // Setup
        $key = 0;
        $expected = 10;
        $data = [10, 20, 30];

        // Test
        $actual = Utilities::tryGetValue($data, $key);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @covers \WindowsAzure\Common\Internal\Utilities::tryGetValue
     */
    public function testTryGetValueUsingDefault(): void
    {
        // Setup
        $key = 10;
        $expected = 6;
        $data = [10, 20, 30];

        // Test
        $actual = Utilities::tryGetValue($data, $key, $expected);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @covers \WindowsAzure\Common\Internal\Utilities::tryGetValue
     */
    public function testTryGetValueWithNull(): void
    {
        // Setup
        $key = 10;
        $data = [10, 20, 30];

        // Test
        $actual = Utilities::tryGetValue($data, $key);

        $this->assertNull($actual);
    }

    /**
     * @covers \WindowsAzure\Common\Internal\Utilities::startsWith
     */
    public function testStartsWith(): void
    {
        // Setup
        $string = 'myname';
        $prefix = 'my';

        // Test
        $actual = Utilities::startsWith($string, $prefix);

        $this->assertTrue($actual);
    }

    /**
     * @covers \WindowsAzure\Common\Internal\Utilities::startsWith
     */
    public function testStartsWithDoesNotStartWithPrefix(): void
    {
        // Setup
        $string = 'amyname';
        $prefix = 'my';

        // Test
        $actual = Utilities::startsWith($string, $prefix);

        $this->assertFalse($actual);
    }

    /**
     * @covers \WindowsAzure\Common\Internal\Utilities::toBoolean
     */
    public function testToBoolean(): void
    {
        // Setup
        $value = 'true';
        $expected = true;

        // Test
        $actual = Utilities::toBoolean($value);

        // Assert
        $this->assertTrue(\is_bool($actual));
        $this->assertEquals($expected, $actual);
    }

    /**
     * @covers \WindowsAzure\Common\Internal\Utilities::booleanToString
     */
    public function testBooleanToString(): void
    {
        // Setup
        $expected = 'true';
        $value = true;

        // Test
        $actual = Utilities::booleanToString($value);

        // Assert
        $this->assertEquals($expected, $actual);
    }

    /**
     * @covers \WindowsAzure\Common\Internal\Utilities::startsWith
     */
    public function testStartsWithIgnoreCase(): void
    {
        // Setup
        $string = 'MYString';
        $prefix = 'mY';

        // Test
        $actual = Utilities::startsWith($string, $prefix, true);

        // Assert
        $this->assertTrue($actual);
    }

    /**
     * @covers \WindowsAzure\Common\Internal\Utilities::inArrayInsensitive
     */
    public function testInArrayInsensitive(): void
    {
        // Setup
        $value = 'CaseInsensitiVe';
        $array = ['caSeinSenSitivE'];

        // Test
        $actual = Utilities::inArrayInsensitive($value, $array);

        // Assert
        $this->assertTrue($actual);
    }

    /**
     * @covers \WindowsAzure\Common\Internal\Utilities::tryGetValueInsensitive
     */
    public function testTryGetValueInsensitive(): void
    {
        // Setup
        $key = 'KEy';
        $value = 1;
        $array = [$key => $value];

        // Test
        $actual = Utilities::tryGetValueInsensitive('keY', $array);

        // Assert
        $this->assertEquals($value, $actual);
    }
}
